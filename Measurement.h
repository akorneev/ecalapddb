#ifndef ROOT_Measurement
#define ROOT_Measurement

//////////////////////////////////////////////////////////////////////////
//                                                                      //
//  22.10.2000 by Andrei Dorokhov                                       //    
//  The program is developing and supporting by Andrei Kouznetsov       // 
//  Measurement                                                         //
//                                                                      //
// Description of Hamamatsu  APDs                                       //
//                                                                      //
//////////////////////////////////////////////////////////////////////////

//#define RESTRICTEDVERSION
#include "Token.h"

#include "TObject.h"
#include "TClonesArray.h"
#include "TH1.h"
#include "TMath.h"
#include "TCanvas.h"

#include <iostream>

#define VARS(t,n) t f##n; t Get##n(){return f##n;} void Set##n(t n) { f##n = n;}
#define VARA(t,n) t f##n; t * Get##n(){return & f##n;} void Set##n(t * n) { f##n = * n;}

class Header : public TObject {
public :
   TString  fFullSerialNo;    
   void DeleteArr(TObject *o);
public :
   Header(){}
   ~Header(){} 
   Int_t sn2i(const char *s) {return atoi(&s[4]);}
   Int_t GetIndex() {const char *s = fFullSerialNo.Data();return atoi(&s[4]);}
   void SetIndex(TString s) {fFullSerialNo = s;}
   TString i2sn(TObjArray *arr,Int_t i) {return ((Header*)arr->At(i))->fFullSerialNo;}
   TString GetUnitsWC(TString n,TString type);
   TString GetUnits(TString n,TString type);

   ClassDef(Header,1)
};

class Hamamatsu : public Header {
public :
   Hamamatsu();
   virtual ~Hamamatsu();

   VARS(TString,Date)

   TString  fPosition; 
   TString  GetPosition() {return fPosition;}
   void SetPosition(TString sp);

   TString  fSerialNo;
   TString  GetSerialNo() {return fSerialNo;}
   void SetSerialNo(TString sn);
// 

   VARS(Int_t,SNo)
   VARS(Int_t,Lot)
   VARS(Int_t,Wafer)
   VARS(Int_t,XPos)
   VARS(Int_t,YPos)
   VARS(Float_t,SN)
   VARS(Float_t,Temperature )
   VARS(Float_t,I0Reference)
   VARS(Float_t,IDReference )
   VARS(Float_t,VB)
   VARS(Float_t,I0)
   VARS(Float_t,VR )
   VARS(Float_t,ID)
   VARS(Float_t,ID50)
   VARS(Float_t,ID100)
   VARS(Float_t,ID200)  
   VARS(Float_t,Noise)
   VARS(Float_t,Ct)
   VARS(Float_t,Noise50)
   VARS(Float_t,Noise100)
   VARS(Float_t,Noise200)
   VARS(Float_t,Ct50)
   VARS(Float_t,par0)
   VARS(Float_t,par1)
   VARS(Float_t,VBt)
   VARS(Float_t,dVBt) 
   VARS(Float_t,dVBtpar0)
   VARS(Float_t,dVBtpar1)


#ifndef RESTRICTEDVERSION   
     //VARS(Float_t,IdOverGain45)
#endif
   VARS(Float_t,IdOverGain50)
   VARS(Float_t,IdOverGain100)
   VARS(Float_t,IdOverGain200)
   //=VARS(Float_t,Vr45)
   VARS(Float_t,Vr50)
   VARS(Float_t,Vr100) 
   VARS(Float_t,Vr200)
   VARS(Float_t,QE)
   VARS(Float_t,dMdV)
     //=VARS(Float_t,NoiseRatio50)
   VARS(Float_t,Gain_incrIDoverM)
   
//
   VARA(TArrayF,aVoltage)
   VARA(TArrayF,aDarkCurrent)
   VARA(TArrayF,aTotalCurrent)
   VARA(TArrayF,aNoise)
   VARA(TArrayF,aCapacitance)
   VARA(TArrayF,aGain)
   VARA(TArrayF,aIdarkOverGain)
   VARA(TArrayF,adMdV)
   VARA(TArrayF,aNoiseRatio)
//
   Long_t fnp1,fnp2;
   
   ClassDef(Hamamatsu,1)
};
//
class Gain : public Header {
public :
   Gain() {}
   virtual ~Gain();
   VARS(TString,SerialNo)
//

   VARS(Float_t,Vr)
   VARS(Float_t,Idark)
   VARS(Float_t,dMdV)
   VARS(Float_t,MeanTemp)
   VARS(Float_t,Vb)
     //=VARS(Float_t,QEcoefficient)
   VARS(Float_t,APDPosition) 
#ifndef RESTRICTEDVERSION   
     //=VARS(Float_t,IdOverGain10)
     //=VARS(Float_t,IdOverGain20)
#endif
   VARS(Float_t,IdOverGain50)
   VARS(Float_t,IdOverGain100)
   VARS(Float_t,IdOverGain200)
#ifndef RESTRICTEDVERSION   
     //=VARS(Float_t,Vr10)
     //=VARS(Float_t,Vr20)
#endif
   VARS(Float_t,Vr50)
   VARS(Float_t,Vr100)
   VARS(Float_t,Vr200)
//
   VARA(TArrayF,aVoltage)
   VARA(TArrayF,aItotal)
   VARA(TArrayF,aIdark)
   VARA(TArrayF,aIphoto)
   VARA(TArrayF,aGain)
   VARA(TArrayF,adMdV)
   VARA(TArrayF,aIdarkOverGain)
   VARA(TArrayF,aGaindMdV)
   VARA(TArrayF,aIpinlight)
   VARA(TArrayF,aIpindark)
   VARA(TArrayF,aGainfit)
   VARA(TArrayF,aIdarkOverGainfit)   
   ClassDef(Gain,1)   

};

class GainQE : public Header {
public:
   GainQE() {}
   virtual ~GainQE();
   VARS(TString,SerialNo)
//   
#ifndef RESTRICTEDVERSION   
   VARS(Float_t,Vr)   
   VARS(Float_t,Idark)
   VARS(Float_t,dMdV)
   VARS(Float_t,MeanTemp)
   VARS(Float_t,Vb)
     //=VARS(Float_t,QEcoefficient)
   VARS(Float_t,APDPosition)
   
   VARA(TArrayF,aVoltage)
   VARA(TArrayF,aItotal)
   VARA(TArrayF,aIdark)
   VARA(TArrayF,aIphoto)
   VARA(TArrayF,aGain)
   VARA(TArrayF,adMdV)
   VARA(TArrayF,aIdarkOverGain)
   VARA(TArrayF,aGaindMdV)
   VARA(TArrayF,aIpinlight)
   VARA(TArrayF,aIpindark)
   VARA(TArrayF,aGainfit)
   VARA(TArrayF,aIdarkOverGainfit)
#endif      
   ClassDef(GainQE,1)      
      
};


//
class QE : public Header {
public:
   QE() {}
   virtual ~QE();
   VARS(TString,SerialNo)
//
#ifndef RESTRICTEDVERSION   
     //=VARS(Float_t,BiasAPD)
     //=VARS(Float_t,APDdarkcurrent)
   VARS(Float_t,APDQE)
     //=VARS(Float_t,BiasPIN)
     //=VARS(Float_t,PINdarkcurrent)
   VARA(TArrayF,aWavelength)
   VARA(TArrayF,aAPDillCurr)
   VARA(TArrayF,aPINillCurr)
   VARA(TArrayF,aQEPIN)
   VARA(TArrayF,aQEAPD)
#endif
   ClassDef(QE,1)
   
};

class Noise : public Header {
public:
   Noise() {}
   virtual ~Noise();
   VARS(TString,SerialNo)
//
#ifndef RESTRICTEDVERSION   

   VARS(Float_t,IdOverGain50)
   VARS(Float_t,IdOverGain100)
   VARS(Float_t,ExNoise50) //VARS(Float_t,ExNoise50)
   VARS(Float_t,ExNoise100)//VARS(Float_t,ExNoise100)
   VARS(Float_t,ENCOverGain50)
   VARS(Float_t,ENCOverGain100)
//
   VARA(TArrayF,aVoltage_ill)
   VARA(TArrayF,aMeasVoltage_ill)
   VARA(TArrayF,aTotal_current)
   VARA(TArrayF,aMean_ampl_total)
   VARA(TArrayF,aSigma_fit_total)
   VARA(TArrayF,aRms_calc_total)
   VARA(TArrayF,aVoltage_dark)
   VARA(TArrayF,aMeasVoltage_dark)
   VARA(TArrayF,aDark_current)
   VARA(TArrayF,aMean_ampl_dark)
   VARA(TArrayF,aSigma_fit_dark)
   VARA(TArrayF,aRms_calc_dark)
   VARA(TArrayF,aIph)
   VARA(TArrayF,aGain)
   VARA(TArrayF,aF)
   VARA(TArrayF,aFrms)
   VARA(TArrayF,aIdOverGain)
   VARA(TArrayF,aENC)
   VARA(TArrayF,aENCOverGain)
   VARA(TArrayF,aENCrms)
   VARA(TArrayF,aENCrmsOverGain)
#endif     
   
   ClassDef(Noise,1)

};



class Capacitance : public Header {
public:
   Capacitance() {}
   virtual ~Capacitance();
   
   VARS(TString,SerialNo)
//

#ifndef RESTRICTEDVERSION   
   VARA(TArrayF,aVoltage)
   VARA(TArrayF,aCsMeasured)
   VARA(TArrayF,aCsCorrected)
   VARA(TArrayF,aRs)
   VARA(TArrayF,a1OverCs)
   VARA(TArrayF,a1OverCs2)
   VARA(TArrayF,aDopProf)
#endif

   ClassDef(Capacitance,1)
};


class Annealing : public Header {
 public:
  Annealing() {}
  virtual ~Annealing();
  
  VARS(TString, SerialNo)
  VARS(TString, Channel)
#ifndef RESTRICTEDVERSION
  VARA(TArrayF,aCurrent)
  VARA(TArrayF,aDays)
  VARA(TArrayF,aHour)
  VARA(TArrayF,aMinute)
  VARA(TArrayF,aDay)
  VARA(TArrayF,aMonth)
  VARA(TArrayF,aYear)
  VARA(TArrayF,aHV)
  VARA(TArrayF,aTemperature)
#endif

    ClassDef(Annealing,1)
    };

class BRKDPSI : public Header {
 public:
  BRKDPSI() {}
  virtual ~BRKDPSI();
  
  VARS(TString, SerialNo)
#ifndef RESTRICTEDVERSION
    VARS(Float_t,ID)
    VARS(Float_t,VB)
    VARS(Float_t,Temperature)
    VARS(Float_t,VBcorr)
    VARS(Float_t,Gain_incrIDoverM)

   VARS(Float_t,ID50)
   VARS(Float_t,ID150)
   VARS(Float_t,ID300)  

  VARA(TArrayF,aBias)
  VARA(TArrayF,aItotal)
  VARA(TArrayF,aIdark)
  VARA(TArrayF,aIphoto)
  VARA(TArrayF,aGainfit)
  VARA(TArrayF,aGaindMdV)
  VARA(TArrayF,aIdarkOverGainfit)
  VARA(TArrayF,adMdV)
#endif

    ClassDef(BRKDPSI,1)
    };

class BRKDCERN : public Header {
 public:
  BRKDCERN() {}
  virtual ~BRKDCERN();
  
  VARS(TString, SerialNo)
#ifndef RESTRICTEDVERSION
    VARS(Float_t,ID)
    VARS(Float_t,VB)
    VARS(Float_t,Temperature)
    VARS(Float_t,VBcorr)
    VARS(Float_t,Gain_incrIDoverM)

    VARS(Float_t,ID50)
    VARS(Float_t,ID150)
    VARS(Float_t,ID300)  

  VARA(TArrayF,aBias)
  VARA(TArrayF,aItotal)
  VARA(TArrayF,aIdark)
  VARA(TArrayF,aIphoto)
  VARA(TArrayF,aGainfit)
  VARA(TArrayF,aGaindMdV)
  VARA(TArrayF,aIdarkOverGainfit)
  VARA(TArrayF,adMdV)
#endif

    ClassDef(BRKDCERN,1)
    };

class GAINPSI : public Header {
 public:
  GAINPSI() {}
  virtual ~GAINPSI();
  
  VARS(TString, SerialNo)
#ifndef RESTRICTEDVERSION
  VARA(TArrayF,aBias)
  VARA(TArrayF,aItotal)
  VARA(TArrayF,aIdark)
  VARA(TArrayF,aIphoto)
  VARA(TArrayF,aGain)
  VARA(TArrayF,aGaindMdV)
  VARA(TArrayF,aIdarkOverGain)
  VARA(TArrayF,adMdV)
#endif

    ClassDef(GAINPSI,1)
    };


class GAINCERN : public Header {
 public:
  GAINCERN() {}
  virtual ~GAINCERN();
   VARS(TString,SerialNo)
//   
#ifndef RESTRICTEDVERSION   
   VARS(Float_t,Vr)   
   VARS(Float_t,Idark)
   VARS(Float_t,dMdV)
   VARS(Float_t,MeanTemp)
   VARS(Float_t,Vb)
     //=VARS(Float_t,QEcoefficient)
   VARS(Float_t,APDPosition)
   
   VARA(TArrayF,aVoltage)
   VARA(TArrayF,aItotal)
   VARA(TArrayF,aIdark)
   VARA(TArrayF,aIphoto)
   VARA(TArrayF,aGain)
   VARA(TArrayF,adMdV)
   VARA(TArrayF,aIdarkOverGain)
   VARA(TArrayF,aGaindMdV)
   VARA(TArrayF,aIpinlight)
   VARA(TArrayF,aIpindark)
   VARA(TArrayF,aGainfit)
   VARA(TArrayF,aIdarkOverGainfit)

#endif      
   ClassDef(GAINCERN,1)      
      
};

class Mnoise : public Header {
public :
  //Mnoise();
   virtual ~Mnoise();
   //   VARS(TString,Date)
   //TString  fPosition; 
   //TString  GetPosition() {return fPosition;}
   //void SetPosition(TString sp);
   TString  fSerialNo;
   TString  GetSerialNo() {return fSerialNo;}
   void SetSerialNo(TString sn);
// 
   VARS(Int_t,SNo)
   VARS(Int_t,Lot)
   VARS(Int_t,Wafer)
   VARS(Int_t,ch)
   VARS(Float_t,SN)
   VARS(Float_t,Temperature )
     //VARS(Float_t,ID)
     //VARS(Float_t,ID50)
     //VARS(Float_t,ID150)
     //VARS(Float_t,ID300)  
     //VARS(Float_t,Noise)
   VARS(Float_t,Noise1)
   VARS(Float_t,Noise50)
   VARS(Float_t,Noise150)
   VARS(Float_t,Noise300)
   
#ifndef RESTRICTEDVERSION   
#endif
     //   VARA(TArrayF,aNoise)
     //Long_t fnp1,fnp2;   
   ClassDef(Mnoise,1)
};
//
#endif



