////////////////////////////////////////////////////////////////////////
// 23.10.2000 by Andrei Dorokhov
// The program is developing and supporting by Andrei Kouznetsov
//
//
////////////////////////////////////////////////////////////////////////

#include <stdlib.h>
#include <sys/types.h>
#include <dirent.h>

#include "TROOT.h"
#include "TFile.h"
#include "TRandom.h"
#include "TTree.h"
#include "TBranch.h"
#include "TClonesArray.h"
#include "TStopwatch.h"

//#include "Measurement.h"
#include "BuildDB.h"
Bool_t doUpdate; 

//______________________________________________________________________________
int main(int argc, char **argv)
{

  if(argc<3 || argc>4) {
    cout<<"Usage:"<<endl<<"Measurement data_directory output_file [-a]"<<endl;
    return 0;
  }
  doUpdate = kFALSE;
  char directory[1000];
  if(strlen(argv[1])>1000) { 
    cout<<"data_directory name too long"<<endl;return 0;
  } 
  doUpdate=kFALSE;
  if(argc==4) if(strcmp(argv[3],"-a")==0) doUpdate=kTRUE;
  
  TROOT APDMeasurement("measurement","APD measurements");
  Int_t dimension = 999999;
  TObjArray *mHam ;
  TObjArray *mGn  ;
  TObjArray *mGa  ;
  TObjArray *mGi  ;
  TObjArray *mGQn ;
  TObjArray *mGQa ;
  TObjArray *mGQi ;
  TObjArray *mQn  ;
  TObjArray *mQa  ;
  TObjArray *mQi  ;
  TObjArray *mFn  ;
  TObjArray *mFa  ;
  TObjArray *mFi  ;
  TObjArray *mCRn ;
  TObjArray *mCRa ;
  TObjArray *mCRi ;
  TObjArray *mAni ; 
  TObjArray *mBPn ;
  TObjArray *mBPa ;
  TObjArray *mBPi ;
  TObjArray *mBCn ;
  TObjArray *mBCa ;
  TObjArray *mBCi ;
  TObjArray *mGPn ;
  TObjArray *mGPa ;
  TObjArray *mGPi ;
  TObjArray *mGCn ;
  TObjArray *mGCa ;
  TObjArray *mGCi ;
  TObjArray *mMno ;

  TFile *hfile;
  
  if(!doUpdate) {
    cout<<"Recreating DB..."<<endl;
    hfile = new TFile(argv[2],"RECREATE","APD measurements ROOT file");
  }
  else {
    cout<<"Adding to DB..."<<endl;
    hfile = new TFile(argv[2],"UPDATE","APD measurements ROOT file");
  }
  if(hfile==NULL) { cout<<"Error in file:"<< argv[2]<<endl;exit(1);}
  hfile->SetCompressionLevel(1);
  
  if(!doUpdate) {
    mHam = new TObjArray(dimension);
    mGn  = new TObjArray(dimension);
    mGa  = new TObjArray(dimension);
    mGi  = new TObjArray(dimension);
    mGQn = new TObjArray(dimension);
    mGQa = new TObjArray(dimension);
    mGQi = new TObjArray(dimension);
    mQn  = new TObjArray(dimension);
    mQa  = new TObjArray(dimension);
    mQi  = new TObjArray(dimension);
    mFn  = new TObjArray(dimension);
    mFa  = new TObjArray(dimension);
    mFi  = new TObjArray(dimension);
    mCRn = new TObjArray(dimension);
    mCRa = new TObjArray(dimension);
    mCRi = new TObjArray(dimension);
    mAni = new TObjArray(dimension); //!!
    mBPn  = new TObjArray(dimension);
    mBPa  = new TObjArray(dimension);
    mBPi  = new TObjArray(dimension);
    mBCn  = new TObjArray(dimension);
    mBCa  = new TObjArray(dimension);
    mBCi  = new TObjArray(dimension);
    mGPn  = new TObjArray(dimension);
    mGPa  = new TObjArray(dimension);
    mGPi  = new TObjArray(dimension);
    mGCn  = new TObjArray(dimension);
    mGCa  = new TObjArray(dimension);
    mGCi  = new TObjArray(dimension);
    mMno  = new TObjArray(dimension);

  }
  else {
    mHam = (TObjArray*)hfile->Get("mHam");
    mGn  = (TObjArray*)hfile->Get("mGn");
    mGa  = (TObjArray*)hfile->Get("mGa");
    mGi  = (TObjArray*)hfile->Get("mGi");
    mGQn = (TObjArray*)hfile->Get("mGQn");
    mGQa = (TObjArray*)hfile->Get("mGQa");
    mGQi = (TObjArray*)hfile->Get("mGQi");
    mQn  = (TObjArray*)hfile->Get("mQn");
    mQa  = (TObjArray*)hfile->Get("mQa");
    mQi  = (TObjArray*)hfile->Get("mQi");
    mFn  = (TObjArray*)hfile->Get("mFn");
    mFa  = (TObjArray*)hfile->Get("mFa");
    mFi  = (TObjArray*)hfile->Get("mFi");
    mCRn = (TObjArray*)hfile->Get("mCRn");
    mCRa = (TObjArray*)hfile->Get("mCRa");
    mCRi = (TObjArray*)hfile->Get("mCRi");
    mAni = (TObjArray*)hfile->Get("mAni"); //!!
    mBPn  = (TObjArray*)hfile->Get("mBPn");
    mBPa  = (TObjArray*)hfile->Get("mBPa");
    mBPi  = (TObjArray*)hfile->Get("mBPi");
    mBCn  = (TObjArray*)hfile->Get("mBCn");
    mBCa  = (TObjArray*)hfile->Get("mBCa");
    mBCi  = (TObjArray*)hfile->Get("mBCi");
    mGPn  = (TObjArray*)hfile->Get("mBGn");
    mGPa  = (TObjArray*)hfile->Get("mBGa");
    mGPi  = (TObjArray*)hfile->Get("mBGi");
    mGCn  = (TObjArray*)hfile->Get("mGCn");
    mGCa  = (TObjArray*)hfile->Get("mGCa");
    mGCi  = (TObjArray*)hfile->Get("mGCi");
    mMno  = (TObjArray*)hfile->Get("mMno");
  }       
  
  
  
  strcpy(directory,argv[1]);
  //TString pathhdb=strcat(directory,"/hamamatsu");
  //BuildDB *hdb = new BuildDB(pathhdb);
    BuildDB *hdb = new BuildDB(strcat(directory,"/r/rhamamatsu"));
  hdb->FillHamamatsu(mHam);
  //  cout<<"Write Hamamatsu data "<<endl;
  mHam->Write("mHam",TObject::kSingleKey | TObject::kOverwrite );
  //cout<<"Hamamatsu data written"<<endl;
  delete hdb;
  //cout<<"Delete Hamamatsu data "<<endl;
  
  
  // Copy data from GainQE to Gain if there is no data in Gain
  //
  /*
    for(Int_t i=0;i<dimension;i++) {
                                                                                                                                                                                                                                                                                                           if(mGn->At(i)==NULL) {
    if(mGQn->At(i)!=NULL) {
    Gain *fm = new Gain();          
            mGn->AddAt(fm,fm->GetIndex());
	    }
	    }
	    }   
  */
  
  
#ifdef RESTRICTEDVERSION 
  strcpy(directory,argv[1]);
  BuildDB *rdb = new BuildDB(strcat(directory,"/r/rgain+qe"));
  rdb->FillGain(mGn,mGa,mGi);
  delete rdb;
#endif
  
  
  strcpy(directory,argv[1]);
  //cout<<"Gain"<<endl;
  
  //TString pathgdb = strcat(directory,"/r/rgain"); //26.09.01
  //BuildDB *gdb = new BuildDB(pathgdb); //26.09.01
  BuildDB *gdb = new BuildDB(strcat(directory,"/r/rgain"));
  //cout<<"Gain1"<<endl;
  gdb->FillGain(mGn,mGa,mGi);
  //cout<<"Gain2"<<endl;
  mGn->Write("mGn",TObject::kSingleKey | TObject::kOverwrite );
  mGa->Write("mGa",TObject::kSingleKey | TObject::kOverwrite );
  mGi->Write("mGi",TObject::kSingleKey | TObject::kOverwrite );
  //delete pathgdb;
  delete gdb;
  
  strcpy(directory,argv[1]);
  //TString pathgqdb = strcat(directory,"/r/rgain+qe"); //26.09.01
  //BuildDB *gqdb = new BuildDB(pathgqdb); //26.09.01
   BuildDB *gqdb = new BuildDB(strcat(directory,"/r/rgain+qe"));
  gqdb->FillGainQE(mGQn,mGQa,mGQi);
  mGQn->Write("mGQn",TObject::kSingleKey | TObject::kOverwrite );
  mGQa->Write("mGQa",TObject::kSingleKey | TObject::kOverwrite );
  mGQi->Write("mGQi",TObject::kSingleKey | TObject::kOverwrite );
  delete gqdb;
  
  strcpy(directory,argv[1]);
  BuildDB *qedb = new BuildDB(strcat(directory,"/r/rqe"));
  qedb->FillQE(mQn,mQa,mQi);
  mQn->Write("mQn",TObject::kSingleKey | TObject::kOverwrite );
  mQa->Write("mQa",TObject::kSingleKey | TObject::kOverwrite );
  mQi->Write("mQi",TObject::kSingleKey | TObject::kOverwrite );
  delete qedb;
  
  strcpy(directory,argv[1]);
  BuildDB *ndb = new BuildDB(strcat(directory,"/r/rnoise"));
  ndb->FillNoise(mFn,mFa,mFi);
  mFn->Write("mFn",TObject::kSingleKey | TObject::kOverwrite );
  mFa->Write("mFa",TObject::kSingleKey | TObject::kOverwrite );
  mFi->Write("mFi",TObject::kSingleKey | TObject::kOverwrite );
  delete ndb;
  
  strcpy(directory,argv[1]);
  BuildDB *cdb = new BuildDB(strcat(directory,"/r/rcapacitance"));
  cdb->FillCapacitance(mCRn,mCRa,mCRi);
  mCRn->Write("mCRn",TObject::kSingleKey | TObject::kOverwrite );
  mCRa->Write("mCRa",TObject::kSingleKey | TObject::kOverwrite );
  mCRi->Write("mCRi",TObject::kSingleKey | TObject::kOverwrite );
  delete cdb;
  
  strcpy(directory,argv[1]);
  BuildDB *adb= new BuildDB(strcat(directory,"/r/rpechka"));
  adb->FillAnnealing(mAni);
  mAni->Write("mAni",TObject::kSingleKey | TObject::kOverwrite );
  delete adb;

  strcpy(directory,argv[1]);
  BuildDB *bpdb= new BuildDB(strcat(directory,"/r/rbrkdpsi"));
  bpdb->FillBRKDPSI(mBPn,mBPa,mBPi);
  mBPn->Write("mBPn",TObject::kSingleKey | TObject::kOverwrite );
  mBPa->Write("mBPa",TObject::kSingleKey | TObject::kOverwrite );
  mBPi->Write("mBPi",TObject::kSingleKey | TObject::kOverwrite );
  delete bpdb;

  strcpy(directory,argv[1]);
  BuildDB *bcdb= new BuildDB(strcat(directory,"/r/rbrkdcern"));
  bcdb->FillBRKDCERN(mBCn,mBCa,mBCi);
  mBCn->Write("mBCn",TObject::kSingleKey | TObject::kOverwrite );
  mBCa->Write("mBCa",TObject::kSingleKey | TObject::kOverwrite );
  mBCi->Write("mBCi",TObject::kSingleKey | TObject::kOverwrite );
  delete bcdb;

  strcpy(directory,argv[1]);
  BuildDB *gpdb= new BuildDB(strcat(directory,"/r/rgainpsi"));
  gpdb->FillGAINPSI(mGPn,mGPa,mGPi);
  mGPn->Write("mGPn",TObject::kSingleKey | TObject::kOverwrite );
  mGPa->Write("mGPa",TObject::kSingleKey | TObject::kOverwrite );
  mGPi->Write("mGPi",TObject::kSingleKey | TObject::kOverwrite );
  delete gpdb;

  strcpy(directory,argv[1]);
  BuildDB *gcdb= new BuildDB(strcat(directory,"/r/rgaincern"));
  gcdb->FillGAINCERN(mGCn,mGCa,mGCi);
  mGCn->Write("mGCn",TObject::kSingleKey | TObject::kOverwrite );
  mGCa->Write("mGCa",TObject::kSingleKey | TObject::kOverwrite );
  mGCi->Write("mGCi",TObject::kSingleKey | TObject::kOverwrite );
  delete gcdb;

  strcpy(directory,argv[1]);
  //TString pathhdb=strcat(directory,"/hamamatsu");
  //BuildDB *hdb = new BuildDB(pathhdb);
  BuildDB *mndb = new BuildDB(strcat(directory,"/r/rNoise"));
  mndb->FillMnoise(mMno);
  //  cout<<"Write Hamamatsu data "<<endl;
  mMno->Write("mMno",TObject::kSingleKey | TObject::kOverwrite );
  //cout<<"Hamamatsu data written"<<endl;
  delete mndb;
  //cout<<"Delete Hamamatsu data "<<endl;
 
  hfile->Close();
  delete hfile;
  
  return 0;
}












