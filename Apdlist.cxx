#include "Apdlist.h"

#include <TMethodCall.h>
#include <TGFileDialog.h>
#include <TLatex.h>
#include <TOrdCollection.h>

//TString PlotSelected::fullnomer;

class TMyGraph : public TGraph {
public:
   TGWindow *fCanvas;
   TMyGraph(Int_t n, Float_t *x, Float_t *y, TGWindow *c): TGraph( n, x, y){fCanvas = c;}
   TMyGraph(Int_t n, Float_t *x, Float_t *y): TGraph( n, x, y){}
   //virtual ~TMyGraph() {~TGraph(); }
   void Dump() const {
      TGFileInfo fi;
      const char* s[] = {"Text file", "*.txt", 0, 0};
      fi.fFileTypes = s;
      /*TGFileDialog* fd =*/ new TGFileDialog(gClient->GetRoot(),gClient->GetRoot(),kFDSave, &fi);
      if(fi.fFilename!=NULL) {
         FILE *fp = fopen(fi.fFilename,"wt+");
      for(Int_t i=0;i<fNpoints;i++) fprintf(fp,"%d,%f,%f\n",i+1,fX[i],fY[i]);
         fclose(fp);
         cout<<"Saved to: " <<fi.fFilename<<endl;
      }
      
      //if(fd) delete fd;
   }
  void SetName() {
    //    Int_t k=fNpoints>>1;
    //Int_t k=GetN()>>1;
    TLatex *tex = new TLatex(0.5,0.5,GetTitle());
    tex->SetNDC();
    //TLatex *tex = new TLatex( fX[k],fY[k],GetTitle());
    //TLatex *tex = new TLatex( xmax,ymax,GetTitle());
    tex->SetTextSize(0.0309735);
    tex->SetLineWidth(2);
    tex->Draw();
    //->Modified();
    //Plot1->cd();
  }
};


class TMyH2F : public TH2F {
public:
  TGWindow *fCanvas;
  TMyH2F(const char* name, const char* title, Int_t nbinsx, Axis_t xlow, Axis_t xup, Int_t nbinsy, Axis_t ylow, Axis_t yup,
	 TGWindow *c ): TH2F(name, title, nbinsx, xlow, xup, nbinsy, ylow, yup){fCanvas = c;}
  TMyH2F(const char* name, const char* title, Int_t nbinsx, Axis_t xlow, Axis_t xup, Int_t nbinsy, Axis_t ylow, Axis_t yup): TH2F(name,
	     title, nbinsx, xlow, xup, nbinsy, ylow, yup){}
  //virtual ~TMyGraph() {~TGraph(); }
   void Dump() const {
      TGFileInfo fi;
      const char *s[] = {"Text file","*.txt",0,0};
      fi.fFileTypes= s;
      /*TGFileDialog* fd =*/ new TGFileDialog(gClient->GetRoot(),gClient->GetRoot(),kFDSave,&fi);
      if(fi.fFilename!=NULL) {
         FILE *fp = fopen(fi.fFilename,"wt+");
	 for(Int_t i=1;i<14;i++) {
	    for(Int_t j=1;j<14;j++) fprintf(fp,"%d,%d,%f\n", i, j, GetBinContent(i,j));
	 }
         fclose(fp);
         cout<<"Saved to: " <<fi.fFilename<<endl;
      }
      
      //if(fd) delete fd;
   }

};

Int_t isApdDead(Header *h);
extern Bool_t needSelection;
extern Bool_t loadAPDListFromFile;
extern TString loadAPDListFile;

void Apdlist_Fill(TGListBox *fListBoxLot,TGListBox *fListBoxWafer,TGListBox *fListBoxAPD, TObjArray *map)
{
   if (!map) return;
   
   char sl[20];
   char sw[20];
   char sa[20];
   
   TOrdCollection cl;
   TOrdCollection cw;
   TOrdCollection ca;
   
   cl.SetOwner();
   cw.SetOwner();
   ca.SetOwner();
   
   for (TObject* o : *map) {
     Header* h = (Header*) o;
     const char* s = h->fFullSerialNo.Data();
     
     sprintf(sl,"%c%c", s[0],s[1]);
     sprintf(sw,"%c%c %c%c", s[0],s[1], s[2],s[3]);
     sprintf(sa,"%c%c%c%c %c%c%c%c%c%c", s[0],s[1],s[2],s[3], s[4],s[5],s[6],s[7],s[8],s[9]);
     TObjString* osl = new TObjString(sl);
     TObjString* osw = new TObjString(sw);
     TObjString* osa = new TObjString(sa);
     if(cl.IndexOf(osl) ==-1) cl.AddLast(osl);
     if(cw.IndexOf(osw) ==-1) cw.AddLast(osw);
     ca.AddLast(osa);     
   }
   
   Int_t i=0;
   for (TObject* o : cl) {
      fListBoxLot->AddEntry(((TObjString*)o)->GetString(), i);
      i++;
   }

   i=0;
   for (TObject* o : cw) {
      fListBoxWafer->AddEntry(((TObjString*)o)->GetString(), i);
      i++;
   }

   for (TObject* o : ca) {
      TString x = ((TObjString*)o)->GetString();
      const int idx = TString(x(5,6)).Atoi();
      //cout << "x=" << x << " idx=" << idx << endl;
      fListBoxAPD->AddEntry(x, idx);
   }
}


Varlist::Varlist(TString cn,TGListBox *lb) {
   fList = lb;

   if(!cn.CompareTo("Hamamatsu"))clmap = (new Hamamatsu())->Class();
   if(!cn.CompareTo("Gain"))clmap = (new Gain())->Class();
   if(!cn.CompareTo("QE"))clmap = (new QE())->Class();
   if(!cn.CompareTo("Noise"))clmap = (new Noise())->Class();
   if(!cn.CompareTo("GainQE"))clmap = (new GainQE())->Class();
   if(!cn.CompareTo("Capacitance"))clmap = (new Capacitance())->Class();
   if(!cn.CompareTo("Annealing"))clmap =(new Annealing())->Class();
   if(!cn.CompareTo("BRKDPSI"))clmap =(new BRKDPSI())->Class();
   if(!cn.CompareTo("BRKDCERN"))clmap =(new BRKDCERN())->Class();
   if(!cn.CompareTo("GAINPSI"))clmap =(new GAINPSI())->Class();
   if(!cn.CompareTo("GAINCERN"))clmap =(new GAINCERN())->Class();
   if(!cn.CompareTo("Mnoise"))clmap = (new Mnoise())->Class();
   if(clmap!=NULL) {
      clmaplist = clmap->GetListOfDataMembers();
      clmaplistiter = new TListIter(clmaplist);
   }
   i=0;
      
   
}


Varlist::Varlist(TGListBox *lb, TString cn) {
   fList = lb;
   TObject *o;
   TListIter *iter;
   Int_t i=0;

   TList *hl  = ((new Hamamatsu())->Class())->GetListOfDataMembers();
   TList *gl  = ((new Gain())->Class())->GetListOfDataMembers();
   TList *ql  = ((new QE())->Class())->GetListOfDataMembers();
   TList *nl  = ((new Noise())->Class())->GetListOfDataMembers();
   TList *gql = ((new GainQE())->Class())->GetListOfDataMembers();
   TList *cl  = ((new Capacitance())->Class())->GetListOfDataMembers();
   TList *pl  = ((new Annealing())->Class())->GetListOfDataMembers();
   TList *bpl  = ((new BRKDPSI())->Class())->GetListOfDataMembers();
   TList *bcl  = ((new BRKDCERN())->Class())->GetListOfDataMembers();
   TList *gpl  = ((new GAINPSI())->Class())->GetListOfDataMembers();
   TList *gcl  = ((new GAINCERN())->Class())->GetListOfDataMembers();
   TList *mnl  = ((new Mnoise())->Class())->GetListOfDataMembers();
   iter = new TListIter(hl);
   while((o=iter->Next())!=0) {
      TDataMember *dmb = (TDataMember *)o;
      TString varname = dmb->GetName();
      TString typname = dmb->GetFullTypeName(); 
      if(typname.EndsWith("t_t") && varname.Length()>2) {      
      	 TString s = varname(1,varname.Length()-1)+"@Ham";
         if(fList) fList->AddEntry(s,i);      	       	
      	 i++; 
      }                      
   }

   if(iter) delete iter;
   iter = new TListIter(gl);
   while((o=iter->Next())!=0) {
      TDataMember *dmb = (TDataMember *)o;
      TString varname = dmb->GetName();
      TString typname = dmb->GetFullTypeName(); 
      if(typname.EndsWith("t_t") && varname.Length()>2) {      
      	 TString s = varname(1,varname.Length()-1)+"@Gn";
         if(fList) fList->AddEntry(s,i);      	       	
      	 i++; 
      }                      
   }
   if(iter)delete iter;


   iter = new TListIter(gl);
   while((o=iter->Next())!=0) {
      TDataMember *dmb = (TDataMember *)o;
      TString varname = dmb->GetName();
      TString typname = dmb->GetFullTypeName(); 
      if(typname.EndsWith("t_t") && varname.Length()>2) {      
      	 TString s = varname(1,varname.Length()-1)+"@Gi";
         if(fList) fList->AddEntry(s,i);      	       	
      	 i++; 
      }                      
   }
   if(iter)delete iter;

   iter = new TListIter(gl);
   while((o=iter->Next())!=0) {
      TDataMember *dmb = (TDataMember *)o;
      TString varname = dmb->GetName();
      TString typname = dmb->GetFullTypeName(); 
      if(typname.EndsWith("t_t") && varname.Length()>2) {      
      	 TString s = varname(1,varname.Length()-1)+"@Ga";
         if(fList) fList->AddEntry(s,i);      	       	
      	 i++; 
      }                      
   }
   if(iter)delete iter;

   iter = new TListIter(ql);
   while((o=iter->Next())!=0) {
      TDataMember *dmb = (TDataMember *)o;
      TString varname = dmb->GetName();
      TString typname = dmb->GetFullTypeName(); 
      if(typname.BeginsWith("Float_t") && varname.Length()>2) {      
      	 TString s = varname(1,varname.Length()-1)+"@Qn";
         if(fList) fList->AddEntry(s,i);      	       	
      	 i++; 
      }                      
   }
   if(iter)delete iter;

   iter = new TListIter(ql);
   while((o=iter->Next())!=0) {
      TDataMember *dmb = (TDataMember *)o;
      TString varname = dmb->GetName();
      TString typname = dmb->GetFullTypeName(); 
      if(typname.BeginsWith("Float_t") && varname.Length()>2) {      
      	 TString s = varname(1,varname.Length()-1)+"@Qi";
         if(fList) fList->AddEntry(s,i);      	       	
      	 i++; 
      }                      
   }
   if(iter)delete iter;

   iter = new TListIter(ql);
   while((o=iter->Next())!=0) {
      TDataMember *dmb = (TDataMember *)o;
      TString varname = dmb->GetName();
      TString typname = dmb->GetFullTypeName(); 
      if(typname.BeginsWith("Float_t") && varname.Length()>2) {      
      	 TString s = varname(1,varname.Length()-1)+"@Qa";
         if(fList) fList->AddEntry(s,i);      	       	
      	 i++; 
      }                      
   }
   if(iter)delete iter;

   iter = new TListIter(nl);
   while((o=iter->Next())!=0) {
      TDataMember *dmb = (TDataMember *)o;
      TString varname = dmb->GetName();
      TString typname = dmb->GetFullTypeName(); 
      if(typname.EndsWith("t_t") && varname.Length()>2) {      
      	 TString s = varname(1,varname.Length()-1)+"@Fn";
         if(fList) fList->AddEntry(s,i);      	       	
      	 i++; 
      }                      
   }
   if(iter)delete iter;

   iter = new TListIter(nl);
   while((o=iter->Next())!=0) {
      TDataMember *dmb = (TDataMember *)o;
      TString varname = dmb->GetName();
      TString typname = dmb->GetFullTypeName(); 
      if(typname.EndsWith("t_t") && varname.Length()>2) {      
      	 TString s = varname(1,varname.Length()-1)+"@Fi";
         if(fList) fList->AddEntry(s,i);      	       	
      	 i++; 
      }                      
   }
   if(iter)delete iter;

   iter = new TListIter(nl);
   while((o=iter->Next())!=0) {
      TDataMember *dmb = (TDataMember *)o;
      TString varname = dmb->GetName();
      TString typname = dmb->GetFullTypeName(); 
      if(typname.EndsWith("t_t") && varname.Length()>2) {      
      	 TString s = varname(1,varname.Length()-1)+"@Fa";
         if(fList) fList->AddEntry(s,i);      	       	
      	 i++; 
      }                      
   }
   if(iter)delete iter;

   iter = new TListIter(gql);
   while((o=iter->Next())!=0) {
      TDataMember *dmb = (TDataMember *)o;
      TString varname = dmb->GetName();
      TString typname = dmb->GetFullTypeName(); 
      if(typname.EndsWith("t_t") && varname.Length()>2) {      
      	 TString s = varname(1,varname.Length()-1)+"@GQn";
         if(fList) fList->AddEntry(s,i);      	       	
      	 i++; 
      }                      
   }
   if(iter)delete iter;

   iter = new TListIter(gql);
   while((o=iter->Next())!=0) {
      TDataMember *dmb = (TDataMember *)o;
      TString varname = dmb->GetName();
      TString typname = dmb->GetFullTypeName(); 
      if(typname.EndsWith("t_t") && varname.Length()>2) {      
      	 TString s = varname(1,varname.Length()-1)+"@GQi";
         if(fList) fList->AddEntry(s,i);      	       	
      	 i++; 
      }                      
   }
   if(iter)delete iter;

   iter = new TListIter(gql);
   while((o=iter->Next())!=0) {
      TDataMember *dmb = (TDataMember *)o;
      TString varname = dmb->GetName();
      TString typname = dmb->GetFullTypeName(); 
      if(typname.EndsWith("t_t") && varname.Length()>2) {      
      	 TString s = varname(1,varname.Length()-1)+"@GQa";
         if(fList) fList->AddEntry(s,i);      	       	
      	 i++; 
      }                      
   }
   if(iter)delete iter;

   iter = new TListIter(cl);
   while((o=iter->Next())!=0) {
      TDataMember *dmb = (TDataMember *)o;
      TString varname = dmb->GetName();
      TString typname = dmb->GetFullTypeName(); 
      if(typname.EndsWith("t_t") && varname.Length()>2) {      
      	 TString s = varname(1,varname.Length()-1)+"@CRn";
         if(fList) fList->AddEntry(s,i);      	       	
      	 i++; 
      }                      
   }
   if(iter) delete iter;

   iter = new TListIter(cl);
   while((o=iter->Next())!=0) {
      TDataMember *dmb = (TDataMember *)o;
      TString varname = dmb->GetName();
      TString typname = dmb->GetFullTypeName(); 
      if(typname.EndsWith("t_t") && varname.Length()>2) {      
      	 TString s = varname(1,varname.Length()-1)+"@CRi";
         if(fList) fList->AddEntry(s,i);      	       	
      	 i++; 
      }                      
   }
   if(iter) delete iter;

   iter = new TListIter(cl);
   while((o=iter->Next())!=0) {
      TDataMember *dmb = (TDataMember *)o;
      TString varname = dmb->GetName();
      TString typname = dmb->GetFullTypeName(); 
      if(typname.EndsWith("t_t") && varname.Length()>2) {      
      	 TString s = varname(1,varname.Length()-1)+"@CRa";
         if(fList) fList->AddEntry(s,i);      	       	
      	 i++; 
      }                      
   }
   if(iter) delete iter;

   iter = new TListIter(pl);
   while((o=iter->Next())!=0) {
      TDataMember *dmb = (TDataMember *)o;
      TString varname = dmb->GetName();
      TString typname = dmb->GetFullTypeName(); 
      if(typname.EndsWith("t_t") && varname.Length()>2) {      
      	 TString s = varname(1,varname.Length()-1)+"@Ani";
         if(fList) fList->AddEntry(s,i);      	       	
      	 i++;
      }                      
   }
   if(iter) delete iter;

   iter = new TListIter(bpl);
   while((o=iter->Next())!=0) {
      TDataMember *dmb = (TDataMember *)o;
      TString varname = dmb->GetName();
      TString typname = dmb->GetFullTypeName(); 
      if(typname.EndsWith("t_t") && varname.Length()>2) {      
	TString s = varname(1,varname.Length()-1)+"@BPn";
	if(fList) fList->AddEntry(s,i);      	       	
	i++;
      }                      
   }
   if(iter) delete iter;

   iter = new TListIter(bpl);
   while((o=iter->Next())!=0) {
      TDataMember *dmb = (TDataMember *)o;
      TString varname = dmb->GetName();
      TString typname = dmb->GetFullTypeName(); 
      if(typname.EndsWith("t_t") && varname.Length()>2) {      
	TString s = varname(1,varname.Length()-1)+"@BPi";
	if(fList) fList->AddEntry(s,i);      	       	
	i++;
      }                      
   }
   if(iter) delete iter;

   iter = new TListIter(bpl);
   while((o=iter->Next())!=0) {
      TDataMember *dmb = (TDataMember *)o;
      TString varname = dmb->GetName();
      TString typname = dmb->GetFullTypeName(); 
      if(typname.EndsWith("t_t") && varname.Length()>2) {      
	TString s = varname(1,varname.Length()-1)+"@BPa";
	if(fList) fList->AddEntry(s,i);      	       	
	i++;
      }                      
   }
   if(iter) delete iter;


   iter = new TListIter(bcl);
   while((o=iter->Next())!=0) {
      TDataMember *dmb = (TDataMember *)o;
      TString varname = dmb->GetName();
      TString typname = dmb->GetFullTypeName(); 
      if(typname.EndsWith("t_t") && varname.Length()>2) {      
	TString s = varname(1,varname.Length()-1)+"@BCn";
	if(fList) fList->AddEntry(s,i);      	       	
	i++;
      }                      
   }
   if(iter) delete iter;

   iter = new TListIter(bcl);
   while((o=iter->Next())!=0) {
      TDataMember *dmb = (TDataMember *)o;
      TString varname = dmb->GetName();
      TString typname = dmb->GetFullTypeName(); 
      if(typname.EndsWith("t_t") && varname.Length()>2) {      
	TString s = varname(1,varname.Length()-1)+"@BCi";
	if(fList) fList->AddEntry(s,i);      	       	
	i++;
      }                      
   }
   if(iter) delete iter;

   iter = new TListIter(bcl);
   while((o=iter->Next())!=0) {
      TDataMember *dmb = (TDataMember *)o;
      TString varname = dmb->GetName();
      TString typname = dmb->GetFullTypeName(); 
      if(typname.EndsWith("t_t") && varname.Length()>2) {      
	TString s = varname(1,varname.Length()-1)+"@BCa";
	if(fList) fList->AddEntry(s,i);      	       	
	i++;
      }                      
   }
   if(iter) delete iter;


   iter = new TListIter(gpl);
   while((o=iter->Next())!=0) {
      TDataMember *dmb = (TDataMember *)o;
      TString varname = dmb->GetName();
      TString typname = dmb->GetFullTypeName(); 
      if(typname.EndsWith("t_t") && varname.Length()>2) {      
	TString s = varname(1,varname.Length()-1)+"@GP";
	if(fList) fList->AddEntry(s,i);      	       	
	i++;
      }                      
   }
   if(iter) delete iter;

   iter = new TListIter(gcl);
   while((o=iter->Next())!=0) {
     TDataMember *dmb = (TDataMember *)o;
     TString varname = dmb->GetName();
     TString typname = dmb->GetFullTypeName(); 
     if(typname.EndsWith("t_t") && varname.Length()>2) {      
       TString s = varname(1,varname.Length()-1)+"@GCn";
       if(fList) fList->AddEntry(s,i);      	       	
       i++; 
     }                      
   }
   if(iter) delete iter;

   iter = new TListIter(gcl);
   while((o=iter->Next())!=0) {
     TDataMember *dmb = (TDataMember *)o;
     TString varname = dmb->GetName();
     TString typname = dmb->GetFullTypeName(); 
     if(typname.EndsWith("t_t") && varname.Length()>2) {      
       TString s = varname(1,varname.Length()-1)+"@GCi";
       if(fList) fList->AddEntry(s,i);      	       	
       i++; 
     }                      
   }
   if(iter) delete iter;

   iter = new TListIter(gcl);
   while((o=iter->Next())!=0) {
     TDataMember *dmb = (TDataMember *)o;
     TString varname = dmb->GetName();
     TString typname = dmb->GetFullTypeName(); 
     if(typname.EndsWith("t_t") && varname.Length()>2) {      
       TString s = varname(1,varname.Length()-1)+"@GCa";
       if(fList) fList->AddEntry(s,i);      	       	
       i++; 
     }                      
   }
   if(iter) delete iter;

   iter = new TListIter(mnl);
   while((o=iter->Next())!=0) {
      TDataMember *dmb = (TDataMember *)o;
      TString varname = dmb->GetName();
      TString typname = dmb->GetFullTypeName(); 
      if(typname.EndsWith("t_t") && varname.Length()>2) {      
      	 TString s = varname(1,varname.Length()-1)+"@Mno";
         if(fList) fList->AddEntry(s,i);      	       	
      	 i++; 
      }                      
   }

   if(iter) delete iter;

   fList->Select(0);

   
}


Varlist::~Varlist() {
   if(clmaplistiter) delete clmaplistiter;

}

void Varlist::FillA() {
   i=0;
   if(clmaplist==NULL || clmaplistiter==NULL) return;
   while((o=clmaplistiter->Next())!=0) {
      TDataMember *dmb = (TDataMember *)o;
      TString classname = dmb->ClassName();
      TString varname = dmb->GetName();
      TString typname = dmb->GetFullTypeName(); 
      if(typname.BeginsWith("TArray") && varname.Length()>2) {      
      	 TString s = varname(2,varname.Length()-1);
         if(fList) fList->AddEntry(s,i);      	       	
      	 i++; 
      }                      
      
   }
   fList->Select(0);
}

void Varlist::FillS() {
   i=0;
   if(clmaplist==NULL || clmaplistiter==NULL) return;
   while((o=clmaplistiter->Next())!=0) {
      TDataMember *dmb = (TDataMember *)o;
      TString classname = dmb->ClassName();
      TString varname = dmb->GetName();
      TString typname = dmb->GetFullTypeName(); 
      if(typname.EndsWith("t_t") && varname.Length()>2) {      
      	 TString s = varname(1,varname.Length()-1);
         if(fList) fList->AddEntry(s,i);      	       	
      	 i++; 
      }        
   }   
   fList->Select(0);
}

PlotSelected::PlotSelected(TGListBox *fListBoxLot,TGListBox *fListBoxWafer,
      	             	   TGListBox *fListBoxAPD,TGListBox *fListX, 
			   TGListBox *fListY, TObjArray *fMap,TObjArray *fMapFile) {
   char str1[80]={0};
   char str2[80]={0};
   char str3[80]={0};
   
   Int_t i=0;
   TObject *o;
   //fSize = 0;

   TList *listLot = new TList();
   fListBoxLot->GetSelectedEntries(listLot);
   TObjArray *stringLot = new TObjArray();
   TListIter *ilistLot = new TListIter(listLot);
   if(ilistLot) {
      while((o=ilistLot->Next())!=0) {      
	 stringLot->Add(new TObjString(((TGTextLBEntry*)o)->GetText()->GetString()));
      }
      delete ilistLot;
   }
   if(listLot)delete listLot;   
   
   TList *listWafer = new TList();
   fListBoxWafer->GetSelectedEntries(listWafer);
   TObjArray *stringWafer = new TObjArray();
   TListIter *ilistWafer = new TListIter(listWafer);
   if(ilistWafer) {
      while((o=ilistWafer->Next())!=0) {      
	 stringWafer->Add(new TObjString(((TGTextLBEntry*)o)->GetText()->GetString()));
      }
      delete ilistWafer;
   }
   if(listWafer)delete listWafer;   
   
   TObjArray *stringAPD = new TObjArray();
   
      {
	    TList listAPD;
	    fListBoxAPD->GetSelectedEntries(&listAPD); 
	    TListIter ilistAPD(&listAPD);
      TObject *oo;
	       while((oo=ilistAPD.Next())) {      
		  const TString fullnomer = ((TGTextLBEntry*)oo)->GetText()->GetString();
		  //cout<<fullnomer<<endl;
		  stringAPD->Add(new TObjString(fullnomer));
	       }
      }
   
   if(fListX->GetSelectedEntry()==NULL || fListY->GetSelectedEntry()==NULL) {
      fListX->Select(0);
      fListY->Select(0);
   }

   fXcoord = ((((TGTextLBEntry *)fListX->GetSelectedEntry())->GetText())->GetString()); //fXcord dlya nazv.
   fYcoord = ((((TGTextLBEntry *)fListY->GetSelectedEntry())->GetText())->GetString());
   fXcoord = "fa" + fXcoord;
   fYcoord = "fa" + fYcoord;
   
   if (!fMap) return;
   
      TObjArray arr;
      TObjArrayIter imap(fMap);
      
      if(!loadAPDListFromFile) {
	 while((o=imap.Next())) {   
	    const char* str0 = (((Header*)o)->fFullSerialNo).Data();

	    sprintf(str1,"%c%c%c%c %c%c%c%c%c%c",str0[0],str0[1],str0[2],str0[3],str0[4],
      	             	      	        	 str0[5],str0[6],str0[7],str0[8],str0[9]);          
	    sprintf(str2,"%c%c",str0[0],str0[1]);       
	    sprintf(str3,"%c%c %c%c",str0[0],str0[1],str0[2],str0[3]);       
	    if(stringLot->Contains(str2)!=0) {
      	       arr.Add(o);
	    }    
	    else if(stringWafer->Contains(str3)!=0) {
      	       arr.Add(o);
	    } 
	    else if(stringAPD->Contains(str1)!=0) {
      	       arr.Add(o);
	    }            
	 }
      }
      else {
      	 TObjArrayIter imapfile(fMapFile);
      	 TObjArray listfile;
         listfile.SetOwner();
	 while((o=imapfile.Next())) {   
	    listfile.AddLast(new TObjString(((Header*)o)->fFullSerialNo));
      	 }
	
	 while((o=imap.Next())) {   
	    const char* str0 = (((Header*)o)->fFullSerialNo).Data();
	    if(listfile.Contains(str0)) {
	       sprintf(str1,"%c%c%c%c %c%c%c%c%c%c",str0[0],str0[1],str0[2],str0[3],str0[4],
      	             	      	        	    str0[5],str0[6],str0[7],str0[8],str0[9]);          
	       //  cout<<"slow6.555"<<endl; 
	       sprintf(str2,"%c%c",str0[0],str0[1]);       
	       sprintf(str3,"%c%c %c%c",str0[0],str0[1],str0[2],str0[3]);       
	       if(stringLot->Contains(str2)!=0) {
      		  arr.Add(o);
	       }    
	       else if(stringWafer->Contains(str3)!=0) {
      		  arr.Add(o);
	       } 
	       else if(stringAPD->Contains(str1)!=0) {
      		  arr.Add(o);
	       }         
	    } 
	 }
      }
      //      cout<<"slow7"<<endl; 
      delete stringLot;
      delete stringWafer;
      delete stringAPD;      
      arr.Compress();
      fSize = arr.GetEntries(); 

      fObjarr = new TObject* [fSize];      
      for(i=0;i<fSize;i++) {
           fObjarr[i] = arr.At(i);
      }
}

PlotSelected::~PlotSelected() {
   if(fObjarr) delete fObjarr;   
}

void PlotSelected::SetTitle(TGraph *g,TString fmap) {
   TH1F *h = g->GetHistogram();
//   h->SetTitle(fYmap+" vs "+fXmap);
   Header header;
   TString xtit = (fXcoord(2,fXcoord.Length())) +(header.GetUnits(fXcoord,fmap)); // nazv. osi
   TString ytit = (fYcoord(2,fYcoord.Length())) +(header.GetUnits(fYcoord,fmap));
   if (xtit=="Days " && ytit=="Current "){
     xtit="Time [days]";
     ytit="Dark Current [nA]";
     }

   h->GetXaxis()->SetTitle(xtit);
   h->GetXaxis()->CenterTitle(kTRUE);
   h->GetYaxis()->SetTitle(ytit);
   h->GetYaxis()->CenterTitle(kTRUE);
   h->GetYaxis()->SetTitleOffset(1.2);
}



void PlotSelected::FillGraphs( TString fType, TString fExtendedType, TObjArray *fGraphs, 
      	             	       Int_t fSkip, Int_t fSkipFirst)
{
   if (fSkip < 0) fSkip = 0;
   if (fSkipFirst < 0) fSkipFirst = 0;
   
   // hide last point, TODO: why?
   if (!fType.CompareTo("Hamamatsu")) fSkip += 1;
   
   for (Int_t i = 0; i < fSize; ++i) {
      TObject* obj = fObjarr[i];
      TClass* cl = obj->IsA();
      
      TArrayF* fXarr = 0;
      TDataMember* dm = cl->GetDataMember(fXcoord);
      TString type = dm->GetFullTypeName();
      if(type.BeginsWith("TArrayF")) {
        TMethodCall* getter = dm->GetterMethod();
        getter->Execute(obj, "", (Longptr_t&) fXarr);
      }
      
      TArrayF* fYarr = 0;
      dm = cl->GetDataMember(fYcoord);
      type = dm->GetFullTypeName();
      if(type.BeginsWith("TArrayF")) {
        TMethodCall* getter = dm->GetterMethod();
        getter->Execute(obj, "", (Longptr_t&) fYarr);
      }
      
      const TString sernum = ((Header*)obj)->fFullSerialNo;
      
      if (fXarr == 0 || fYarr == 0) continue;
        
  int npoints = fXarr->GetSize()-fSkip-fSkipFirst;
  int nstart = fSkipFirst;
  
  if (npoints < 0) {
    cout << "invalid skip points" << endl;
    npoints = 0;
    nstart = 0;
  }
  
    TMyGraph* gr = new TMyGraph(npoints, &(fXarr->GetArray()[nstart]), &(fYarr->GetArray()[nstart]));
 
	 gr->SetTitle(sernum);

      	 Int_t color =(fGraphs->GetEntries())%32;

	 if(color==0 ) color=33; 
	 if(color==1 ) color=34; 


//Mark Dead APDs
      	 if(needSelection) {
      	    Int_t res = isApdDead((Header*)obj);
	    if(res==1) color=2 ;
	    if(res==0) color=1 ;
	    if(res==-1) color=3 ;
	 }
//
	 gr->SetLineColor(color);

      
	 Float_t maxx=(gr->GetX())[0],minx=(gr->GetX())[0],maxy=(gr->GetY())[0],miny=(gr->GetY())[0];      
	 for(Int_t ix=1;ix<gr->GetN();ix++) {
      	    if(maxx<gr->GetX()[ix]) maxx=gr->GetX()[ix];      	 
      	    if(minx>gr->GetX()[ix]) minx=gr->GetX()[ix];      	 
      	    if(maxy<gr->GetY()[ix]) maxy=gr->GetY()[ix];      	 
      	    if(miny>gr->GetY()[ix]) miny=gr->GetY()[ix];      	 
	 }
	 if(i==0) {
	    fxa[0] = minx;
	    fxa[1] = maxx;
	    fya[0] = miny;
	    fya[1] = maxy;
	 }
	 else {
      	    if(fxa[0]>minx) fxa[0]=minx;  
      	    if(fxa[1]<maxx) fxa[1]=maxx;  
      	    if(fya[0]>miny) fya[0]=miny;  
      	    if(fya[1]<maxy) fya[1]=maxy;  
	 }      
	 fGraphs->Add(gr);          
   }

}


///////
HistSelected::HistSelected(TGListBox *fListBoxLot,TGListBox *fListBoxWafer,
      	             	   TGListBox *fListBoxAPD,TGListBox *fListX, 
			   TGListBox *fListY, TGListBox *fListZ,TObjArray *fMap)                    {

//   Float_t xmax=-1.0e30,xmin=1.0e30;
//   Float_t ymax=-1.0e30,ymin=1.0e30;

   index = 0;
   fXunits=" ";
   fYunits=" ";
   fZunits=" ";
   char str1[80]={0};
   char str2[80]={0};
   char str3[80]={0};
   fSize = 0;
   TObject *o;

   TList *listLot = new TList();
   fListBoxLot->GetSelectedEntries(listLot);
   TObjArray *stringLot = new TObjArray();
   TListIter *ilistLot = new TListIter(listLot);
   if(ilistLot) {
      while((o=ilistLot->Next())!=0) {      
	 stringLot->Add(new TObjString(((TGTextLBEntry*)o)->GetText()->GetString()));
      }
      delete ilistLot;
   }
   if(listLot)delete listLot;   

   TList *listWafer = new TList();
   fListBoxWafer->GetSelectedEntries(listWafer);
   TObjArray *stringWafer = new TObjArray();
   TListIter *ilistWafer = new TListIter(listWafer);
   if(ilistWafer) {
      while((o=ilistWafer->Next())!=0) {      
	 stringWafer->Add(new TObjString(((TGTextLBEntry*)o)->GetText()->GetString()));
      }
      delete ilistWafer;
   }
   if(listWafer)delete listWafer;   

   TObjArray *stringAPD = new TObjArray();

   {      
	 TList listAPD;
	 fListBoxAPD->GetSelectedEntries(&listAPD); 
	 TListIter ilistAPD(&listAPD);
   TObject *oo;
	 while((oo=ilistAPD.Next())) {      
	    stringAPD->Add(new TObjString(((TGTextLBEntry*)oo)->GetText()->GetString()));
	 }
   }

   if(fListX->GetSelectedEntry()==NULL) fListX->Select(0);
   Token *tokx = new Token(((((TGTextLBEntry *)fListX->GetSelectedEntry())->GetText())->GetString()),"@");
   if(tokx->HasNext()) fXcoord = tokx->Get();
   if(tokx->HasNext()) fXmap = tokx->Get();
   delete tokx;


   if(fListY->GetSelectedEntry()==NULL) fListY->Select(0);
   Token *toky = new Token(((((TGTextLBEntry *)fListY->GetSelectedEntry())->GetText())->GetString()),"@");
   if(toky->HasNext()) fYcoord = toky->Get();
   if(toky->HasNext()) fYmap = toky->Get();
   delete toky;

   if(fListZ->GetSelectedEntry()==NULL) fListZ->Select(0);
   Token *tokz = new Token(((((TGTextLBEntry *)fListZ->GetSelectedEntry())->GetText())->GetString()),"@");
   if(tokz->HasNext()) fZcoord = tokz->Get();
   if(tokz->HasNext()) fZmap = tokz->Get();
   delete tokz;
   



   fXcoord = "f"+fXcoord;
   fYcoord = "f"+fYcoord;
   fZcoord = "f"+fZcoord;

   fXmap = "m"+fXmap;
   fYmap = "m"+fYmap;
   fZmap = "m"+fZmap;
   Int_t iarr=0,iarrcount=0;
//cout<<"begin"<<endl;
   if(fMap) {
      TObjArrayIter imap(fMap);
      arr = new TArrayI(fMap->GetEntries());
      
      while((o=imap.Next())) {   
	 const char *str0 = (((Header*)o)->fFullSerialNo).Data();
	 sprintf(str1,"%c%c%c%c %c%c%c%c%c%c",str0[0],str0[1],str0[2],str0[3],str0[4],
      	             	      	              str0[5],str0[6],str0[7],str0[8],str0[9]);          
	 sprintf(str2,"%c%c",str0[0],str0[1]);       
	 sprintf(str3,"%c%c %c%c",str0[0],str0[1],str0[2],str0[3]);       
	 if(stringLot->Contains(str2)!=0) {
      	    arr->AddAt(((Header*)o)->GetIndex(),iarrcount);
	    iarrcount++;
	 }    
	 else if(stringWafer->Contains(str3)!=0) {
      	    arr->AddAt(((Header*)o)->GetIndex(),iarrcount);
	    iarrcount++;
	 } 
	 else if(stringAPD->Contains(str1)!=0) {
      	    arr->AddAt(((Header*)o)->GetIndex(),iarrcount);
	    iarrcount++;
	 }           
	 iarr++; 
      }

      arr->Set(iarrcount);
      fSize = arr->GetSize(); 
   }
//cout<<"end"<<endl;
 
   if(stringLot)  delete stringLot;
   if(stringWafer)delete stringWafer;
   if(stringAPD)  delete stringAPD;

   fxa = new Float_t[2];
   fya = new Float_t[2];

}

HistSelected::~HistSelected() {
   if(x) delete x;
   if(y) delete y;
   if(z) delete z;
   if(index) delete [] index;
   if(indexc) delete indexc;
   if(fxa) delete fxa;
   if(fya) delete fya;
   if(fXarr) delete fXarr;
   if(fYarr) delete fYarr;
   if(fZarr) delete fZarr;
   if(arr) delete arr;   

}



Float_t HistSelected::GetVar(TObject *ox,const char *var) {
   if(ox==NULL) {
      cout<<"HistSelected::GetVar from NULL object"<<endl;
      return 0;
   }
   TDataMember *dm = (ox->IsA())->GetDataMember(var);
   TMethodCall *getter = dm->GetterMethod();
   TString type = dm->GetFullTypeName();
   if(type.BeginsWith("Float_t")) {
      Double_t retd=0;
      getter->Execute(ox,"",retd);
      return (Float_t)(retd);
   }
   if(type.BeginsWith("Int_t")) {
      Longptr_t reti=0;
      getter->Execute(ox,"",reti);
      return ((Float_t)(reti));
   }
   
   return 0.;
}


void HistSelected::LoadX(TFile *fDBFile) {
  //if(x!=NULL) delete x; 
  x = new Float_t[fSize];
  //if(index!=NULL) delete index; 
  index = new Bool_t[fSize];
  //if(indexc!=NULL) delete indexc;
  indexc = new Int_t[fSize];

   xmax=-1.0e30;xmin=1.0e30;
   TString blank(" ");
   TString type;
   TObject *ox=0;
   TObjArray *Xmap = (TObjArray*)fDBFile->Get(fXmap);
   for(Int_t i=0;i<fSize;i++) {
      ox =Xmap->At(arr->At(i)); 
      if(ox!=NULL) {
      	 x[i] = GetVar(ox,(const char*)fXcoord );
	 if (xmax<x[i]) xmax=x[i];
	 if (xmin>x[i]) xmin=x[i];
      	 index[i] = kTRUE;
	 indexc[i] = isApdDead((Header*)ox);
      }
      else {      
      	 index[i] = kFALSE;
      }
   }
/*
   if(Xmap) {
      Xmap->Delete();
      Xmap->SetOwner();
      delete Xmap;
   }      
*/
}


void HistSelected::LoadXY(TFile *fDBFile) {

  //if(x!=NULL) delete x; 
  x = new Float_t[fSize];
   xmax=-1.0e30;xmin=1.0e30;
   // if(y!=NULL) delete y;
   y = new Float_t[fSize];
   ymax=-1.0e30;ymin=1.0e30;
   //if(index!=NULL) delete index;
   index = new Bool_t[fSize];
   //if(indexc!=NULL) delete indexc;
   indexc = new Int_t[fSize];
 
   TObject *ox=0;
   TObject *oy=0;

   TObjArray *Xmap = (TObjArray*)fDBFile->Get(fXmap);
   TObjArray *Ymap;
   if(!fXmap.CompareTo(fYmap)) Ymap = Xmap;
   else Ymap = (TObjArray*)fDBFile->Get(fYmap);

   TString blank(" ");
   TString type;
   for(Int_t i=0;i<fSize;i++) {
     ox =Xmap->At(arr->At(i)); 
     oy =Ymap->At(arr->At(i)); 
     if(ox!=NULL && oy!=NULL) {
	x[i] = GetVar(ox,(const char*)fXcoord );
	y[i] = GetVar(oy,(const char*)fYcoord );
	if (xmax<x[i]) xmax=x[i];
	if (xmin>x[i]) xmin=x[i];
	if (ymax<y[i]) ymax=y[i];
	if (ymin>y[i]) ymin=y[i];
	index[i] = kTRUE;
	indexc[i] = isApdDead((Header*)ox);
     }
     else {
	index[i] = kFALSE;
     }
   }
/*
   if(Xmap) {
     Xmap->Delete();
     Xmap->SetOwner();
     delete Xmap;
   }
   if(Ymap) {
      if(fXmap.CompareTo(fYmap)) {
	Ymap->Delete();
	Ymap->SetOwner();
	delete Ymap;
      }
   }
*/
}

void HistSelected::LoadX_Y(TFile *fDBFile) {
  //if(x!=NULL) delete x;
  x = new Float_t[fSize];
   xmax=-1.0e30;xmin=1.0e30;
   //if(index!=NULL)delete index;
   index = new Bool_t[fSize];
   //if(indexc!=NULL)delete indexc;
   indexc = new Int_t[fSize];
   TObject *ox=0;
   TObject *oy=0;
   TObjArray *Xmap = (TObjArray*)fDBFile->Get(fXmap);
   TObjArray *Ymap;
   if(!fXmap.CompareTo(fYmap)) Ymap = Xmap;
   else Ymap = (TObjArray*)fDBFile->Get(fYmap);

   TString blank(" ");
   TString type;
   for(Int_t i=0;i<fSize;i++) {
      ox =Xmap->At(arr->At(i)); 
      oy =Ymap->At(arr->At(i)); 
      if(ox!=NULL && oy!=NULL) {
      	 x[i] = GetVar(ox,(const char*)fXcoord ) -
      	          GetVar(oy,(const char*)fYcoord );
	 if (xmax<x[i]) xmax=x[i];
	 if (xmin>x[i]) xmin=x[i];
	 index[i]=kTRUE;
	 indexc[i] = isApdDead((Header*)ox);
      }
      else {
	 index[i]=kFALSE;
      }
   }
/*
   if(Xmap) {
      Xmap->Delete();
      Xmap->SetOwner();
      delete Xmap;
   }
   if(fXmap.CompareTo(fYmap)) {
      Ymap->Delete();
      Ymap->SetOwner();
      delete Ymap;
   }
*/
}


void HistSelected::LoadXYZ(TFile *fDBFile) {
  //if(x!=NULL) delete x;
  x = new Float_t[fSize];
   xmax=-1.0e30;xmin=1.0e30;
   //if(y!=NULL) delete y;
   y = new Float_t[fSize];
   ymax=-1.0e30;ymin=1.0e30;
   //if(x!=NULL) delete z;
   z = new Float_t[fSize];

   //if(index!=NULL) delete index;
   index = new Bool_t[fSize];
   //if(indexc!=NULL) delete indexc;
   indexc = new Int_t[fSize];
   TObject *ox=0;
   TObject *oy=0;
   TObject *oz=0;

   TObjArray *Xmap = (TObjArray*)fDBFile->Get(fXmap);
   TObjArray *Ymap;
   if(!fXmap.CompareTo(fYmap)) Ymap = Xmap;
   else Ymap = (TObjArray*)fDBFile->Get(fYmap);
   TObjArray *Zmap;
   if(!fXmap.CompareTo(fZmap)) Zmap = Xmap;
   else if(!fYmap.CompareTo(fZmap)) Zmap = Ymap; 
   else Zmap = (TObjArray*)fDBFile->Get(fZmap);


   TString blank(" ");
   TString type;
   for(Int_t i=0;i<fSize;i++) {
      ox =Xmap->At(arr->At(i)); 
      oy =Ymap->At(arr->At(i)); 
      oz =Zmap->At(arr->At(i)); 
      if(ox!=NULL && oy!=NULL && oz!=NULL) {
      	 x[i] = GetVar(ox,(const char*)fXcoord );
      	 y[i] = GetVar(oy,(const char*)fYcoord );
      	 z[i] = GetVar(oz,(const char*)fZcoord );
	 if (xmax<x[i]) xmax=x[i];
	 if (xmin>x[i]) xmin=x[i];
	 if (ymax<y[i]) ymax=y[i];
	 if (ymin>y[i]) ymin=y[i];
	 index[i]=kTRUE;
	 indexc[i] = isApdDead((Header *)ox);
      }
      else {
	 index[i]=kFALSE;
      }
   }
/*
   if(Xmap) {
      Xmap->Delete();
      Xmap->SetOwner();
      delete Xmap;
   }
   if(fXmap.CompareTo(fYmap)) {
      Ymap->Delete();
      Ymap->SetOwner();
      delete Ymap;
   }
   if(fXmap.CompareTo(fZmap) && fYmap.CompareTo(fZmap) ) {
      Zmap->Delete();
      Zmap->SetOwner();
      delete Zmap;
   }
*/
}

void HistSelected::SetTitle(TH2F *h,TString tit,TString titx,TString tity) {
   Header header;
   TString xtit =  (titx(1,titx.Length())) +(header.GetUnits(fXcoord,fXmap));
   TString ytit =  (tity(1,tity.Length())) +(header.GetUnits(fYcoord,fYmap));
   h->SetTitle(tit);
   h->GetXaxis()->SetTitle(xtit);
   h->GetXaxis()->CenterTitle(kTRUE);
   h->GetYaxis()->SetTitle(ytit);
   h->GetYaxis()->CenterTitle(kTRUE);
}

void HistSelected::SetTitle(TH1F *h,TString tit,TString titx) {
   h->SetTitle(tit);
   Header header;
   TString xtit =  (titx(1,titx.Length())) +(header.GetUnits(fXcoord,fXmap));
   h->GetXaxis()->SetTitle(xtit);
   h->GetXaxis()->CenterTitle(kTRUE);
}

void HistSelected::SetTitle(TGraph *g,TString tit,TString titx, TString tity) {
   fTitle = tit;
   Header header;
   fTitleX = (titx(1,titx.Length())) +(header.GetUnits(fXcoord,fXmap));
   fTitleY = (tity(1,tity.Length())) +(header.GetUnits(fYcoord,fYmap));

}





void HistSelected::FillHist( TString fType, TString fExtendedType, 

      	             	     TObjArray *fHist,TGListBox *fListHist, TFile *fDBFile,
			     Bool_t *isN,  char **sopt ) {
   if(fSize==0 ) return;
   
   if(isN[0] && isN[1] && atof(sopt[0])>atof(sopt[1])) {
      cout<<"xmax < xmin"<<endl;
      isN[0]=kFALSE;isN[1]=kFALSE;
   }
   if(isN[2] && isN[3] && atof(sopt[2])>atof(sopt[3])) {
      cout<<"ymax < ymin"<<endl;
      isN[2]=kFALSE;isN[3]=kFALSE;
   }

   if( (fListHist->GetSelectedEntry())==NULL) return;

   TString selstring = ((((TGTextLBEntry *)fListHist->GetSelectedEntry())
      	             	   ->GetText())->GetString());
   if(!selstring.CompareTo("")) return;
   if(!selstring.CompareTo("X hist")) {
     //cout<<"X histTH1F"<<endl;
      LoadX(fDBFile);
      //cout<<"X histTH1F"<<endl;
      Int_t nbins=200;
      if(isN[0]) xmin=atof(sopt[0]);
      //cout<<"X histTH1F"<<endl;
      if(isN[1]) xmax=atof(sopt[1]);
      if(!fXcoord.CompareTo("fXPos")) nbins = (Int_t)(xmax-xmin)+1;
      Float_t delta=0.5*(xmax-xmin+1)/nbins;
      if (delta==0.0) delta=xmax*0.1;
      TH1F *hist1 = new TH1F("Xhist","Xhist",nbins,xmin-delta,xmax+delta);
      if(needSelection) {
//	 TH1F *hist11 = new TH1F("hist11","X hist",nbins,xmin-delta,xmax+delta);
	 TH1F *hist12 = new TH1F("hist12","X hist",nbins,xmin-delta,xmax+delta);
	 TH1F *hist13 = new TH1F("hist13","X hist",nbins,xmin-delta,xmax+delta);
	 for(Int_t i=0;i<fSize;i++) {
      	    if(index[i]) {
	       hist1->Fill(x[i]);
//	       if(indexc[i]==0) hist11->Fill(x[i]);
	       if(indexc[i]==1) hist12->Fill(x[i]);
	       else if(indexc[i]==-1) hist13->Fill(x[i]);
	    }
	 }
	 SetTitle(hist1,fXmap,fXcoord);
	 fHist->Add(hist1);
//	 hist11->SetLineColor(0);hist11->SetLineStyle(2);
//	 fHist->Add(hist11);
	 hist12->SetLineColor(2);hist12->SetLineStyle(2);
	 fHist->Add(hist12);
	 hist13->SetLineColor(3);hist13->SetLineStyle(2);
	 fHist->Add(hist13);
      }
      else {
	 for(Int_t i=0;i<fSize;i++) {
      	    if(index[i]) hist1->Fill(x[i]);
	 }
	 SetTitle(hist1,fXmap,fXcoord);
	 fHist->Add(hist1);
      }
   }

   if(!selstring.CompareTo("XY hist")) {
      LoadXY(fDBFile);
      Int_t nbinsx=40;
      Int_t nbinsy=40;
      
      if(isN[0]) xmin=atof(sopt[0]);
      if(isN[1]) xmax=atof(sopt[1]);
      if(isN[2]) ymin=atof(sopt[2]);
      if(isN[3]) ymax=atof(sopt[3]);

      if(!fXcoord.CompareTo("fXPos")) nbinsx = (Int_t)(xmax-xmin)+1;      
      if(!fYcoord.CompareTo("fYPos")) nbinsy = (Int_t)(ymax-ymin)+1;      

      Float_t xdelta=0.5*(xmax-xmin+1)/nbinsx;
      Float_t ydelta=0.5*(ymax-ymin+1)/nbinsy;
      if (xdelta==0.0) xdelta=xmax*0.1;
      if (ydelta==0.0) ydelta=ymax*0.1;
      TMyH2F *hist2 = new TMyH2F("XYhist","XYhist",nbinsx,xmin-xdelta,xmax+xdelta,
      	             	      	             nbinsy,ymin-ydelta,ymax+ydelta);

      for(Int_t i=0;i<fSize;i++) {
         if(index[i]) hist2->Fill(x[i],y[i]);
      }
      SetTitle(hist2,fYmap+" vs "+fXmap,fXcoord,fYcoord);
      fHist->Add(hist2);
   }

   if(!selstring.CompareTo("X-Y hist")) {
      LoadX_Y(fDBFile);
      Int_t nbins=200;
      if(isN[0]) xmin=atof(sopt[0]);
      if(isN[1]) xmax=atof(sopt[1]);
      if(!fXcoord.CompareTo("fXPos")) nbins = (Int_t)(xmax-xmin)+1;      
      Float_t delta=0.5*(xmax-xmin+1)/nbins;
      if (delta==0.0) delta=xmax*0.1;
      TH1F *hist1 = new TH1F("X-Yhist","X-Yhist",nbins,xmin-delta,xmax+delta);
      if(needSelection) {
	 TH1F *hist12 = new TH1F("hist12","X hist",nbins,xmin-delta,xmax+delta);
	 TH1F *hist13 = new TH1F("hist13","X hist",nbins,xmin-delta,xmax+delta);
	 for(Int_t i=0;i<fSize;i++) {
      	    if(index[i]) {
	       hist1->Fill(x[i]);
	       if(indexc[i]==1) hist12->Fill(x[i]);
	       else if(indexc[i]==-1) hist13->Fill(x[i]);
	    }
	 }
	 SetTitle(hist1,fXmap+" - "+fYmap,fXcoord +" - "+fYcoord(1,fYcoord.Length()));
	 fHist->Add(hist1);
	 hist12->SetLineColor(2);hist12->SetLineStyle(2);
	 fHist->Add(hist12);
	 hist13->SetLineColor(3);hist13->SetLineStyle(2);
	 fHist->Add(hist13);
      }
      else {
	 for(Int_t i=0;i<fSize;i++) {
            if(index[i])hist1->Fill(x[i]);
	 }
	 SetTitle(hist1,fXmap+" - "+fYmap,fXcoord +" - "+fYcoord(1,fYcoord.Length()));
	 fHist->Add(hist1);
      }
   }

   if(!selstring.CompareTo("XYZ hist")) {
      LoadXYZ(fDBFile);
      if(isN[0]) xmin=atof(sopt[0]);
      if(isN[1]) xmax=atof(sopt[1]);
      if(isN[2]) ymin=atof(sopt[2]);
      if(isN[3]) ymax=atof(sopt[3]);

      Int_t nbinsx=40;
      Int_t nbinsy=40;
      
      if(!fXcoord.CompareTo("fXPos")) nbinsx = (Int_t)(xmax-xmin)+1;      
      if(!fYcoord.CompareTo("fYPos")) nbinsy = (Int_t)(ymax-ymin)+1;      

      Float_t xdelta=0.5*(xmax-xmin+1)/nbinsx;
      Float_t ydelta=0.5*(ymax-ymin+1)/nbinsy;


      if (xdelta==0.0) xdelta=xmax*0.1;
      if (ydelta==0.0) ydelta=ymax*0.1;
      TH2F *hist2 = new TH2F("XYZhist","XYZhist",nbinsx,xmin-xdelta,xmax+xdelta,
      	             	      	             nbinsy,ymin-ydelta,ymax+ydelta);

      for(Int_t i=0;i<fSize;i++) {
         if(index[i]) hist2->Fill(x[i],y[i],z[i]);
      }
      SetTitle(hist2,fXmap+" vs "+fYmap,fXcoord,fYcoord);
      fHist->Add(hist2);
   }
//

   if(!selstring.CompareTo("XY[Z] hist")) {
      LoadXYZ(fDBFile);
      if(isN[0]) xmin=atof(sopt[0]);
      if(isN[1]) xmax=atof(sopt[1]);
      if(isN[2]) ymin=atof(sopt[2]);
      if(isN[3]) ymax=atof(sopt[3]);

      Int_t nbinsx=40;
      Int_t nbinsy=40;
      
      if(!fXcoord.CompareTo("fXPos")) nbinsx = (Int_t)(xmax-xmin)+1;      
      if(!fYcoord.CompareTo("fYPos")) nbinsy = (Int_t)(ymax-ymin)+1;      

      Float_t xdelta=0.5*(xmax-xmin+1)/nbinsx;
      Float_t ydelta=0.5*(ymax-ymin+1)/nbinsy;

      if (xdelta==0.0) xdelta=xmax*0.1;
      if (ydelta==0.0) ydelta=ymax*0.1;
      TMyH2F *hist2 = new TMyH2F("XY[Z]hist","XY[Z]hist",nbinsx,xmin-xdelta,xmax+xdelta,
      	             	      	             nbinsy,ymin-ydelta,ymax+ydelta);
      TH2F *hist3 = new TH2F("hist3","hist3",nbinsx,xmin-xdelta,xmax+xdelta,
      	             	      	             nbinsy,ymin-ydelta,ymax+ydelta);

      for(Int_t i=0;i<fSize;i++) {
         if(index[i]) {
	    hist2->Fill(x[i],y[i],z[i]);
            hist3->Fill(x[i],y[i]);
	 }
      }
      hist2->Divide(hist3);
      delete hist3;
      SetTitle(hist2,fXmap+" vs "+fYmap,fXcoord,fYcoord);
      fHist->Add(hist2);
   }

// Graphs
   if(!selstring.CompareTo("XY graph")) {
     //  if(x) delete x;
     x = new Float_t[fSize];
     // if(y) delete y;
     y = new Float_t[fSize];


      if(isN[0]) xmin=atof(sopt[0]);
      if(isN[1]) xmax=atof(sopt[1]);
      if(isN[2]) ymin=atof(sopt[2]);
      if(isN[3]) ymax=atof(sopt[3]);
      Bool_t *index = new Bool_t[fSize];
      TObject *ox=0;
      TObject *oy=0;
      TObjArray *Xmap = (TObjArray*)fDBFile->Get(fXmap);
      TObjArray *Ymap;
      if(!fXmap.CompareTo(fYmap)) Ymap = Xmap;
      else Ymap = (TObjArray*)fDBFile->Get(fYmap);
      TString blank(" ");
      TString type;
      for(Int_t i=0;i<fSize;i++) {
      	 ox =Xmap->At(arr->At(i)); 
      	 oy =Ymap->At(arr->At(i)); 
      	 if(ox!=NULL && oy!=NULL) {
      	    x[i] = GetVar(ox,(const char*)fXcoord );
      	    y[i] = GetVar(oy,(const char*)fYcoord );
	    index[i] = kTRUE;
	 }
	 else {
	    index[i] = kFALSE;
	 }
         if(isN[0]) if(xmin>x[i]) index[i]=kFALSE;
         if(isN[1]) if(xmax<x[i]) index[i]=kFALSE;
         if(isN[2]) if(ymin>y[i]) index[i]=kFALSE;
         if(isN[3]) if(ymax<y[i]) index[i]=kFALSE;
	 
      }
//   fDBFile->DeleteAll();
/*
      if(Xmap) {
	 Xmap->Delete();
	 Xmap->SetOwner();
	 delete Xmap;
      }
      if(fXmap.CompareTo(fYmap)) {
	 if(Ymap) {
      	    Ymap->Delete();
      	    Ymap->SetOwner();
      	    delete Ymap;
	 }
      }
*/      
      Int_t j=0;

for(Int_t i=0;i<fSize;i++) { 
if(!index[i]) {
cout<<endl<<"Points not shown in the Graph:"<<endl;
break;}}
      for(Int_t i=0;i<fSize;i++) {
      	 if(index[i]) {
	    x[j] = x[i];
	    y[j] = y[i];	    
	    j++;
	 }
     else {
       //     cout<< (const char*)fXcoord <<":"<<x[i]<<" "<<(const char*)fYcoord <<":"<<y[i]<<endl;
     }
      }
      if(j>0) {
         TMyGraph *graph1 = new TMyGraph(j,x,y,fCanvas); 
         SetTitle(graph1,fYmap+" vs "+fXmap,fXcoord,fYcoord);
         fHist->Add(graph1);
      }   
      if(index)delete [] index;

   }

   if(!selstring.CompareTo("X(Y-Z) graph")) {
     //if(x!=NULL) delete x; x = new Float_t[fSize];
x = new Float_t[fSize];
     // if(y!=NULL) delete y; y = new Float_t[fSize];
y = new Float_t[fSize];
  if(isN[0]) xmin=atof(sopt[0]);
      if(isN[1]) xmax=atof(sopt[1]);
      if(isN[2]) ymin=atof(sopt[2]);
      if(isN[3]) ymax=atof(sopt[3]);
      Bool_t *index = new Bool_t[fSize];
      TObject *ox=0;
      TObject *oy=0;
      TObject *oz=0;
      TObjArray *Xmap = (TObjArray*)fDBFile->Get(fXmap);
      TObjArray *Ymap;
      if(!fXmap.CompareTo(fYmap)) Ymap = Xmap;
      else Ymap = (TObjArray*)fDBFile->Get(fYmap);
      TObjArray *Zmap;
      if(!fXmap.CompareTo(fZmap)) Zmap = Xmap;
      else if(!fYmap.CompareTo(fZmap)) Zmap = Ymap; 
      else Zmap = (TObjArray*)fDBFile->Get(fZmap);
      TString blank(" ");
      TString type;
      for(Int_t i=0;i<fSize;i++) {
      	 ox =Xmap->At(arr->At(i)); 
      	 oy =Ymap->At(arr->At(i)); 
      	 oz =Zmap->At(arr->At(i)); 
      	 if(ox!=NULL && oy!=NULL && oz!=NULL) {
      	    x[i] = GetVar(ox,(const char*)fXcoord );
      	    y[i] = GetVar(oy,(const char*)fYcoord ) -
      	             GetVar(oz,(const char*)fZcoord );
	    index[i]=kTRUE;
	 }
	 else {
	    index[i]=kFALSE;
	 }
         if(isN[0]) if(xmin>x[i]) index[i]=kFALSE;
         if(isN[1]) if(xmax<x[i]) index[i]=kFALSE;
         if(isN[2]) if(ymin>y[i]) index[i]=kFALSE;
         if(isN[3]) if(ymax<y[i]) index[i]=kFALSE;
      }
/*      
      if(Xmap) {
	 Xmap->Delete();
	 Xmap->SetOwner();
	 delete Xmap;
      }
      if(fXmap.CompareTo(fYmap)) {
      	 Ymap->Delete();
      	 Ymap->SetOwner();
      	 delete Ymap;
      }
      if(fXmap.CompareTo(fZmap) && fYmap.CompareTo(fZmap)) {
      	 Zmap->Delete();
      	 Zmap->SetOwner();
      	 delete Zmap;
      }
*/
      Int_t j=0;
for(Int_t i=0;i<fSize;i++) { 
if(!index[i]) {
cout<<endl<<"Points not shown in the Graph:"<<endl;
break;}}
      for(Int_t i=0;i<fSize;i++) {
      	 if(index[i]) {
	    x[j] = x[i];
	    y[j] = y[i];	    
	    j++;
	 }
     else {
       // cout<< (const char*)fXcoord <<":"<<x[i]<<" "<<(const char*)fYcoord<<"-"<<(const char*)fZcoord <<":"<<y[i]<<endl;
     }
      }
      if(j>0) {
         TMyGraph *graph1 = new TMyGraph(j,x,y);
	 ///	 cout<<"TMyGraph"<<endl;
         SetTitle(graph1,
            fYmap+"-"+fZmap+" vs "+fXmap,fXcoord,fYcoord+"-"+fZcoord(1,fZcoord.Length()));
         fHist->Add(graph1);
      }
      if(index)delete [] index;
   }
   if(!selstring.CompareTo("X(Y/Z) graph")) {
     //  if(x) delete x; 
     x = new Float_t[fSize];
     // if(y) delete y; 
     y = new Float_t[fSize];
      if(isN[0]) xmin=atof(sopt[0]);
      if(isN[1]) xmax=atof(sopt[1]);
      if(isN[2]) ymin=atof(sopt[2]);
      if(isN[3]) ymax=atof(sopt[3]);
      Bool_t *index = new Bool_t[fSize];
      TObject *ox=0;
      TObject *oy=0;
      TObject *oz=0;
      TObjArray *Xmap = (TObjArray*)fDBFile->Get(fXmap);
      TObjArray *Ymap;
      if(!fXmap.CompareTo(fYmap)) Ymap = Xmap;
      else Ymap = (TObjArray*)fDBFile->Get(fYmap);
      TObjArray *Zmap;
      if(!fXmap.CompareTo(fZmap)) Zmap = Xmap;
      else if(!fYmap.CompareTo(fZmap)) Zmap = Ymap; 
      else Zmap = (TObjArray*)fDBFile->Get(fZmap);
      TString blank(" ");
      TString type;
      for(Int_t i=0;i<fSize;i++) {
      	 ox =Xmap->At(arr->At(i)); 
      	 oy =Ymap->At(arr->At(i)); 
      	 oz =Zmap->At(arr->At(i)); 
      	 if(ox!=NULL && oy!=NULL && oz!=NULL) {
      	    x[i] = GetVar(ox,(const char*)fXcoord );
	    Double_t del = GetVar(oz,(const char*)fZcoord);
	    if(del != 0.0) {
      	       y[i] = GetVar(oy,(const char*)fYcoord )/del;
//      	        	GetVar(oz,(const char*)fZcoord );
	    }
	    else y[i] = 0.0;
	    index[i]=kTRUE;
	 }
	 else {
	    index[i]=kFALSE;
	 }
         if(isN[0]) if(xmin>x[i]) index[i]=kFALSE;
         if(isN[1]) if(xmax<x[i]) index[i]=kFALSE;
         if(isN[2]) if(ymin>y[i]) index[i]=kFALSE;
         if(isN[3]) if(ymax<y[i]) index[i]=kFALSE;
      }
/* 
      if(Xmap) {
	 Xmap->Delete();
	 Xmap->SetOwner();
	 delete Xmap;
      }
      if(fXmap.CompareTo(fYmap)) {
      	 Ymap->Delete();
      	 Ymap->SetOwner();
      	 delete Ymap;
      }
      if(fXmap.CompareTo(fZmap) && fYmap.CompareTo(fZmap)) {
      	 Zmap->Delete();
      	 Zmap->SetOwner();
      	 delete Zmap;
      }
*/
      Int_t j=0;
for(Int_t i=0;i<fSize;i++) { 
if(!index[i]) {
cout<<endl<<"Points not shown in the Graph:"<<endl;
break;}}
      for(Int_t i=0;i<fSize;i++) {
      	 if(index[i]) {
	    x[j] = x[i];
	    y[j] = y[i];	    
	    j++;
	 }
     else {
       //          cout<< (const char*)fXcoord <<":"<<x[i]<<" "<<(const char*)fYcoord<<"/"<<(const char*)fZcoord <<":"<<y[i]<<endl;
     }
      }
      if(j>0) {
         TMyGraph *graph1 = new TMyGraph(j,x,y); 
         SetTitle(graph1,
            fYmap+"/"+fZmap+" vs "+fXmap,fXcoord,fYcoord+"/"+fZcoord(1,fZcoord.Length()));
         fHist->Add(graph1);
      }
      if(index)delete [] index;
   }
   if(!selstring.CompareTo("X(Y*Z) graph")) {
     //      if(x) delete x; 
     x = new Float_t[fSize];
     // if(y) delete y; 
     y = new Float_t[fSize];
      if(isN[0]) xmin=atof(sopt[0]);
      if(isN[1]) xmax=atof(sopt[1]);
      if(isN[2]) ymin=atof(sopt[2]);
      if(isN[3]) ymax=atof(sopt[3]);

      Bool_t *index = new Bool_t[fSize];
      TObject *ox=0;
      TObject *oy=0;
      TObject *oz=0;
      TObjArray *Xmap = (TObjArray*)fDBFile->Get(fXmap);
      TObjArray *Ymap;
      if(!fXmap.CompareTo(fYmap)) Ymap = Xmap;
      else Ymap = (TObjArray*)fDBFile->Get(fYmap);
      TObjArray *Zmap;
      if(!fXmap.CompareTo(fZmap)) Zmap = Xmap;
      else if(!fYmap.CompareTo(fZmap)) Zmap = Ymap; 
      else Zmap = (TObjArray*)fDBFile->Get(fZmap);
      TString blank(" ");
      TString type;
      for(Int_t i=0;i<fSize;i++) {
      	 ox =Xmap->At(arr->At(i)); 
      	 oy =Ymap->At(arr->At(i)); 
      	 oz =Zmap->At(arr->At(i)); 
      	 if(ox!=NULL && oy!=NULL && oz!=NULL) {
      	    x[i] = GetVar(ox,(const char*)fXcoord );
      	    y[i] = GetVar(oy,(const char*)fYcoord ) *
      	             GetVar(oz,(const char*)fZcoord );
	    index[i]=kTRUE;
	 }
	 else {
	    index[i]=kFALSE;
	 }
         if(isN[0]) if(xmin>x[i]) index[i]=kFALSE;
         if(isN[1]) if(xmax<x[i]) index[i]=kFALSE;
         if(isN[2]) if(ymin>y[i]) index[i]=kFALSE;
         if(isN[3]) if(ymax<y[i]) index[i]=kFALSE;
      }
/*
      if(Xmap) {
	 Xmap->Delete();
	 Xmap->SetOwner();
	 delete Xmap;
      }
      if(fXmap.CompareTo(fYmap)) {
      	 Ymap->Delete();
      	 Ymap->SetOwner();
      	 delete Ymap;
      }
      if(fXmap.CompareTo(fZmap) && fYmap.CompareTo(fZmap)) {
      	 Zmap->Delete();
      	 Zmap->SetOwner();
      	 delete Zmap;
      }
*/
      Int_t j=0;
for(Int_t i=0;i<fSize;i++) { 
if(!index[i]) {
cout<<endl<<"Points not shown in the Graph:"<<endl;
break;}}
      for(Int_t i=0;i<fSize;i++) {
      	 if(index[i]) {
	    x[j] = x[i];
	    y[j] = y[i];	    
	    j++;
	 }
     else {
     cout<< (const char*)fXcoord <<":"<<x[i]<<" "<<(const char*)fYcoord<<"*"<<(const char*)fZcoord <<":"<<y[i]<<endl;
     }
      }
      if(j>0) {
         TMyGraph *graph1 = new TMyGraph(j,x,y); 
         SetTitle(graph1,
            fYmap+"*"+fZmap+" vs "+fXmap,fXcoord,fYcoord+"*"+fZcoord(1,fZcoord.Length()));
         fHist->Add(graph1);
      }
      if(index)delete [] index;
   }

}



























