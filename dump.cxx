#include "TFile.h"
#include "Measurement.h"
using namespace std;

int main(int argc, char** argv)
{
  TFile f("measurementtot.root");
  
  // load table 'mHam', see the Apdbrowser.cxx line 950 for full list and details
  TObjArray* table = (TObjArray*)f.Get("mHam");
  if (!table) {
    cout << "ERROR: can't find the table" << endl;
    return 1;
  }
  
  cout << "table total entries = " << table->GetEntries() << endl;
  int nempty = 0;
  
  for (int i = 0; i < table->GetEntries(); ++i) {
    
    // see the Measurement.h for 'Hamamatsu' structure explanations
    const Hamamatsu* h = (Hamamatsu *) (table->At(i));
    if (!h) {
      //cout << "idx=" << i << " missing" << endl;
      nempty++;
      continue;
    }
    
    // example of scalar types
    cout
      << "idx=" << i << " "
      << "SNo=" << h->fSNo << " "            // type label is    VARS(Int_t,SNo)
      << "Temperature=" << h->fTemperature    // type label is    VARS(Float_t,Temperature )
      << endl;
    
    // example of vector types
    const int nVoltage = h->faVoltage.GetSize();
    const Float_t* Voltage = h->faVoltage.GetArray();            // type label is    VARA(TArrayF,aVoltage)
    cout << "Voltage=";
    for (int j = 0; j < nVoltage; ++j) cout << Voltage[j] << " ";
    cout << endl;
    
    const int nTotalCurrent = h->faVoltage.GetSize();
    const Float_t* TotalCurrent = h->faTotalCurrent.GetArray();  // type label is    VARA(TArrayF,aTotalCurrent)
    cout << "TotalCurrent=";
    for (int j = 0; j < nTotalCurrent; ++j) cout << TotalCurrent[j] << " ";
    cout << endl;
  }
  
  cout << "table empty entries = " << nempty << endl;
  cout << "table full entries = " << (table->GetEntries() - nempty) << endl;
}
