////////////////////////////////////////////////////////////////////////
// 8.11.2000 by Andrei Dorokhov
// 
//The program is developing and supporting by Andrei Kouznetsov
//
////////////////////////////////////////////////////////////////////////

#include <stdlib.h>
#include <sys/types.h>
#include <dirent.h>

#include "TROOT.h"
#include "TSystem.h"
#include "TApplication.h"
#include "TFile.h"
#include "TRandom.h"
#include "TTree.h"
#include "TBranch.h"
#include "TClonesArray.h"
#include "TStopwatch.h"
#include "TCanvas.h"
#include "TGraph.h"

//#include "Measurement.h"
#include "BuildDB.h"
#include "Apdlist.h"
#include <TMethodCall.h>

void MakeScalarsTable(TObject *obj,TString fmap) {
   Header header;

   if(obj==NULL) return;
   cout<<"<TABLE BORDER=8 COLS=2 WIDTH=\"500\""<<endl;
   TClass *cl = obj->IsA();
   TList *cllist = cl->GetListOfDataMembers();
   TListIter *cliter = new TListIter(cllist);
   TObject *o;
   Double_t retf=0;
   Long_t reti=0;
   while((o=cliter->Next())!=0) {
      TDataMember *dm = (TDataMember *)o;
      TString varname = dm->GetName();
      TString typname = dm->GetFullTypeName();
      if(varname.Length()>2) {
      	 TString s = varname(1,varname.Length()-1);
	 TMethodCall *getter = dm->GetterMethod();
	 if(typname.BeginsWith("Float") && getter!=NULL ) {  
	    TString units = (header.GetUnits(varname,fmap));
       	    getter->Execute(obj,"",retf);
	    cout<<"<TR><TD>"<<s<<units <<"</TD><TD>"<<retf<<"</TD></TR>"<<endl;	       	 
	 }      
	 if(typname.BeginsWith("Int") && getter!=NULL) {      
	    TString units = (header.GetUnits(varname,fmap));
      	    getter->Execute(obj,"",reti);      	 
	    cout<<"<TR><TD>"<<s<<units<<"</TD><TD>"<<reti<<"</TD></TR>"<<endl;	       	 
	 }      
      }
            
   }
   delete cliter;
   cout<<"</TABLE>"<<endl;
}

void MakeArraysTable(TObject *obj, TString fmap) {
   Header header;
   if(obj==NULL) return;
   TClass *cl = obj->IsA();
   TList *cllist = cl->GetListOfDataMembers();
   TListIter *cliter = new TListIter(cllist);
   TObject *o;
   Long_t retd=-1;
   Int_t max=0;
   while((o=cliter->Next())!=0) {
      TDataMember *dmb = (TDataMember *)o;
      TString varname = dmb->GetName();
      TString typname = dmb->GetFullTypeName();
      if(varname.Length()>2) {
	 if(typname.BeginsWith("TArrayF") ) {      
      	    TMethodCall *getter = dmb->GetterMethod();
	    if(getter!=NULL) {
      	       getter->Execute(obj,"",retd);
	       if(retd!=-1) {
      		  TArrayF *arr = (TArrayF*) retd;	    
		  if(arr!=NULL) if(max<arr->GetSize()) max=arr->GetSize();
	       }
	    }
	 }      
      }      
   }
   cliter->Reset();
   cout<<"<TABLE BORDER=8 COLS="<<max<<" WIDTH=\"500\""<<endl;

   while((o=cliter->Next())!=0) {
      TDataMember *dmb = (TDataMember *)o;
      TString varname = dmb->GetName();
      TString typname = dmb->GetFullTypeName();
      if(varname.Length()>2) {
      	 TString s = varname(2,varname.Length()-2);
	 if(typname.BeginsWith("TArrayF") ) {      
      	    (dmb->GetterMethod())->Execute(obj,"",retd);
	    TArrayF *arr = (TArrayF*) retd;
	    if(arr!=NULL) {	    
      	       TString units = (header.GetUnits(varname,fmap));
      	       cout<<"<TR><TD>"<<s<<units<<"</TD>";
      	       for(Int_t i=0;i<arr->GetSize();i++) 
		  cout<<"<TD>"<<arr->At(i)<<"</TD>";
	       for(Int_t i=arr->GetSize();i<max;i++) 
	          cout<<"<TD> </TD>";
   	       cout<<"</TR>"<<endl;;
      	    }	    
	 }      
      }      
   }
   delete cliter;
   cout<<"</TABLE>"<<endl;
}


//______________________________________________________________________________
int main(int argc, char **argv)
{

   

//   cout<<"Content-type: text/html"<<endl<<endl;
//
   cout<<"<HTML>"<<endl;
   cout<<"<HEAD><TITLE>APD lab home page</TITLE></HEAD>"<<endl;
   cout<<"<BODY BACKGROUND=\"http://pcephc172.cern.ch/apdlab/Paper.gif\">"<<endl;
   cout<<"<!--"<<endl;
   TROOT APDMeasurement("measurement","APD measurements");

   char *rootfile=argv[1];
   TApplication theApp("App", &argc, argv);
//   TStorage MyHeap;
//   MyHeap.EnableStatistics();
//   cout<<"Before"<<endl;
//   MyHeap.PrintStatistics();
   gROOT->SetBatch();
   TString apdnumber = "1";
   TString envar  = getenv("QUERY_STRING");

   Token *tok = new Token(envar,"="); 
   tok->HasNext();
   if(tok->HasNext()) {
      tok->Get();
      cout<<tok->Get()<<endl;
      apdnumber = tok->Get();
   }
   else {
      apdnumber="1";
   }
   delete tok;   

   cout<<"-->"<<endl;   
   /*   if(argc!=2) {
      cout<<"please specify root filename"<<endl; 
      cout<<"</BODY>"<<endl;
      cout<<"</HTML>"<<endl;
      return 0;
      } */
   TFile *f = new TFile(rootfile);   

   const char *ss = apdnumber.Data();
   
   Int_t number=0;
   if(strlen(ss)==10) number = atol(&ss[4]);
   if(strlen(ss)<=6) number = atol(&ss[0]);

   TObjArray *mHam = (TObjArray*)f->Get("mHam"); 

   TObjArray *mGn =  (TObjArray*)f->Get("mGn"); 
   TObjArray *mGa =  (TObjArray*)f->Get("mGa"); 
   TObjArray *mGi =  (TObjArray*)f->Get("mGi"); 
   
   TObjArray *mGQn =  (TObjArray*)f->Get("mGQn"); 
   TObjArray *mGQa =  (TObjArray*)f->Get("mGQa"); 
   TObjArray *mGQi =  (TObjArray*)f->Get("mGQi"); 

   TObjArray *mQn =  (TObjArray*)f->Get("mQn"); 
   TObjArray *mQa =  (TObjArray*)f->Get("mQa"); 
   TObjArray *mQi =  (TObjArray*)f->Get("mQi"); 

   TObjArray *mFn =  (TObjArray*)f->Get("mFn"); 
   TObjArray *mFa =  (TObjArray*)f->Get("mFa"); 
   TObjArray *mFi =  (TObjArray*)f->Get("mFi"); 

   TObjArray *mCRn =  (TObjArray*)f->Get("mCRn"); 
   TObjArray *mCRa =  (TObjArray*)f->Get("mCRa"); 
   TObjArray *mCRi =  (TObjArray*)f->Get("mCRi"); 

   TObjArray *mAni =  (TObjArray*)f->Get("mAni");

   TObjArray *mBPn =  (TObjArray*)f->Get("mBPn");
   TObjArray *mBPi =  (TObjArray*)f->Get("mBPi");
   TObjArray *mBPa =  (TObjArray*)f->Get("mBPa");

   TObjArray *mBCn =  (TObjArray*)f->Get("mBCn");
   TObjArray *mBCi =  (TObjArray*)f->Get("mBCi");
   TObjArray *mBCa =  (TObjArray*)f->Get("mBCa");

   TObjArray *mGPn =  (TObjArray*)f->Get("mGPn");
   TObjArray *mGPi =  (TObjArray*)f->Get("mGPi");
   TObjArray *mGPa =  (TObjArray*)f->Get("mGPa");

   TObjArray *mMno =  (TObjArray*)f->Get("mMno");
//cout<<number<<endl;   

   if(number>0) {
      if(mHam){
      if(number<mHam->GetSize()) {
      	 if(mHam->At(number)) {
	    cout<<"<CENTER>"<< "Hamamatsu data for "<< number<< " APD"<<"</CENTER>"<<endl;
	    MakeScalarsTable(mHam->At(number),TString("mHam"));
	    MakeArraysTable(mHam->At(number),TString("mHam"));
	 }
      }
      }
      if(mGn){
      if(number<mGn->GetSize()) {
      	 if(mGn->At(number)) {
	    cout<<"<CENTER>"<< "Gain new data for "<< number<< " APD"<<"</CENTER>"<<endl;
	    MakeScalarsTable(mGn->At(number),TString("mGn"));
	    MakeArraysTable(mGn->At(number),TString("mGn"));
	 }
      }
      }
      if(mGa){
      if(number<mGa->GetSize()) {
      	 if(mGa->At(number)) {
	    cout<<"<CENTER>"<< "Gain aged data for "<< number<< " APD"<<"</CENTER>"<<endl;
	    MakeScalarsTable(mGa->At(number),TString("mGa"));
	    MakeArraysTable(mGa->At(number),TString("mGa"));
	 }
      }
      }
      if(mGi){
      if(number<mGi->GetSize()) {
      	 if(mGi->At(number)) {
	    cout<<"<CENTER>"<< "Gain irradiated data for "<< number<< " APD"<<"</CENTER>"<<endl;
	    MakeScalarsTable(mGi->At(number),TString("mGi"));
	    MakeArraysTable(mGi->At(number),TString("mGi"));
	 }
      }


      }
      if(mGQn){
      if(number<mGQn->GetSize()) {
      	 if(mGQn->At(number)) {
	    cout<<"<CENTER>"<< "GainQE new data for "<< number<< " APD"<<"</CENTER>"<<endl;
	    MakeScalarsTable(mGQn->At(number),TString("mGQn"));
	    MakeArraysTable(mGQn->At(number),TString("mGQn"));
	 }
      }
      }
      if(mGQa){
      if(number<mGQa->GetSize()) {
      	 if(mGQa->At(number)) {
	    cout<<"<CENTER>"<< "GainQE aged data for "<< number<< " APD"<<"</CENTER>"<<endl;
	    MakeScalarsTable(mGQa->At(number),TString("mGQa"));
	    MakeArraysTable(mGQa->At(number),TString("mGQa"));
	 }
      }
      }
      if(mGQi){
      if(number<mGQi->GetSize()) {
      	 if(mGQi->At(number)) {
	    cout<<"<CENTER>"<< "GainQE irradiated data for "<< number<< " APD"<<"</CENTER>"<<endl;
	    MakeScalarsTable(mGQi->At(number),TString("mGQi"));
	    MakeArraysTable(mGQi->At(number),TString("mGQi"));
	 }
      }



      }
      if(mQn){
      if(number<mQn->GetSize()) {
      	 if(mQn->At(number)) {
	    cout<<"<CENTER>"<< "QE new data for "<< number<< " APD"<<"</CENTER>"<<endl;
	    MakeScalarsTable(mQn->At(number),TString("mQEn"));
	    MakeArraysTable(mQn->At(number),TString("mQEn"));
	 }
      }
      }
      if(mQa){
      if(number<mQa->GetSize()) {
      	 if(mQa->At(number)) {
	    cout<<"<CENTER>"<< "QE aged data for "<< number<< " APD"<<"</CENTER>"<<endl;
	    MakeScalarsTable(mQa->At(number),TString("mQEa"));
	    MakeArraysTable(mQa->At(number),TString("mQEa"));
	 }
      }
      }
      if(mQi){
      if(number<mQi->GetSize()) {
      	 if(mQi->At(number)) {
	    cout<<"<CENTER>"<< "QE irradiated data for "<< number<< " APD"<<"</CENTER>"<<endl;
	    MakeScalarsTable(mQi->At(number),TString("mQEi"));
	    MakeArraysTable(mQi->At(number),TString("mQEi"));
	 }
      }



      }
      if(mFn){
      if(number<mFn->GetSize()) {
      	 if(mFn->At(number)) {
	    cout<<"<CENTER>"<< "ExNoise new data for "<< number<< " APD"<<"</CENTER>"<<endl;
	    MakeScalarsTable(mFn->At(number),TString("mFn"));
	    MakeArraysTable(mFn->At(number),TString("mFn"));
	 }
      }
      }
      if(mFa){
      if(number<mFa->GetSize()) {
      	 if(mFa->At(number)) {
	    cout<<"<CENTER>"<< "ExNoise aged data for "<< number<< " APD"<<"</CENTER>"<<endl;
	    MakeScalarsTable(mFa->At(number),TString("mFa"));
	    MakeArraysTable(mFa->At(number),TString("mFa"));
	 }
      }
      }
      if(mFi){
      if(number<mFi->GetSize()) {
      	 if(mFi->At(number)) {
	    cout<<"<CENTER>"<< "ExNoise irradiated data for "<< number<< " APD"<<"</CENTER>"<<endl;
	    MakeScalarsTable(mFi->At(number),TString("mFi"));
	    MakeArraysTable(mFi->At(number),TString("mFi"));
	 }
      }


      }
      if(mCRn){
      if(number<mCRn->GetSize()) {
      	 if(mCRn->At(number)) {
	    cout<<"<CENTER>"<< "CR new data for "<< number<< " APD"<<"</CENTER>"<<endl;
	    MakeScalarsTable(mCRn->At(number),TString("mCRn"));
	    MakeArraysTable(mCRn->At(number),TString("mCRn"));
	 }
      }
      }
      if(mCRa){
      if(number<mCRa->GetSize()) {
      	 if(mCRa->At(number)) {
	    cout<<"<CENTER>"<< "CR aged data for "<< number<< " APD"<<"</CENTER>"<<endl;
	    MakeScalarsTable(mCRa->At(number),TString("mCRa"));
	    MakeArraysTable(mCRa->At(number),TString("mCRa"));
	 }
      }
      }
      if(mCRi){
	if(number<mCRi->GetSize()) {
	  if(mCRi->At(number)) {
	    cout<<"<CENTER>"<< "CR irradiated data for "<< number<< " APD"<<"</CENTER>"<<endl;
	    MakeScalarsTable(mCRi->At(number),TString("mCRi"));
	    MakeArraysTable(mCRi->At(number),TString("mCRi"));
	  }
	}
      }
      if(mAni){
	if(number<mAni->GetSize()) {
	  if(mAni->At(number)) {
	    cout<<"<CENTER>"<< "Annealing data for "<< number<< " APD"<<"</CENTER>"<<endl;
	    MakeScalarsTable(mAni->At(number),TString("mAni"));
	    MakeArraysTable(mAni->At(number),TString("mAni"));
	  }  
	}
      }
      if(mBPn){
	if(number<mBPn->GetSize()) {
	  if(mBPn->At(number)) {
	    cout<<"<CENTER>"<< "BP new data for "<< number<< " APD"<<"</CENTER>"<<endl;
	    MakeScalarsTable(mBPn->At(number),TString("mBPn"));
	    MakeArraysTable(mBPn->At(number),TString("mBPn"));
	  }  
	}
      }
      if(mBPi){
	if(number<mBPi->GetSize()) {
	  if(mBPi->At(number)) {
	    cout<<"<CENTER>"<< "BP irradiated data for "<< number<< " APD"<<"</CENTER>"<<endl;
	    MakeScalarsTable(mBPi->At(number),TString("mBPi"));
	    MakeArraysTable(mBPi->At(number),TString("mBPi"));
	  }  
	}
      }      
      if(mBPa){
	if(number<mBPa->GetSize()) {
	  if(mBPa->At(number)) {
	    cout<<"<CENTER>"<< "BP aged data for "<< number<< " APD"<<"</CENTER>"<<endl;
	    MakeScalarsTable(mBPa->At(number),TString("mBPa"));
	    MakeArraysTable(mBPa->At(number),TString("mBPa"));
	  }  
	}
      }
      if(mBCn){
	if(number<mBCn->GetSize()) {
	  if(mBCn->At(number)) {
	    cout<<"<CENTER>"<< "BC new data for "<< number<< " APD"<<"</CENTER>"<<endl;
	    MakeScalarsTable(mBCn->At(number),TString("mBCn"));
	    MakeArraysTable(mBCn->At(number),TString("mBCn"));
	  }  
	}
      }
      if(mBCi){
	if(number<mBCi->GetSize()) {
	  if(mBCi->At(number)) {
	    cout<<"<CENTER>"<< "BC irradiated data for "<< number<< " APD"<<"</CENTER>"<<endl;
	    MakeScalarsTable(mBCi->At(number),TString("mBCi"));
	    MakeArraysTable(mBCi->At(number),TString("mBCi"));
	  }  
	}
      }      
      if(mBCa){
	if(number<mBCa->GetSize()) {
	  if(mBCa->At(number)) {
	    cout<<"<CENTER>"<< "BC aged data for "<< number<< " APD"<<"</CENTER>"<<endl;
	    MakeScalarsTable(mBCa->At(number),TString("mBCa"));
	    MakeArraysTable(mBCa->At(number),TString("mBCa"));
	  }  
	}
      }
      if(mGPn){
	if(number<mGPn->GetSize()) {
	  if(mGPn->At(number)) {
	    cout<<"<CENTER>"<< "GP new data for "<< number<< " APD"<<"</CENTER>"<<endl;
	    MakeScalarsTable(mGPn->At(number),TString("mGPn"));
	    MakeArraysTable(mGPn->At(number),TString("mGPn"));
	  }  
	}
      }
      if(mGPi){
	if(number<mGPi->GetSize()) {
	  if(mGPi->At(number)) {
	    cout<<"<CENTER>"<< "GP irradiated data for "<< number<< " APD"<<"</CENTER>"<<endl;
	    MakeScalarsTable(mGPi->At(number),TString("mGPi"));
	    MakeArraysTable(mGPi->At(number),TString("mGPi"));
	  }  
	}
      }      
      if(mGPa){
	if(number<mGPa->GetSize()) {
	  if(mGPa->At(number)) {
	    cout<<"<CENTER>"<< "GP aged data for "<< number<< " APD"<<"</CENTER>"<<endl;
	    MakeScalarsTable(mGPa->At(number),TString("mGPa"));
	    MakeArraysTable(mGPa->At(number),TString("mGPa"));
	  }  
	}
      }
      if(mMno){
	if(number<mMno->GetSize()) {
	  if(mMno->At(number)) {
	    cout<<"<CENTER>"<< "Noise data for "<< number<< " APD"<<"</CENTER>"<<endl;
	    MakeScalarsTable(mMno->At(number),TString("mMno"));
	    MakeArraysTable(mMno->At(number),TString("mMno"));
	  }  
	}
      }
   }     
   else {
     cout<<"Invalid APD number:"<<number<<endl;
   }

	  
/*
   TObjString *key = new TObjString(apdnumber);
   Hamamatsu *m = (Hamamatsu*) mHam->GetValue(key);


//   cout<<m->fTemperature<<endl;

   key = new TObjString(apdnumber);
   Gain *g = (Gain*) mGi->GetValue(key);
//   cout<<g->fMeanTemp<<endl;
//   cout<<g->faVoltage[19]<<endl;
   Bool_t found=kFALSE;
   if(m!=NULL && g!=NULL)  {
   found = kTRUE;
   TCanvas *c1 = new TCanvas("c1",apdnumber,200,10,700,500);
   c1->SetFillColor(42);
   c1->SetGridx();
   c1->SetGridy();
   TGraph *g1 = new TGraph(12,m->faVoltage.GetArray(),m->faDarkCurrent.GetArray());
   g1->SetTitle("DarkCurrent vs Voltage(Hamamatsu data)");
   g1->Draw("AL");


   TCanvas *c2 = new TCanvas("c2",apdnumber,200,10,700,500);
   c2->SetFillColor(42);
   c2->SetGridx();
   c2->SetGridy();
   TGraph *g2 = new TGraph(12,m->faVoltage.GetArray(),m->faCapacitance.GetArray());
   g2->SetTitle("Capacitance vs Voltage(Hamamatsu data)");
   g2->Draw("AC*");

   TCanvas *c3 = new TCanvas("c3",apdnumber,200,10,700,500);
   c3->SetFillColor(42);
   c3->SetGridx();
   c3->SetGridy();
   TGraph *g3 = new TGraph(20,g->faVoltage.GetArray(),g->faItotal.GetArray());
   g3->SetTitle("Itotal vs Voltage(Irradiated)");
   g3->Draw("AC*");

   TCanvas *c4 = new TCanvas("c4",apdnumber,200,10,700,500);
   c4->SetFillColor(42);
   c4->SetGridx();
   c4->SetGridy();
   TGraph *g4 = new TGraph(20,g->faVoltage.GetArray(),g->faGain.GetArray());
   g4->SetTitle("Gain vs Voltage(Irradiated)");
   g4->Draw("AC*");


   c1->SaveAs("/tmp/c1.eps");
   c2->SaveAs("/tmp/c2.eps");
   c3->SaveAs("/tmp/c3.eps");
   c4->SaveAs("/tmp/c4.eps");


   gSystem->Exec("cd /tmp/;pstopnm -ppm -xborder 0 -yborder 0 -portrait c1.eps");
   gSystem->Exec("cd /tmp/;ppmtogif c1.eps001.ppm > /home/dorokhov/apache/htdocs/c1.gif");

   gSystem->Exec("cd /tmp/;pstopnm -ppm -xborder 0 -yborder 0 -portrait c2.eps");
   gSystem->Exec("cd /tmp/;ppmtogif c2.eps001.ppm > /home/dorokhov/apache/htdocs/c2.gif");

   gSystem->Exec("cd /tmp/;pstopnm -ppm -xborder 0 -yborder 0 -portrait c3.eps");
   gSystem->Exec("cd /tmp/;ppmtogif c3.eps001.ppm > /home/dorokhov/apache/htdocs/c3.gif");

   gSystem->Exec("cd /tmp/;pstopnm -ppm -xborder 0 -yborder 0 -portrait c4.eps");
   gSystem->Exec("cd /tmp/;ppmtogif c4.eps001.ppm > /home/dorokhov/apache/htdocs/c4.gif");

   }

   cout<<"-->"<<endl;

   if(found) {
      cout<<"APD "<<apdnumber<< " found in database"<<endl;   
      cout<<"<IMG SRC=\"http://pcephc172.cern.ch:8080/c1.gif\" ALIGN=CENTER>"<<endl;
      cout<<"<IMG SRC=\"http://pcephc172.cern.ch:8080/c2.gif\" ALIGN=CENTER>"<<endl;
      cout<<"<IMG SRC=\"http://pcephc172.cern.ch:8080/c3.gif\" ALIGN=CENTER>"<<endl;
      cout<<"<IMG SRC=\"http://pcephc172.cern.ch:8080/c4.gif\" ALIGN=CENTER>"<<endl;

   }
   else {
      cout<<"APD "<<apdnumber<< " not found in database"<<endl;   
   }
*/
   cout<<"</BODY>"<<endl;
   cout<<"</HTML>"<<endl;
   f->Close();
   delete f;
   if(mHam) mHam->SetOwner();
   if(mGn)mGn->SetOwner();
   if(mGa)mGa->SetOwner();
   if(mGi)mGi->SetOwner();
   
   if(mGQn)mGQn->SetOwner(); 
   if(mGQa)mGQa->SetOwner(); 
   if(mGQi)mGQi->SetOwner(); 

   if(mQn)mQn->SetOwner();
   if(mQa)mQa->SetOwner();
   if(mQi)mQi->SetOwner();

   if(mFn)mFn->SetOwner();
   if(mFa)mFa->SetOwner();
   if(mFi)mFi->SetOwner();

   if(mCRn)mCRn->SetOwner(); 
   if(mCRa)mCRa->SetOwner(); 
   if(mCRi)mCRi->SetOwner(); 

   if(mAni)mAni->SetOwner();

   if(mBPn)mBPn->SetOwner();
   if(mBPi)mBPi->SetOwner();
   if(mBPa)mBPa->SetOwner();

   if(mBCn)mBCn->SetOwner();
   if(mBCi)mBCi->SetOwner();
   if(mBCa)mBCa->SetOwner();

   if(mGPn)mGPn->SetOwner();
   if(mGPi)mGPi->SetOwner();
   if(mGPa)mGPa->SetOwner();

   if(mMno)mMno->SetOwner();

//    mHam->Clear();
//    mGn->Clear(); 
//    mGa->Clear(); 
//    mGi->Clear(); 
//    
//    mGQn->Clear(); 
//    mGQa->Clear(); 
//    mGQi->Clear(); 
// 
//    mQn->Clear(); 
//    mQa->Clear(); 
//    mQi->Clear(); 
// 
//    mFn->Clear(); 
//    mFa->Clear(); 
//    mFi->Clear(); 
// 
//    mCRn->Clear(); 
//    mCRa->Clear(); 
//    mCRi->Clear(); 

   if(mHam)mHam->Delete();
   if(mGn)mGn->Delete(); 
   if(mGa)mGa->Delete(); 
   if(mGi)mGi->Delete(); 
   
   if(mGQn)mGQn->Delete(); 
   if(mGQa)mGQa->Delete(); 
   if(mGQi)mGQi->Delete(); 

   if(mQn)mQn->Delete(); 
   if(mQa)mQa->Delete(); 
   if(mQi)mQi->Delete(); 

   if(mFn)mFn->Delete(); 
   if(mFa)mFa->Delete(); 
   if(mFi)mFi->Delete(); 

   if(mCRn)mCRn->Delete(); 
   if(mCRa)mCRa->Delete(); 
   if(mCRi)mCRi->Delete(); 

   if(mAni)mAni->Delete();

   if(mBPn)mBPn->Delete(); 
   if(mBPa)mBPa->Delete();
   if(mBPi)mBPi->Delete();

   if(mBCn)mBCn->Delete(); 
   if(mBCa)mBCa->Delete();
   if(mBCi)mBCi->Delete();

   if(mGPn)mGPn->Delete(); 
   if(mGPa)mGPa->Delete();
   if(mGPi)mGPi->Delete();

   if(mMno)mMno->Delete();
   return 0;
}




