#ifdef __CINT__

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;

#pragma link C++ class Header;
#pragma link C++ class Hamamatsu;
#pragma link C++ class Gain;
#pragma link C++ class GainQE;
#pragma link C++ class QE;
#pragma link C++ class Noise;
#pragma link C++ class Capacitance;
#pragma link C++ class Annealing;
#pragma link C++ class BRKDPSI;
#pragma link C++ class BRKDCERN;
#pragma link C++ class GAINPSI;
#pragma link C++ class GAINCERN;
#pragma link C++ class Mnoise;
//#pragma link C++ class BuildDB;
#endif
