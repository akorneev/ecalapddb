#include "TFile.h"
#include "TString.h"
#include <string.h>
#include <fstream>
#include <iostream>
#include "Measurement.h"
using std::ifstream;
using std::ofstream;
//______________________________________________________________________
int main( int argc, char **argv ){
  Int_t nomi=0,gmin=40, gmax=400, gminp=100, nhmin,nhmax,fl, n;
  Double_t IDoGp[500], Gp[500], IDoGc[500], Gc[500], IDoGh[20], Gh[20],IDoGhmax;
  ofstream fcernvbg1("1gaincern_vb.txt");
  ofstream fcernvbg2("2gaincern_vb.txt");
  ofstream fcernvb1("1cern_vb.txt");
  ofstream fcernvb2("2cern_vb.txt");
  ofstream fpsivb1("1psi_vb.txt");
  ofstream fpsivb2("2psi_vb.txt");
  ofstream fnoise1("1noise.txt");
  ofstream fidoverM1("1IdoverM.txt");
  ofstream fidoverM2("2IdoverM.txt");
  BRKDCERN *bc1=NULL;
  BRKDCERN *bc2=NULL;
  BRKDPSI *bp1=NULL;
  BRKDPSI *bp2=NULL;
  Hamamatsu *h=NULL;
  Mnoise *no=NULL;
  GAINCERN *bg1=NULL;
  GAINCERN *bg2=NULL;
  /*
  TObjArray *arrg1=NULL;
  TObjArray *arrg2=NULL;
  TObjArray *arrc1=NULL;
  TObjArray *arrc2=NULL;
  TObjArray *arrp1=NULL;
  TObjArray *arrp2=NULL;
  TObjArray *arrh=NULL;  
  */  
  TString line;
  ////
  TFile f("/home/apddata/Double_irr2/measurement.root");
  TObjArray *arrg1=NULL; arrg1=(TObjArray*)f.Get("mGCi");
  TObjArray *arrg2=NULL; arrg2=(TObjArray*)f.Get("mGCa");
  TObjArray *arrc1=NULL; arrc1=(TObjArray*)f.Get("mBCi"); //
  TObjArray *arrc2=NULL; arrc2=(TObjArray*)f.Get("mBCa");  //
  TObjArray *arrp1=NULL; arrp1=(TObjArray*)f.Get("mBPi"); 
  TObjArray *arrp2=NULL; arrp2=(TObjArray*)f.Get("mBPa");
  TObjArray *arrh=NULL; arrh=(TObjArray*)f.Get("mHam");
  //  TObjArray *arrn=(TObjArray*)f.Get("mMno");
 
  if (argc < 2) {
    cout << "Usage: txtgaindata.exe input_file" << endl;
    return 1;
  }
  
  ifstream fin(argv[1]);
  fcernvbg1<<"APDNumber,,TestDone,,,,,,,,,Temperature,Vb,,ID50,,"<<endl;
  fcernvbg2<<"APDNumber,,TestDone,,,,,,,,,Temperature,Vb,,ID50,,"<<endl;
 
  fcernvb1<<"APDNumber,,TestDone,,,,,,,,,Temperature,Vb,,ID50,,"<<endl; 
  fcernvb2<<"APDNumber,,TestDone,,,,,,,,,Temperature,Vb,,ID50,,"<<endl; 

  fpsivb1<<"APDNumber,,TestDone,,,,,,,,,Temperature,Vb,,ID50,,"<<endl; 
  fpsivb2<<"APDNumber,,TestDone,,,,,,,,,Temperature,Vb,,ID50,,"<<endl; 
  //fnoise<<"APDNumber,Noise1,Noise50,Noise150,Noise300"<<endl;
  fidoverM1<<"APD ID,flag,Reason,MIN position"<<endl;
  fidoverM2<<"APD ID,flag,Reason,MIN position"<<endl;

  while(!fin.eof()){
    bc1=NULL;bc2=NULL;bp1=NULL;bp2=NULL;no=NULL;bg1=NULL;bg2=NULL;
    line.ReadLine(fin);
    if(line.Sizeof()<2) continue;
    line.Resize(10);
    nomi=atoi(&line.Data()[4]);
    if(nomi<929000 && nomi>928000) continue;
    if(nomi<=arrg1->GetLast()) bg1= (GAINCERN *) (arrg1->At(nomi));
    if(nomi<=arrg2->GetLast()) bg2= (GAINCERN *) (arrg2->At(nomi));
    //    if(nomi<=arrc1->GetLast()) bc1= (BRKDCERN *) (arrc1->At(nomi));
    //  if(nomi<=arrc2->GetLast()) bc2= (BRKDCERN *) (arrc2->At(nomi));
    if(nomi<=arrp1->GetLast()) bp1= (BRKDPSI *) (arrp1->At(nomi));
    if(nomi<=arrp2->GetLast()) bp2= (BRKDPSI *) (arrp2->At(nomi));
    //    if(nomi<=arrn->GetLast()) no= (Mnoise *) (arrn->At(nomi));
    h= (Hamamatsu *) (arrh->At(nomi));

    n=(h->faIdarkOverGain).GetSize();
    for (Int_t l=0;l<n;l++){
      IDoGh[l]=((h->faIdarkOverGain).GetArray())[l];
      Gh[l]=((h->faGain).GetArray())[l];
    }
    fl=0; nhmin=0; nhmax=0;
    for (Int_t l=1;l<n-2;l++){
      if(Gh[l]>gmin && nhmin==0) nhmin=l+1;
      if(Gh[l+2]>gmax && nhmax==0) nhmax=l+1;
      if(IDoGh[l+1]>1.1*IDoGh[l] && Gh[l]>gmin && Gh[l+1]<gmax && Gh[l+1]>Gh[l]) {
   	if((IDoGh[l+2]>IDoGh[l+1] && Gh[l+2]>Gh[l+1]) || l+2>gmax) {
	  fidoverM1<<line.Data()<<",1,Id/M_Ham,"<<Gh[l]<<endl;
	  fidoverM2<<line.Data()<<",1,Id/M_Ham,"<<Gh[l]<<endl;
	  fl=1;break;
	}
      }    
      else if(IDoGh[l+2]>1.1*IDoGh[l+1] && Gh[l]>gmin && Gh[l+2]>Gh[l+1] && Gh[l+1]<265 && Gh[l+2]>400) {
	fidoverM1<<line.Data()<<",1,Id/M_Ham,"<<Gh[l+1]<<endl;
	fidoverM2<<line.Data()<<",1,Id/M_Ham,"<<Gh[l+1]<<endl;
	fl=1;break;   
      }
    }
    IDoGhmax=IDoGh[nhmin] ;
    for (Int_t l=1;l<n-2;l++){
      if(Gh[l]>gmin && Gh[l+1]<gmax) {if (IDoGhmax<IDoGh[l+1]) IDoGhmax=IDoGh[l+1];}
    }
    if(nhmax==0) nhmax=n-1;
    if(IDoGhmax>1.1*IDoGh[nhmin] && fl==0) {
      fidoverM1<<line.Data()<<",1,Id/M_Ham,"<<Gh[nhmin]<<endl;
      fidoverM2<<line.Data()<<",1,Id/M_Ham,"<<Gh[nhmin]<<endl;
    }

    if(!bc1) {} //{fcernvb<<line.Data()<<"\t"<<"no data"<<endl;}
    else{
      fcernvb1<<line.Data()<<",,a,,,,,,,,,"<<bc1->fTemperature<<","<<bc1->fVB<<",,"<<bc1->fID<<",,"<<endl;
      n=(bc1->faIdarkOverGainfit).GetSize();
      for (Int_t l=0;l<n;l++){
	IDoGc[l]=((bc1->faIdarkOverGainfit).GetArray())[l];
	Gc[l]=((bc1->faGainfit).GetArray())[l];
      }
      for (Int_t l=1;l<n-2;l++){
	if(IDoGc[l+1]>1.05*IDoGc[l] && Gc[l]>gmin && Gc[l]<gmax && Gc[l+1]>Gc[l]) 
	  if(IDoGc[l+2]>IDoGc[l+1] && Gc[l+2]>Gc[l+1])
	    {fidoverM1<<line.Data()<<",100,Id/M_cern,"<<Gc[l]<<endl;break;}
	//{cout<<"CERN "<<nomi<<endl;break;}
      }
    }

    if(!bc2) {} //{fcernvb<<line.Data()<<"\t"<<"no data"<<endl;}
    else{
      fcernvb2<<line.Data()<<",,a,,,,,,,,,"<<bc2->fTemperature<<","<<bc2->fVB<<",,"<<bc2->fID<<",,"<<endl;
      n=(bc2->faIdarkOverGainfit).GetSize();
      for (Int_t l=0;l<n;l++){
	IDoGc[l]=((bc2->faIdarkOverGainfit).GetArray())[l];
	Gc[l]=((bc2->faGainfit).GetArray())[l];
      }
      for (Int_t l=1;l<n-2;l++){
	if(IDoGc[l+1]>1.05*IDoGc[l] && Gc[l]>gmin && Gc[l]<gmax && Gc[l+1]>Gc[l]) 
	  if(IDoGc[l+2]>IDoGc[l+1] && Gc[l+2]>Gc[l+1])
	    {fidoverM2<<line.Data()<<",100,Id/M_cern,"<<Gc[l]<<endl;break;}
	//{cout<<"CERN "<<nomi<<endl;break;}
      }
    }

   if(!bg1) {} //{fcernvb<<line.Data()<<"\t"<<"no data"<<endl;}
    else{
      fcernvbg1<<line.Data()<<",,a,,,,,,,,,"<<bg1->fMeanTemp<<","<<bg1->fVb<<",,"<<bg1->fIdark<<",,"<<endl;
      n=(bg1->faIdarkOverGain).GetSize();
      for (Int_t l=0;l<n;l++){
	IDoGc[l]=((bg1->faIdarkOverGain).GetArray())[l];
	Gc[l]=((bg1->faGain).GetArray())[l];
      }
      for (Int_t l=1;l<n-2;l++){
	if(IDoGc[l+1]>1.05*IDoGc[l] && Gc[l]>gmin && Gc[l]<gmax && Gc[l+1]>Gc[l]) 
	  if(IDoGc[l+2]>IDoGc[l+1] && Gc[l+2]>Gc[l+1])
	    {fidoverM1<<line.Data()<<",100,Id/M_cerngain,"<<Gc[l]<<endl;break;}
	//{cout<<"CERN "<<nomi<<endl;break;}
      }
    }

   if(!bg2) {} //{fcernvb<<line.Data()<<"\t"<<"no data"<<endl;}
    else{
      fcernvbg2<<line.Data()<<",,a,,,,,,,,,"<<bg2->fMeanTemp<<","<<bg2->fVb<<",,"<<bg2->fIdark<<",,"<<endl;
      n=(bg2->faIdarkOverGain).GetSize();
      for (Int_t l=0;l<n;l++){
	IDoGc[l]=((bg2->faIdarkOverGain).GetArray())[l];
	Gc[l]=((bg2->faGain).GetArray())[l];
      }
      for (Int_t l=1;l<n-2;l++){
	if(IDoGc[l+1]>1.05*IDoGc[l] && Gc[l]>gmin && Gc[l]<gmax && Gc[l+1]>Gc[l]) 
	  if(IDoGc[l+2]>IDoGc[l+1] && Gc[l+2]>Gc[l+1])
	    {fidoverM2<<line.Data()<<",100,Id/M_cerngain,"<<Gc[l]<<endl;break;}
	//{cout<<"CERN "<<nomi<<endl;break;}
      }
    }



    if(!bp1) {} //{fpsivb<<line.Data()<<"\t"<<"no data"<<endl;}
    else{
      fpsivb1<<line.Data()<<",,i,,,,,,,,,"<<bp1->fTemperature<<","<<bp1->fVB<<",,"<<bp1->fID<<",,"<<endl;
      n=(bp1->faIdarkOverGainfit).GetSize();
      for (Int_t l=0;l<n;l++){
	IDoGp[l]=((bp1->faIdarkOverGainfit).GetArray())[l];
	Gp[l]=((bp1->faGainfit).GetArray())[l];
      }
      for (Int_t l=1;l<n-2;l++){
	if(IDoGp[l+1]>1.05*IDoGp[l] && Gp[l]>gminp && Gp[l]<gmax && Gp[l+1]>Gp[l]) 
	  if(IDoGp[l+2]>IDoGp[l+1] && Gp[l+2]>Gp[l+1])
	    {fidoverM1<<line.Data()<<",10,Id/M_psi,"<<Gp[l]<<endl;break;}
      }
    }

    if(!bp2) {} //{fpsivb<<line.Data()<<"\t"<<"no data"<<endl;}
    else{
      fpsivb2<<line.Data()<<",,i,,,,,,,,,"<<bp2->fTemperature<<","<<bp2->fVB<<",,"<<bp2->fID<<",,"<<endl;
      n=(bp2->faIdarkOverGainfit).GetSize();
      for (Int_t l=0;l<n;l++){
	IDoGp[l]=((bp2->faIdarkOverGainfit).GetArray())[l];
	Gp[l]=((bp2->faGainfit).GetArray())[l];
      }
      for (Int_t l=1;l<n-2;l++){
	if(IDoGp[l+1]>1.05*IDoGp[l] && Gp[l]>gminp && Gp[l]<gmax && Gp[l+1]>Gp[l]) 
	  if(IDoGp[l+2]>IDoGp[l+1] && Gp[l+2]>Gp[l+1])
	    {fidoverM2<<line.Data()<<",10,Id/M_psi,"<<Gp[l]<<endl;break;}
      }
    }

    
    if(!no) {} //{fnoise1<<line.Data()<<"\t"<<"no data"<<endl;}
    else{
      fnoise1<<line.Data()<<","<<no->fNoise1<<","<<no->fNoise50<<","<<no->fNoise150<<","<<no->fNoise300<<endl;
    } 
  }
  fin.close();
  f.Close();
  //f.Delete("*");
  if(arrg1) delete arrg1;
  // cout<<arrg1<<endl;
  if(arrg2) delete arrg2;
  if(arrc1) delete arrc1;
  if(arrc2) delete arrc2;
  if(arrp1) delete arrp1;
  if(arrp2) delete arrp2;
  if(arrh) delete arrh;
  if(bc1) delete bc1;
  if(bc2) delete bc2;
  if(bp1) delete bp1;
  if(bp2) delete bp2;
  if(bg1) delete bg1;
  if(bg2) delete bg2;
  if(h) delete h;
  if(no) delete no;

  //line.~TString();
}  
/*
 
  cout<<"lya2"<<endl;
  TFile f2("/home/apddata/Double_irr2/measurement.root");
  //TString line;
  cout<<"lya3"<<endl;
  arrg1=new TObjArray(); 

  if(f2.Get("mGCi")) arrg1=(TObjArray*) f2.Get("mGCi");
  //TObjArray *arrg2=(TObjArray*)f.Get("mGCa");
  //TObjArray *arrc1=(TObjArray*)f.Get("mBCi");
  cout<<"lya3.5"<<endl;
  cout<<arrc2<<endl;
  //arrc2=new TObjArray();
  cout<<arrc2<<endl;
  //  cout<<(TObjArray*)f2.Get("mBCa")<<endl;
  //arrc2=(TObjArray*) f2.Get("mBCa");
  //TObjArray* arrc22=(TObjArray*) f2.Get("BCa");
  cout<<"lya3.51"<<endl;
  arrp1=new TObjArray();
  cout<<"lya3.52"<<endl;
  arrp1=(TObjArray*) f2.Get("mBPi"); 
  arrp2=new TObjArray();
  cout<<"lya3.53"<<endl;
  arrp2=(TObjArray*)f2.Get("mBPa");
  cout<<"lya3.54"<<endl;
  arrh=new TObjArray();
  arrh=(TObjArray*)f2.Get("mHam");
  //  TObjArray *arrn=(TObjArray*)f.Get("mMno");
  cout<<"lya4"<<endl;
  //////////////  fin.open(argv[1]);
  ifstream fin(argv[1]);
  //fnoise<<"APDNumber,Noise1,Noise50,Noise150,Noise300"<<endl;
  cout<<"lya5"<<endl;
  while(!fin.eof()){
    bc1=NULL;bc2=NULL;bp1=NULL;bp2=NULL;no=NULL;bg1=NULL;bg2=NULL;
    cout<<"lya6"<<endl;
    line.ReadLine(fin);
    cout<<"lya7"<<endl;
    if(line.Sizeof()<2) continue;
    line.Resize(10);
    nomi=atoi(&line.Data()[4]);
    if(nomi<929000 && nomi>928000) continue;
    cout<<"lya7.5"<<endl;
    bg1=new GAINCERN();
    cout<<"lya7.6"<<endl;
    if(nomi<=arrg1->GetLast()) if(arrg1->At(nomi)) {cout<<"lyaaaa"<<endl; bg1= (GAINCERN *) (arrg1->At(nomi));} 
    cout<<"lya7.7"<<endl;
    //if(nomi<=arrg2->GetLast()) bg2= (GAINCERN *) (arrg2->At(nomi));
    //if(nomi<=arrc1->GetLast()) bc1= (BRKDCERN *) (arrc1->At(nomi));
    bc2=new BRKDCERN();
    cout<<"lya7.8"<<endl;
    if(nomi<=arrc2->GetLast()) bc2= (BRKDCERN *) (arrc2->At(nomi));
    cout<<"lya7.85"<<endl;
    bp1=new BRKDPSI();
    if(nomi<=arrp1->GetLast()) bp1= (BRKDPSI *) (arrp1->At(nomi));
    bp2=new BRKDPSI();
    cout<<"lya7.9"<<endl;
    if(nomi<=arrp2->GetLast()) bp2= (BRKDPSI *) (arrp2->At(nomi));
    //    if(nomi<=arrn->GetLast()) no= (Mnoise *) (arrn->At(nomi));
    h=new Hamamatsu();
    h= (Hamamatsu *) (arrh->At(nomi));
    cout<<"lya8"<<endl;
    n=(h->faIdarkOverGain).GetSize();
    for (Int_t l=0;l<n;l++){
      IDoGh[l]=((h->faIdarkOverGain).GetArray())[l];
      Gh[l]=((h->faGain).GetArray())[l];
    }
    fl=0; nhmin=0; nhmax=0;
    for (Int_t l=1;l<n-2;l++){
      if(Gh[l]>gmin && nhmin==0) nhmin=l+1;
      if(Gh[l+2]>gmax && nhmax==0) nhmax=l+1;
      if(IDoGh[l+1]>1.1*IDoGh[l] && Gh[l]>gmin && Gh[l+1]<gmax && Gh[l+1]>Gh[l]) {
   	if((IDoGh[l+2]>IDoGh[l+1] && Gh[l+2]>Gh[l+1]) || l+2>gmax) {
	  fidoverM1<<line.Data()<<",1,Id/M_Ham,"<<Gh[l]<<endl;
	  fidoverM2<<line.Data()<<",1,Id/M_Ham,"<<Gh[l]<<endl;
	  fl=1;break;
	}
      }    
      else if(IDoGh[l+2]>1.1*IDoGh[l+1] && Gh[l]>gmin && Gh[l+2]>Gh[l+1] && Gh[l+1]<265 && Gh[l+2]>400) {
	fidoverM1<<line.Data()<<",1,Id/M_Ham,"<<Gh[l+1]<<endl;
	fidoverM2<<line.Data()<<",1,Id/M_Ham,"<<Gh[l+1]<<endl;
	fl=1;break;   
      }
    }
    IDoGhmax=IDoGh[nhmin] ;
    for (Int_t l=1;l<n-2;l++){
      if(Gh[l]>gmin && Gh[l+1]<gmax) {(IDoGhmax>IDoGh[l+1])?:IDoGhmax=IDoGh[l+1];}
    }
    if(nhmax==0) nhmax=n-1;
    if(IDoGhmax>1.1*IDoGh[nhmin] && fl==0) {
      fidoverM1<<line.Data()<<",1,Id/M_Ham,"<<Gh[nhmin]<<endl;
      fidoverM2<<line.Data()<<",1,Id/M_Ham,"<<Gh[nhmin]<<endl;
    }
    //
    if(!bc1) {} //{fcernvb<<line.Data()<<"\t"<<"no data"<<endl;}
    else{
      fcernvb1<<line.Data()<<",,a,,,,,,,,,"<<bc1->fTemperature<<","<<bc1->fVB<<",,"<<bc1->fID<<",,"<<endl;
      n=(bc1->faIdarkOverGainfit).GetSize();
      for (Int_t l=0;l<n;l++){
	IDoGc[l]=((bc1->faIdarkOverGainfit).GetArray())[l];
	Gc[l]=((bc1->faGainfit).GetArray())[l];
      }
      for (Int_t l=1;l<n-2;l++){
	if(IDoGc[l+1]>1.05*IDoGc[l] && Gc[l]>gmin && Gc[l]<gmax && Gc[l+1]>Gc[l]) 
	  if(IDoGc[l+2]>IDoGc[l+1] && Gc[l+2]>Gc[l+1])
	    {fidoverM1<<line.Data()<<",100,Id/M_cern,"<<Gc[l]<<endl;break;}
	//{cout<<"CERN "<<nomi<<endl;break;}
      }
    }
    

  
    if(!bc2) {} //{fcernvb<<line.Data()<<"\t"<<"no data"<<endl;}
    else{
      fcernvb2<<line.Data()<<",,a,,,,,,,,,"<<bc2->fTemperature<<","<<bc2->fVB<<",,"<<bc2->fID<<",,"<<endl;
      n=(bc2->faIdarkOverGainfit).GetSize();
      for (Int_t l=0;l<n;l++){
	IDoGc[l]=((bc2->faIdarkOverGainfit).GetArray())[l];
	Gc[l]=((bc2->faGainfit).GetArray())[l];
      }
      for (Int_t l=1;l<n-2;l++){
	if(IDoGc[l+1]>1.05*IDoGc[l] && Gc[l]>gmin && Gc[l]<gmax && Gc[l+1]>Gc[l]) 
	  if(IDoGc[l+2]>IDoGc[l+1] && Gc[l+2]>Gc[l+1])
	    {fidoverM2<<line.Data()<<",100,Id/M_cern,"<<Gc[l]<<endl;break;}
	//{cout<<"CERN "<<nomi<<endl;break;}
      }
    }

   if(!bg1) {} //{fcernvb<<line.Data()<<"\t"<<"no data"<<endl;}
    else{
      fcernvbg1<<line.Data()<<",,a,,,,,,,,,"<<bg1->fMeanTemp<<","<<bg1->fVb<<",,"<<bg1->fIdark<<",,"<<endl;
      n=(bg1->faIdarkOverGain).GetSize();
      for (Int_t l=0;l<n;l++){
	IDoGc[l]=((bg1->faIdarkOverGain).GetArray())[l];
	Gc[l]=((bg1->faGain).GetArray())[l];
      }
      for (Int_t l=1;l<n-2;l++){
	if(IDoGc[l+1]>1.05*IDoGc[l] && Gc[l]>gmin && Gc[l]<gmax && Gc[l+1]>Gc[l]) 
	  if(IDoGc[l+2]>IDoGc[l+1] && Gc[l+2]>Gc[l+1])
	    {fidoverM1<<line.Data()<<",100,Id/M_cerngain,"<<Gc[l]<<endl;break;}
	//{cout<<"CERN "<<nomi<<endl;break;}
      }
    }
   
   if(!bg2) {} //{fcernvb<<line.Data()<<"\t"<<"no data"<<endl;}
    else{
      fcernvbg2<<line.Data()<<",,a,,,,,,,,,"<<bg2->fMeanTemp<<","<<bg2->fVb<<",,"<<bg2->fIdark<<",,"<<endl;
      n=(bg2->faIdarkOverGain).GetSize();
      for (Int_t l=0;l<n;l++){
	IDoGc[l]=((bg2->faIdarkOverGain).GetArray())[l];
	Gc[l]=((bg2->faGain).GetArray())[l];
      }
      for (Int_t l=1;l<n-2;l++){
	if(IDoGc[l+1]>1.05*IDoGc[l] && Gc[l]>gmin && Gc[l]<gmax && Gc[l+1]>Gc[l]) 
	  if(IDoGc[l+2]>IDoGc[l+1] && Gc[l+2]>Gc[l+1])
	    {fidoverM2<<line.Data()<<",100,Id/M_cerngain,"<<Gc[l]<<endl;break;}
	//{cout<<"CERN "<<nomi<<endl;break;}
      }
    }
   


    if(!bp1) {} //{fpsivb<<line.Data()<<"\t"<<"no data"<<endl;}
    else{
      fpsivb1<<line.Data()<<",,i,,,,,,,,,"<<bp1->fTemperature<<","<<bp1->fVB<<",,"<<bp1->fID<<",,"<<endl;
      n=(bp1->faIdarkOverGainfit).GetSize();
      for (Int_t l=0;l<n;l++){
	IDoGp[l]=((bp1->faIdarkOverGainfit).GetArray())[l];
	Gp[l]=((bp1->faGainfit).GetArray())[l];
      }
      for (Int_t l=1;l<n-2;l++){
	if(IDoGp[l+1]>1.05*IDoGp[l] && Gp[l]>gminp && Gp[l]<gmax && Gp[l+1]>Gp[l]) 
	  if(IDoGp[l+2]>IDoGp[l+1] && Gp[l+2]>Gp[l+1])
	    {fidoverM1<<line.Data()<<",10,Id/M_psi,"<<Gp[l]<<endl;break;}
      }
    }

    if(!bp2) {} //{fpsivb<<line.Data()<<"\t"<<"no data"<<endl;}
    else{
      fpsivb2<<line.Data()<<",,i,,,,,,,,,"<<bp2->fTemperature<<","<<bp2->fVB<<",,"<<bp2->fID<<",,"<<endl;
      n=(bp2->faIdarkOverGainfit).GetSize();
      for (Int_t l=0;l<n;l++){
	IDoGp[l]=((bp2->faIdarkOverGainfit).GetArray())[l];
	Gp[l]=((bp2->faGainfit).GetArray())[l];
      }
      for (Int_t l=1;l<n-2;l++){
	if(IDoGp[l+1]>1.05*IDoGp[l] && Gp[l]>gminp && Gp[l]<gmax && Gp[l+1]>Gp[l]) 
	  if(IDoGp[l+2]>IDoGp[l+1] && Gp[l+2]>Gp[l+1])
	    {fidoverM2<<line.Data()<<",10,Id/M_psi,"<<Gp[l]<<endl;break;}
      }
    }

   
  //  if(!no) {} //{fnoise1<<line.Data()<<"\t"<<"no data"<<endl;}
   // else{
    //  fnoise1<<line.Data()<<","<<no->fNoise1<<","<<no->fNoise50<<","<<no->fNoise150<<","<<no->fNoise300<<endl;
   // } 
   
  }
  fin.close();
  //  f2.Close();
  //  f.Delete("*");
  if(arrg1) arrg1->Delete();
  //if(arrg2) arrg2->~TObjArray();
  //if(arrc1) arrc1->~TObjArray();
  if(arrc2) arrc2->Delete();
  if(arrp1) arrp1->Delete();
  if(arrp2) arrp2->Delete();
  if(arrh) arrh->Delete();
  // line.~TString();
  cout<<"lya9"<<endl;
  fcernvb1.close();
  fcernvb2.close();
  fcernvbg1.close();
  fcernvbg2.close();
  fpsivb1.close();
  fpsivb2.close();
  fnoise1.close();
  fidoverM1.close();
  fidoverM2.close();
  cout<<"lya10"<<endl;

*/















