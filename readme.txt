Project
-------

  Interface for screening tests data of CMS ECAL APDs.
  
  Source code:
  
    https://gitlab.cern.ch/akorneev/ecalapddb


CMS APD
-------

The avalanche photodiodes (APDs) are in use for
scintillation light detection of CMS ECAL barrel PbWO4 crystals.

ECAL barrel has 61200 crystals, with two APD mounted to each crystal.
Total APDs installed are 61200 * 2 = 122400 units.
Screening tests were organised to verify good quality of all installed APDs.

two apd:
  https://cds.cern.ch/record/1431477/files/oreach-2001-001_03.jpg?subformat=icon-640
two apd glued to crystal:
  https://cds.cern.ch/record/1431477/files/oreach-2001-001.jpg?subformat=icon-640

some history:
  https://cms.cern/detector/measuring-energy/energy-electrons-and-photons-ecal

timeline:

2000-2004: APD QA/QC measurements
2005-2007: APD assembly to the detector
2008: in operation
2012: one of Higgs event candidates:
  https://cds.cern.ch/record/1606503/files/gammagamma_run194108_evt564224000_ispy_3d-annotated-2.png?subformat=icon-640


Papers
------

(year 1997) CMS ECAL TDR
https://cds.cern.ch/record/349375
=> section "4.1 Avalanche Photodiodes"

(year 2000, preprint)  https://cds.cern.ch/record/478872
(year 2001, nima = 8th Pisa Meeting La Biodola)   https://inspirehep.net/literature/558877
(year 2003, nima)  https://doi.org/10.1016/j.nima.2003.11.102

Double screening tests of the CMS ECAL avalanche photodiodes
https://www.sciencedirect.com/science/article/pii/S0168900205001865


Usage on LXPLUS
---------------

  $ ssh lxplus.cern.ch
  $ git clone https://gitlab.cern.ch/akorneev/ecalapddb.git
  $ cd ecalapddb
  $ make
  $ ./Apdbrowser.exe measurement.root


Usage on Windows
----------------

* Install ROOT
  https://root.cern.ch/download/root_v6.32.08.win64.vc17.exe
  Select the option "Add ROOT to the system PATH for all users" during installation.
  
* Install Visual Studio 2022 Build Tools
  https://visualstudio.microsoft.com/downloads/#build-tools-for-visual-studio-2022

* Install Git client
  https://git-scm.com/downloads

* Open VS command prompt
  Run the vs2022x64.lnk
  Or run Start -> Visual Studio 2022 -> x64 Native Tools

* Checkout / compile sources / run program
  > git clone https://gitlab.cern.ch/akorneev/ecalapddb.git
  > cd ecalapddb
  > nmake -f Makefile.win
  > Apdbrowser.exe measurement.root
