#include "BuildDB.h"
#include "Apdlist.h"

// ROOT
#include <TApplication.h>
#include <TGListBox.h>
#include <TGLabel.h>
#include <TGButton.h>
#include <TGTextEntry.h>
#include <TGMsgBox.h>
#include <TGMenu.h>
#include <TGStatusBar.h>
#include <TGFileDialog.h>

#include <TStyle.h>
#include <TCanvas.h>
#include <TGraph.h>
#include <TF1.h>
#include <TH1.h>
#include <TH2.h>

#include <TROOT.h>
#include <TSystem.h>
#include <TString.h>
#include <TFile.h>
#include <TMap.h>
#include <TObjString.h>
#include <TDataMember.h>
#include <TMethodCall.h>

// c++
#include <vector>
#include <fstream>
#include <sstream>
#include <iostream>
#include <cstring>
using std::vector;
using std::ifstream;
using std::ostringstream;

//TString PlotSelected::fullnomer;
/* Double_t fitf(Double_t *x, Double_t *par)
			 {
			   Double_t arg = x[0];
			   cout<<"fitf3"<<endl;
			   cout<<"param"<<endl; 
			   //cout<<GetParameter(0)<<endl;
			   cout<<par[1]<<" "<<par[2]<<" "<<endl;
			   Double_t fitval = par[1]/(par[0]*(exp(par[1]*(par[2]-arg))-1));
			   cout<<fitval<<endl;
*/

MyFile   *fDBFile = 0;
class PlotCanvas;
PlotCanvas* lastCanvas = 0;

Bool_t loadAPDListFromFile = kFALSE;
TString loadAPDListFile;
Bool_t needSelection = kFALSE;

Int_t *ApdResult = new Int_t[999999];

Int_t isApdDead(Header *h) {
   return ApdResult[h->GetIndex()];
}

void GetDeadApds() {
  //  cout<<"GetDeadApds"<<endl;
   TObjArray *aham = (TObjArray*)(fDBFile->Get("mHam"));
   TObjArray *ag = (TObjArray*)(fDBFile->Get("mGi"));
   
   if (!aham) return;
   if (!ag) return;
   
   TObjArrayIter iter(ag);

    TObject *o;
   while((o=iter.Next())) {
      Gain *g = (Gain*)(o);
      Int_t index=((Header*)o)->GetIndex();
      if(index>=999999 || index<0) {
      	 cout<<"invalid apd index"<< index<<endl;
      }
      else {
	 Hamamatsu *ham = (Hamamatsu*)(aham->At(index));
	 //	 cout<<"GetDeadApds"<<endl; 
	 if(ham) {
	   //cout<<"GetDeadApds"<<endl; 
	    if((ham->fVB - g->fVb)>40.0) ApdResult[index] = 1;
	    else ApdResult[index] = -1;
	   // cout<<"GetDeadApds"<<endl; 
	 } 
      }
   }
   //cout<<"GetDeadApds"<<endl;
}


// list of GUI commands ids
enum {
   M_FILE_OPEN,
   M_FILE_EXIT,
   M_HELP_ABOUT,
   M_BROWSER,
   M_HISTO,
   
   B_UNSELECT,
   B_SELECTLIST,
   B_DRAW,
   B_CANVAS,
   B_SUPER
};

class PlotCanvas : public TCanvas {
public:
   PlotCanvas();
   virtual ~PlotCanvas();
   
   void SaveMe(); // *MENU* 
};

void PlotCanvas::SaveMe()
{
  cout<<"Save me invoked"<<endl;
}

PlotCanvas::PlotCanvas()
: TCanvas(kTRUE)
{
  lastCanvas = this;
  
  if (!GetShowEventStatus()) ToggleEventStatus();
  SetBorderMode(-1);
  SetBorderSize(0);
  SetFillColor(0);
  //gStyle->SetGridStyle(3);
  gStyle->SetGridColor(kGray);
  SetGrid();
  SetBorderMode(0);
}

PlotCanvas::~PlotCanvas()
{
  if (lastCanvas == this) lastCanvas = 0;
}

class Browser : public TGMainFrame {
public:
   TObjArray *fMap;
   TObjArray *fMapFile;
   TString fType;
   TString fExtendedType;
   
   TGListBox *fListBoxLot;
   TGListBox *fListBoxWafer;
   TGListBox *fListBoxAPD;

   TGListBox *fListX;
   TGListBox *fListY;

   TGTextEntry *fTextEntryXmin,*fTextEntryXmax,*fTextEntryYmin,*fTextEntryYmax,
      	       *fTextEntrySkip, *fTextEntrySkipFirst;

   TObjArray *fGraphs;

   TString fMapName;
   
   Browser(const TGWindow *main, UInt_t w, UInt_t h, const char *map,
      	   TString type, TString extendedType);
   virtual ~Browser();
//   void   SetTitle();
   void   Popup();
   void   CloseWindow();
   Bool_t ProcessMessage(Longptr_t msg, Longptr_t parm1, Longptr_t parm2) override;
   
   void Unselect();
   void SelectList();
   void Draw();
   void NewCanvas();
};

TObjArray* loadAPDlist(TString fname)
{
  TObjArray* list = new TObjArray();
  list->SetOwner();
  
  ifstream in(fname);
  
  if (!in) {
    cout << "ERROR: can not open APD list file: " << fname << endl;
    return list;
  }
  
  TString line;
  while (in.good()) {
    line.ReadLine(in);
    line.Resize(10);
    Token t(line, ",");
    while (t.HasNext()) {
      if (strlen(t.Get()) == 10) {
        Header* header = new Header;
        header->fFullSerialNo = t.Get();
        list->AddLast(header);
      }
    }
  }
  
  return list;
}

Browser::Browser(const TGWindow *main, UInt_t w, UInt_t h, const char *map,
                 TString type, TString extendedType)
: TGMainFrame(main, w, h)
{
   fType = type;
   fExtendedType = extendedType;
   fMapName = map;
   fMap = (TObjArray*)fDBFile->Get(map);
   
   fMapFile = loadAPDListFromFile ? loadAPDlist(loadAPDListFile) : 0;

   fGraphs = new TObjArray();
   fGraphs->SetOwner();
   
   TGHorizontalFrame* fContainer = new TGHorizontalFrame(this);
   AddFrame(fContainer, new TGLayoutHints(kLHintsExpandX | kLHintsExpandY));
   
   TGLayoutHints* hints1 = new TGLayoutHints(kLHintsTop | kLHintsLeft | kLHintsExpandY, 10, 0, 0, 0);
   TGLayoutHints* hints2 = new TGLayoutHints(kLHintsTop | kLHintsLeft | kLHintsExpandX, 0, 0, 5, 0);
   TGLayoutHints* hints3 = new TGLayoutHints(kLHintsExpandY, 3, 3, 5, 5);
   
   TGGroupFrame* fContainerList = new TGGroupFrame(fContainer, "APD lot, wafer, serial");
   fContainerList->SetLayoutManager(new TGHorizontalLayout(fContainerList));
   fContainerList->Resize(300,325);
   fContainer->AddFrame(fContainerList, hints1);
   
   fListBoxLot = new TGListBox(fContainerList,200);
   fListBoxLot->Resize(45,300);
   fListBoxLot->SetMultipleSelections(kTRUE);
   fContainerList->AddFrame(fListBoxLot, hints3);
   
   fListBoxWafer = new TGListBox(fContainerList,300);
   fListBoxWafer->Resize(60,300);
   fListBoxWafer->SetMultipleSelections(kTRUE);
   fContainerList->AddFrame(fListBoxWafer, hints3);
   
   fListBoxAPD = new TGListBox(fContainerList,400);
   fListBoxAPD->Resize(100,300);
   fListBoxAPD->SetMultipleSelections(kTRUE);
   fContainerList->AddFrame(fListBoxAPD, hints3);

   if (loadAPDListFromFile) {
      Apdlist_Fill(fListBoxLot,fListBoxWafer,fListBoxAPD,fMapFile);
   }
   else {
      Apdlist_Fill(fListBoxLot,fListBoxWafer,fListBoxAPD,fMap);
   }

   
   TGGroupFrame* fContainerX = new TGGroupFrame(fContainer, "X coordinate");
   fContainerX->Resize(150,325);
   fContainer->AddFrame(fContainerX,hints1);
   
   fListX = new TGListBox(fContainerX,500);
   fListX->Resize(150,300);   
   fContainerX->AddFrame(fListX, hints3);
   
   TGGroupFrame* fContainerY = new TGGroupFrame(fContainer, "Y coordinate");
   fContainerY->Resize(150,325);
   fContainer->AddFrame(fContainerY,hints1);
   
   fListY = new TGListBox(fContainerY,600);
   fContainerY->AddFrame(fListY, hints3);
   fListY->Resize(150,300);
   
   Varlist *varlist = new Varlist(fType,fListX);
   varlist->FillA();
   delete varlist;
   
   varlist = new Varlist(fType,fListY);
   varlist->FillA();
   delete varlist;
   
   TGGroupFrame* fContainerCtl = new TGGroupFrame(fContainer, "Plot control");
   fContainer->AddFrame(fContainerCtl, hints1);
   
   //TGTextButton *fSuperimpose;   

   TGTextButton* fDraw = new TGTextButton(fContainerCtl,"&Draw", B_DRAW);
   fDraw->Associate(this);
   fContainerCtl->AddFrame(fDraw,hints2);
   
   TGTextButton* buttonCanvas = new TGTextButton(fContainerCtl,"&New Canvas", B_CANVAS);
   buttonCanvas->Associate(this);
   fContainerCtl->AddFrame(buttonCanvas, hints2);
   
   TGTextButton* fClearAll = new TGTextButton(fContainerCtl,"&Unselect ", B_UNSELECT);
   fClearAll->Associate(this);
   fContainerCtl->AddFrame(fClearAll,hints2);
   
   TGTextButton* bSelectList = new TGTextButton(fContainerCtl,"Select &List", B_SELECTLIST);
   bSelectList->Associate(this);
   fContainerCtl->AddFrame(bSelectList,hints2);
   
   TGGroupFrame* fContainerOpt = new TGGroupFrame(fContainer, "Plot options");
   fContainerOpt->Resize(100,320);
   fContainer->AddFrame(fContainerOpt,hints1);

   fContainerOpt->AddFrame(new TGLabel(fContainerOpt,"Xmin:"));
   fTextEntryXmin = new TGTextEntry(fContainerOpt);
   fTextEntryXmin->Resize(70,20);
   fContainerOpt->AddFrame(fTextEntryXmin);

   fContainerOpt->AddFrame(new TGLabel(fContainerOpt,"Xmax:"));
   fTextEntryXmax = new TGTextEntry(fContainerOpt);
   fTextEntryXmax->Resize(70,20);
   fContainerOpt->AddFrame(fTextEntryXmax);

   fContainerOpt->AddFrame(new TGLabel(fContainerOpt,"Ymin:"));
   fTextEntryYmin = new TGTextEntry(fContainerOpt);
   fTextEntryYmin->Resize(70,20);
   fContainerOpt->AddFrame(fTextEntryYmin);

   fContainerOpt->AddFrame(new TGLabel(fContainerOpt,"Ymax:"));
   fTextEntryYmax = new TGTextEntry(fContainerOpt);
   fTextEntryYmax->Resize(70,20);
   fContainerOpt->AddFrame(fTextEntryYmax);

   fContainerOpt->AddFrame(new TGLabel(fContainerOpt,"Hide last points:"));
   fTextEntrySkip = new TGTextEntry(fContainerOpt);
   fTextEntrySkip->Resize(70,20);
   fContainerOpt->AddFrame(fTextEntrySkip);

   fContainerOpt->AddFrame(new TGLabel(fContainerOpt,"Hide first points:"));
   fTextEntrySkipFirst = new TGTextEntry(fContainerOpt);
   fTextEntrySkipFirst->Resize(70,20);
   fContainerOpt->AddFrame(fTextEntrySkipFirst);

   //
   SetWindowName("Browser: " + fType + " " + fExtendedType);
   MapSubwindows();
   MapWindow();
   Layout();
}

Browser::~Browser()
{
   delete fMapFile;
   delete fGraphs;
}

void Browser::NewCanvas()
{
  lastCanvas = new PlotCanvas;
}

void Browser::Unselect()
{
  fListBoxLot->SetMultipleSelections(kFALSE);
  fListBoxLot->SetMultipleSelections(kTRUE);
  fListBoxWafer->SetMultipleSelections(kFALSE);
  fListBoxWafer->SetMultipleSelections(kTRUE);
  fListBoxAPD->SetMultipleSelections(kFALSE);
  fListBoxAPD->SetMultipleSelections(kTRUE);
}

/*
void Browser::SelectPrint()
{
  cout << "Browser::SelectPrint()" << endl;
  const Int_t n = fListBoxAPD->GetNumberOfEntries();
  cout << "n=" << n << endl;
  
  TList list;
  fListBoxAPD->GetSelectedEntries(&list); 
  for (TObject* i : list) {
    TGTextLBEntry* e = (TGTextLBEntry*) i;
    const TString serial = e->GetTitle();
    cout << "id=" << e->EntryId()
         << " text=" << serial
         << endl;
  }
}
*/

void Browser::SelectList()
{
  TGFileInfo fi;
  const char* s[] = {"Text file", "*.txt", 0, 0};
  fi.fFileTypes = s;
  new TGFileDialog(gClient->GetRoot(), gClient->GetRoot(), kFDOpen, &fi);
  if (!fi.fFilename) return;
  
  // load the serials list
  ifstream f(fi.fFilename);
  vector<int> list;
  int x;
  while (f >> x) list.push_back(x);
  
  Unselect();
  
  vector<int> list0;
  
  for (int id : list) {
    TGLBEntry* e = fListBoxAPD->GetEntry(id);
    if (e) e->Activate(kTRUE);
    if (!e) list0.push_back(id);
  }
  
  if (list0.size()) {
    ostringstream oss;
    oss << "WARNING: some serials are missing from the database\n"
        << "\n"
        << "List file = " << fi.fFilename << "\n"
        << "List entries, n = " << list.size() << "\n"
        << "\n"
        << "Entries missing in the database, n = " << list0.size() << "\n";
    for (int i : list0) oss << i << " ";
    oss << "\n";
    
    new TGMsgBox(gClient->GetRoot(), this, "Missing serials", oss.str().c_str(),
                 kMBIconExclamation, kMBOk, 0, kVerticalFrame, kTextLeft|kTextTop);
  }
}

void Browser::Draw()
{
  const TString xmin = fTextEntryXmin->GetText();
  const TString xmax = fTextEntryXmax->GetText();
  const TString ymin = fTextEntryYmin->GetText();
  const TString ymax = fTextEntryYmax->GetText();
  const TString skipLast = fTextEntrySkip->GetText();
  const TString skip1st = fTextEntrySkipFirst->GetText();
  
  if (!lastCanvas) {
      lastCanvas = new PlotCanvas;
   }
   
     lastCanvas->cd();
     lastCanvas->Clear();
     
     if (fGraphs) {
       fGraphs->Clear();
       fGraphs->Compress();
     }
     
     PlotSelected *plsel = new PlotSelected(fListBoxLot,fListBoxWafer,
					    fListBoxAPD,fListX,fListY,fMap,fMapFile);
     //cout<<fListX<<endl;
     
     const Int_t nskipLast = skipLast.IsDigit() ? skipLast.Atoi() : 0;
     const Int_t nskip1st = skip1st.IsDigit() ? skip1st.Atoi() : 0;
     plsel->FillGraphs(fType,fExtendedType,fGraphs, nskipLast, nskip1st);
     
   TGraph *agr = new TGraph(2,plsel->fxa,plsel->fya);
   if (!agr) return;
   
   agr->SetLineColor(0);
   agr->SetMarkerStyle(0);
   agr->Draw("ALP");
   //cout<<"fxa="<<(plsel->fxa)[1]<<endl;
   //TObject *qgr=selCanvas->WaitPrimitive("fMapName","Text");
   plsel->SetTitle(agr,fMapName); //17.10
   //qgr->Print();
   if (fType=="Annealing"){
     //     TAttText *newt= new TAttText();
     // newt->SetTextSize(0.5);
     //  (agr->GetHistogram())->SetTitle("APD annealing at 90^{o}C (after Co-irradiation)");//nazv. plota
     (agr->GetHistogram())->SetTitle("APD annealing (after Co-irradiation)");
     
     // TLatex l;
     // l.DrawLatex(0.5,0.95,"90^{o}");
   }
   else (agr->GetHistogram())->SetTitle(fType+" "+fExtendedType);//nazv. plota
   
   //if(plsel) delete plsel;//30.09
   //cout<<"befor Draw"<<endl;
   
   TH1* h = agr->GetHistogram();
   
  const Double_t minix = xmin.IsFloat() ? xmin.Atof() : h->GetXaxis()->GetXmin();
  const Double_t maxix = xmax.IsFloat() ? xmax.Atof() : h->GetXaxis()->GetXmax();
  h->SetAxisRange(minix, maxix);
  
   if (ymin.IsFloat()) h->SetMinimum(ymin.Atof());
   if (ymax.IsFloat()) h->SetMaximum(ymax.Atof());
   
   h->Draw();

   TObjArrayIter io(fGraphs);
   TObject* o;
   
   while ((o=io.Next())) {
     ((TGraph *)o)->Draw("LP");
   }
   
   lastCanvas->Modified();   
   lastCanvas->Update();   
   // fPlotCanvas->Pop();
}


Bool_t Browser::ProcessMessage(Longptr_t msg, Longptr_t parm1, Longptr_t /*parm2*/)
{
   switch (GET_MSG(msg)) {
      case kC_COMMAND:
        switch (GET_SUBMSG(msg)) {
            case kCM_BUTTON:
               switch (parm1) {
      	          case B_UNSELECT: Unselect(); break;
      	          case B_SELECTLIST: SelectList(); break;
      	          case B_DRAW:     Draw(); break;
      	          case B_CANVAS:   NewCanvas(); break;
	       }
         break;
         }
        break;
   }
   
   return kTRUE;
}

void Browser::Popup(){
   MapWindow();
}

void Browser::CloseWindow(){
   UnmapWindow();
//   delete this;
}
///////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////


class Histo : public TGMainFrame {
public:
   TObjArray *fMap;
   TString fType;
   TString fExtendedType;
   
   TGListBox *fListBoxLot;
   TGListBox *fListBoxWafer;
   TGListBox *fListBoxAPD;

   TGListBox *fListX;
   TGListBox *fListY;
   TGListBox *fListZ;

   TGListBox    *fListHist;

   TGTextEntry *fTextEntryXmin,*fTextEntryXmax,*fTextEntryYmin,*fTextEntryYmax,*fTextEntrySkip;

   TObjArray *fHist;
   Int_t fklc,fkl;

   Histo(const TGWindow *main, UInt_t w, UInt_t h, const char *map,
      	   TString type, TString extendedType);
   virtual ~Histo();
//   void   SetTitle();
   void   Popup();
   void   CloseWindow();
   Bool_t ProcessMessage(Longptr_t msg, Longptr_t parm1, Longptr_t parm2) override;
   void Draw();
   void Unselect();
   void SelectList();
   void NewCanvas();
};

Histo::Histo(const TGWindow *main, UInt_t w, UInt_t h, const char *map,
                 TString type, TString extendedType) :
    TGMainFrame( main, w, h)
{
   //cout << "Histo" << endl;
   fType = type;
   fExtendedType = extendedType;
   
   if(!loadAPDListFromFile) fMap = (TObjArray*)fDBFile->Get(map);
   else fMap = loadAPDlist(loadAPDListFile);
   
   fHist = new TObjArray();
   fHist->SetOwner();
   fkl=0;fklc=0;
   
   TGHorizontalFrame* fContainer = new TGHorizontalFrame(this);
   AddFrame(fContainer, new TGLayoutHints(kLHintsExpandX | kLHintsExpandY));
   
   TGLayoutHints* hints1 = new TGLayoutHints(kLHintsTop | kLHintsLeft | kLHintsExpandY, 5, 0, 0, 0);
   TGLayoutHints* hints2 = new TGLayoutHints(kLHintsTop | kLHintsLeft | kLHintsExpandX, 0, 0, 8, 0);
   TGLayoutHints* hints3 = new TGLayoutHints(kLHintsExpandY, 3, 3, 5, 5);

   //
   TGGroupFrame* fContainerList = new TGGroupFrame(fContainer, "APD lot, wafer, serial"); 
   fContainerList->SetLayoutManager(new TGHorizontalLayout(fContainerList));
   fContainerList->Resize(200,515);
   fContainer->AddFrame(fContainerList,hints1);
   
   fListBoxLot = new TGListBox(fContainerList,200);
   fListBoxLot->Resize(45,525);
   fListBoxLot->SetMultipleSelections(kTRUE);
   fContainerList->AddFrame(fListBoxLot, hints3);
   
   fListBoxWafer = new TGListBox(fContainerList,300);
   fListBoxWafer->Resize(60,525);
   fListBoxWafer->SetMultipleSelections(kTRUE);
   fContainerList->AddFrame(fListBoxWafer, hints3);
   
   fListBoxAPD = new TGListBox(fContainerList,400);
   fListBoxAPD->Resize(100,525);
   fListBoxAPD->SetMultipleSelections(kTRUE);
   fContainerList->AddFrame(fListBoxAPD, hints3);
   
   //cout<<"Histo"<<endl;
   Apdlist_Fill(fListBoxLot,fListBoxWafer,fListBoxAPD,fMap);

   TGGroupFrame* fContainerX = new TGGroupFrame(fContainer, "X coordinate");
   fContainerX->Resize(205,500);
   fContainer->AddFrame(fContainerX,hints1);
   
   fListX = new TGListBox(fContainerX,500);
   fListX->Resize(200,500);
   fContainerX->AddFrame(fListX,hints3);
   
   TGGroupFrame* fContainerY = new TGGroupFrame(fContainer, "Y coordinate");
   fContainerY->Resize(205,500);
   fContainer->AddFrame(fContainerY,hints1);
   
   fListY = new TGListBox(fContainerY,600);
   fListY->Resize(200,500);
   fContainerY->AddFrame(fListY, hints3);
   
   TGGroupFrame* fContainerZ = new TGGroupFrame(fContainer, "Z coordinate");
   fContainerZ->Resize(205,500);
   fContainer->AddFrame(fContainerZ,hints1);
   
   fListZ = new TGListBox(fContainerZ,700);
   fListZ->Resize(200,500);
   fContainerZ->AddFrame(fListZ ,hints3);
   
   Varlist *varlist = new Varlist(fListX,fType);
//   varlist->FillS();
   //delete varlist;
   
   varlist = new Varlist(fListY,fType);
//   varlist->FillS();
   //delete varlist;
   
   varlist = new Varlist(fListZ,fType);
//   varlist->FillS();
   //delete varlist;

//
   TGGroupFrame* fContainerCtl = new TGGroupFrame(fContainer, "Plot control");
   fContainer->AddFrame(fContainerCtl,hints1);
   
   TGTextButton* fDraw = new TGTextButton(fContainerCtl,"&Draw", B_DRAW);
   fDraw->Associate(this);
   fContainerCtl->AddFrame(fDraw,hints2);
   
   TGTextButton* buttonCanvas = new TGTextButton(fContainerCtl,"&New Canvas", B_CANVAS);
   buttonCanvas->Associate(this);
   fContainerCtl->AddFrame(buttonCanvas, hints2);
   
   //TGTextButton* fSuperimpose = new TGTextButton(fContainerCtl,"&Superimpose", xxx);
   //fSuperimpose->Associate(this);
   //fContainerCtl->AddFrame(fSuperimpose,hints2);

   TGTextButton* fClearAll = new TGTextButton(fContainerCtl,"&Unselect", B_UNSELECT);
   fClearAll->Associate(this);
   fContainerCtl->AddFrame(fClearAll,hints2);
   
   TGTextButton* bSelectList = new TGTextButton(fContainerCtl,"Select &List", B_SELECTLIST);
   bSelectList->Associate(this);
   fContainerCtl->AddFrame(bSelectList,hints2);
   
   TGTextButton* fSame = new TGTextButton(fContainerCtl,"&Superimpose", B_SUPER);
   fSame->Associate(this);
   fContainerCtl->AddFrame(fSame,hints2);
   
   fListHist = new TGListBox(fContainerCtl,800);
   fListHist->AddEntry("X hist",1);
   fListHist->AddEntry("XY hist",2);
   fListHist->AddEntry("XYZ hist",3);
   fListHist->AddEntry("XY[Z] hist",4);
   fListHist->AddEntry("X-Y hist",5);
   fListHist->AddEntry("XY graph",6);
   fListHist->AddEntry("X(Y-Z) graph",7);
   fListHist->AddEntry("X(Y/Z) graph",8);
   fListHist->AddEntry("X(Y*Z) graph",9);
   fListHist->Select(1);
   fListHist->Resize(100,171); //100,131
   fContainerCtl->AddFrame(fListHist,hints2);

   //
   TGGroupFrame* fContainerOpt = new TGGroupFrame(fContainer, "Plot options");
   fContainerOpt->Resize(100,320);
   fContainer->AddFrame(fContainerOpt,hints1);

   fContainerOpt->AddFrame(new TGLabel(fContainerOpt,"Xmin:"));
   fTextEntryXmin = new TGTextEntry(fContainerOpt);
   fTextEntryXmin->Resize(70,20);
   fContainerOpt->AddFrame(fTextEntryXmin);

   fContainerOpt->AddFrame(new TGLabel(fContainerOpt,"Xmax:"));
   fTextEntryXmax = new TGTextEntry(fContainerOpt);
   fTextEntryXmax->Resize(70,20);
   fContainerOpt->AddFrame(fTextEntryXmax);

   fContainerOpt->AddFrame(new TGLabel(fContainerOpt,"Ymin:"));
   fTextEntryYmin = new TGTextEntry(fContainerOpt);
   fTextEntryYmin->Resize(70,20);
   fContainerOpt->AddFrame(fTextEntryYmin);

   fContainerOpt->AddFrame(new TGLabel(fContainerOpt,"Ymax:"));
   fTextEntryYmax = new TGTextEntry(fContainerOpt);
   fTextEntryYmax->Resize(70,20);
   fContainerOpt->AddFrame(fTextEntryYmax);
   
   //
   SetWindowName("Histo: " + fType + " " + fExtendedType);
   MapSubwindows();
   MapWindow();
   Layout();
}

Histo::~Histo()
{
   if (loadAPDListFromFile) delete fMap;
   delete fHist;
}

void Histo::Draw()
{
  if(fkl==0) fklc=0;
  
  const Int_t nopt = 4; 
  char *sopt[nopt];
  const char *siopt[nopt];
  siopt[0] = fTextEntryXmin->GetText();siopt[1] = fTextEntryXmax->GetText();
  siopt[2] = fTextEntryYmin->GetText();siopt[3] = fTextEntryYmax->GetText();
  Bool_t *isN = new Bool_t[nopt];
  for(int iss=0;iss<nopt;iss++) {
    unsigned int isss=0;
    sopt[iss] = new char[strlen(siopt[iss])+1]; 
    isN[iss]=kTRUE;
    for(unsigned int is=0;is<strlen(siopt[iss]);is++) {       
      if(siopt[iss][is]!=' ') {strncpy(&sopt[iss][isss],&siopt[iss][is],1); isss++;}	 
    }
    sopt[iss][isss]='\0';
  }
  for(int iss=0;iss<nopt;iss++) {
    
    if(strlen(sopt[iss])==0) { 
      isN[iss]=kFALSE;
    } 
    else {
       if(!Token::isNum(sopt[iss])) {
	 isN[iss]=kFALSE;	
	 cout<<"option is not a number "<<sopt[iss]<<endl;
       }
    } 
  }
  
  if(fkl==0){
  if(fHist!=NULL){ fHist->SetOwner(); delete fHist;}
  fHist= new TObjArray();
  
  //Find or create canvas to plot
  //   if(fkl!=1){
  //if(fkl!=1){   //Apr 19
  
  if (!lastCanvas) {
    lastCanvas = new PlotCanvas;
  }
  } //Apr 19
  
  if(fListHist->GetSelectedEntry() == NULL) return;
  
    if(fkl==0) lastCanvas->Clear();                                       //Apr 18
    //fPlotCanvas->Clear();
    lastCanvas->cd();
  
    HistSelected *plsel = new HistSelected(fListBoxLot,fListBoxWafer,fListBoxAPD,
					   fListX,fListY,fListZ,fMap);
    plsel->SetCanvas(this);
    plsel->FillHist(fType,fExtendedType,fHist,fListHist,fDBFile,isN,sopt);
    TString selstring = ((((TGTextLBEntry *)fListHist->GetSelectedEntry())
			  ->GetText())->GetString());
    if(fHist==NULL) return;

    if(fHist->First()==NULL) return;
    
    TString name = ((fHist->First())->IsA())->GetName();
    if(!name.CompareTo("TH1F")) {
      TObjArrayIter io(fHist);
      TObject* o;
      if((o=io.Next())) {
	TH1F *h  = ((TH1F*)o);
	if(fkl!=0) {h->Draw("same");} else {h->Draw();}  //Apr 18
	//if(fkl!=1)  h->Draw();
	//h->Draw();
	//if(fkl!=0)  ((TH1F *)o)->SetLineColor(fklc);
	while((o=io.Next())) {
	  TString name1 = ((o->IsA())->GetName());
	  if(!name1.CompareTo("TH1F")) {
	    if(fkl!=0)  ((TH1F *)o)->SetLineColor(fklc);
	    ((TH1F *)o)->Draw("same");
	    //if(fkl!=0)  ((TH1F *)o)->SetLineColor(fklc);
	  }
	}
      }
    }
    if(!name.CompareTo("TH2F")) {
      ((TH2F*)(fHist->First()))->Draw("lego2");
    }
    
    if(!name.CompareTo("TGraph")) {
      TGraph *g1 = ((TGraph*)(fHist->First()));
      g1->Draw("WA*");
      cout<<g1->GetYaxis()->GetXmin()<<endl;
      g1->SetTitle(plsel->fTitle);
      g1->GetXaxis()->SetTitle(plsel->fTitleX);
      g1->GetXaxis()->CenterTitle(kTRUE);
      g1->GetYaxis()->SetTitle(plsel->fTitleY);
      g1->GetYaxis()->CenterTitle(kTRUE);            
    }
    
    //   if(plsel)delete plsel;
    
    //if(plsel!=NULL) delete plsel; //cout<<"plselin"<<endl;
    
    lastCanvas->Modified();   
    lastCanvas->Update();
    
  fkl=0;
}

void Histo::Unselect()
{
  fListBoxLot->SetMultipleSelections(kFALSE);
  fListBoxLot->SetMultipleSelections(kTRUE);
  fListBoxWafer->SetMultipleSelections(kFALSE);
  fListBoxWafer->SetMultipleSelections(kTRUE);
  fListBoxAPD->SetMultipleSelections(kFALSE);
  fListBoxAPD->SetMultipleSelections(kTRUE);
}


// TODO: fix copy-paste with Browser::SelectList()
void Histo::SelectList()
{
  TGFileInfo fi;
  const char* s[] = {"Text file", "*.txt", 0, 0};
  fi.fFileTypes = s;
  new TGFileDialog(gClient->GetRoot(), gClient->GetRoot(), kFDOpen, &fi);
  if (!fi.fFilename) return;
  
  // load the serials list
  ifstream f(fi.fFilename);
  vector<int> list;
  int x;
  while (f >> x) list.push_back(x);
  
  Unselect();
  
  vector<int> list0;
  
  for (int id : list) {
    TGLBEntry* e = fListBoxAPD->GetEntry(id);
    if (e) e->Activate(kTRUE);
    if (!e) list0.push_back(id);
  }
  
  if (list0.size()) {
    ostringstream oss;
    oss << "WARNING: some serials are missing from the database\n"
        << "\n"
        << "List file = " << fi.fFilename << "\n"
        << "List entries, n = " << list.size() << "\n"
        << "\n"
        << "Entries missing in the database, n = " << list0.size() << "\n";
    for (int i : list0) oss << i << " ";
    oss << "\n";
    
    new TGMsgBox(gClient->GetRoot(), this, "Missing serials", oss.str().c_str(),
                 kMBIconExclamation, kMBOk, 0, kVerticalFrame, kTextLeft|kTextTop);
  }
}

void Histo::NewCanvas()
{
  lastCanvas = new PlotCanvas;
}

Bool_t Histo::ProcessMessage(Longptr_t msg, Longptr_t parm1, Longptr_t /*parm2*/)
{
  switch (GET_MSG(msg)) {
    case kC_COMMAND:
      switch (GET_SUBMSG(msg)) {
            case kCM_BUTTON:
              switch (parm1) {
      	          case B_UNSELECT: Unselect(); break;
      	          case B_SELECTLIST: SelectList(); break;
      	          case B_DRAW:     Draw(); break;
      	          case B_CANVAS:   NewCanvas(); break;
	                case B_SUPER:    { fkl=1; fklc++; } break;
	       }
              break;
      }
      break;
  }
  
  return kTRUE;
}


void Histo::Popup(){
   MapWindow();
}

void Histo::CloseWindow(){
   UnmapWindow();
//   delete this;
}


struct MenyEntry {
  const char* map;
  const char* type;
  const char* ext;
  
  TString display_type() const
  {
     // rename "Noise" -> "ExNoise" for datasets "mFn", "mFi", "mFa"
     // it is not done inside menudata[].type as "Noise" has some special meaning inside machinery (as I can see)
     const TString m = map;
     const bool is_ex_noise = (m == "mFn" || m == "mFi" || m == "mFa");
     const TString newtype = (is_ex_noise ? "ExNoise" : type);
     
     return newtype;
  }
  
  // return menu name
  TString menu() const
  {
     return display_type() + " " + ext;
  }
};

const int NMENU = 30;

const MenyEntry menudata[NMENU] = {
  {"mHam", "Hamamatsu", "New"},
  {"mGn", "Gain", "New"},
  {"mGi", "Gain", "Irradiated"},
  {"mGa", "Gain", "Aged"},
  {"mQn", "QE", "New"},
  {"mQi", "QE", "Irradiated"},
  {"mQa", "QE", "Aged"},
  {"mGQn", "GainQE", "New"},
  {"mGQi", "GainQE", "Irradiated"},
  {"mGQa", "GainQE", "Aged"},
  {"mFn", "Noise", "New"},
  {"mFi", "Noise", "Irradiated"},
  {"mFa", "Noise", "Aged"},
  {"mCRn", "Capacitance", "New"},
  {"mCRi", "Capacitance", "Irradiated"},
  {"mCRa", "Capacitance", "Aged"},
  {"mAni", "Annealing", " "},
  {"mBPn", "BRKDPSI", "New"},
  {"mBPi", "BRKDPSI", "Irradiated"},
  {"mBPa", "BRKDPSI", "Aged"},
  {"mBCn", "BRKDCERN", "New"},
  {"mBCi", "BRKDCERN", "Irradiated"},
  {"mBCa", "BRKDCERN", "Aged"},
  {"mGPn", "GAINPSI", "New"},
  {"mGPi", "GAINPSI", "Irradiated"},
  {"mGPa", "GAINPSI", "Aged"},
  {"mGCn", "GAINCERN", "New"},
  {"mGCi", "GAINCERN", "Irradiated"},
  {"mGCa", "GAINCERN", "Aged"},
  {"mMno", "Noise", "mult"}
};

class BrowserMainFrame : public TGMainFrame {
private:
   TGLabel* fileInfoLabel;
   TGTextEntry   *fBrowserText;
   TGButton      *fBrowserButton;
   TGCheckButton *fCheckButton;
   TGCheckButton *fCheckButtonFile;
   
   TGPopupMenu* menuBrowser;
   TGPopupMenu* menuHisto;

public:
   BrowserMainFrame(const TGWindow *p, UInt_t w, UInt_t h);
   virtual ~BrowserMainFrame();

   virtual void CloseWindow();
   virtual Bool_t ProcessMessage(Longptr_t msg, Longptr_t parm1, Longptr_t parm2) override;
   void OpenFile(TString datafile = "");
   void OnHelpAbout();
   void OnNewBrowser(int id);
   void OnNewHisto(int id);
};

BrowserMainFrame::BrowserMainFrame(const TGWindow* p, UInt_t w, UInt_t h)
: TGMainFrame(p, w, h)
{
   // main menu
   TGMenuBar* menu = new TGMenuBar(this, 0, 0, 0);
   AddFrame(menu);
   
   TGPopupMenu* menuFile    = menu->AddPopup("&File");
   menuBrowser = menu->AddPopup("&Browser");
   menuHisto   = menu->AddPopup("H&isto");
   TGPopupMenu* menuHelp    = menu->AddPopup("&Help");
   
   // Associate() to let menu connection to BrowserMainFrame::ProcessMessage()
   menuFile->Associate(this);
   menuHelp->Associate(this);
   menuBrowser->Associate(this);
   menuHisto->Associate(this);
   
   menuFile->AddEntry("&Open...", M_FILE_OPEN);
   menuFile->AddEntry("E&xit", M_FILE_EXIT);
   
   menuHelp->AddEntry("&About", M_HELP_ABOUT);
   
   const int N = NMENU;
   TString lasttype = menudata[0].type;
   
   for (intptr_t i = 0; i < N; ++i) {
     const MenyEntry e = menudata[i];
     
     if (lasttype != e.type) {
       menuBrowser->AddSeparator();
       menuHisto->AddSeparator();
       lasttype = e.type;
     }
     
     menuBrowser->AddEntry(e.menu(), M_BROWSER, (void*)i);
     menuHisto->AddEntry(e.menu(), M_HISTO, (void*)i);
   }
   
   // main content
   //TGHorizontalFrame* frameMain = new TGHorizontalFrame(this);
   //AddFrame(frameMain, new TGLayoutHints(kLHintsExpandX | kLHintsExpandY));
   
   // file contents details
   fileInfoLabel = new TGLabel(this);
   fileInfoLabel->SetTextJustify(kTextLeft | kTextTop);
   AddFrame(fileInfoLabel, new TGLayoutHints(kLHintsExpandX | kLHintsExpandY));
   //fileInfoLabel->SetText(oss.str().c_str());
   // force label resize to fit to new text
   //((TGFrame*)fileInfoLabel->GetParent()->GetParent())->Layout();

   
   // a few controls
   //fBrowserButton = new TGTextButton(this, "&  ", 150);
   //fBrowserButton->SetToolTipText("Load APD list");
   //AddFrame(fBrowserButton);
   
   fCheckButton = new TGCheckButton(this, "Highlight dead Apds", 150);
   AddFrame(fCheckButton);
   
   fCheckButtonFile = new TGCheckButton(this, "Use file for APD list:", 160);
   AddFrame(fCheckButtonFile);
   
   fBrowserText = new TGTextEntry("", this, 170);
   fBrowserText->Resize(300, fBrowserText->GetDefaultHeight());
   AddFrame(fBrowserText);
   
   // status bar
   TGStatusBar* statusBar = new TGStatusBar(this); 
   AddFrame(statusBar, new TGLayoutHints(kLHintsExpandX, 0, 0, 2));
   
   //
   SetWindowName("Apdbrowser");
   MapSubwindows();
   MapWindow();
   Layout();
  
   // open the file specified on the command line
   const TString datafile = (gApplication->Argc() < 2) ? "" : gApplication->Argv()[1];
   //cout << "datafile = " << datafile << endl;
   OpenFile(datafile);
}

BrowserMainFrame::~BrowserMainFrame()
{
   delete fDBFile;
}

void redraw(TGWindow* window)
{
  gClient->NeedRedraw(window);
  gSystem->ProcessEvents();
}



void BrowserMainFrame::OpenFile(TString datafile)
{
  if (datafile == "") {
    // data file name is not specified, show file open dialog
    TGFileInfo fi;
    const char* mask[] = {"ROOT file", "*.root", 0, 0};
    fi.fFileTypes = mask;
    new TGFileDialog(gClient->GetRoot(), gClient->GetRoot(), kFDOpen, &fi);
    if (!fi.fFilename) return; // dialog is cancelled
    
    datafile = fi.fFilename;
  }
  
  // load data file
  fileInfoLabel->SetText("Loading file...");
  redraw(fileInfoLabel);
  delete fDBFile;
  fDBFile = new MyFile(datafile, "READ");
  
  if (!fDBFile) {
    cout << "Error in file:" << datafile << endl;
  }
  
  GetDeadApds();
  
  ostringstream oss;
  oss << "File " << datafile << " contents:" << endl;
  
  // update menu to indicate entries with data
  for (intptr_t i = 0; i < NMENU; ++i) {
    const MenyEntry e = menudata[i];
    TObjArray* map = (TObjArray*) fDBFile->Get(e.map);
    const int size = map ? map->GetEntries() : 0;
    
    oss << "   " << e.menu() << " " << size << " entries" << endl;
    
    if (size > 0) {
      menuBrowser->CheckEntryByData((void*)i);
      menuHisto->CheckEntryByData((void*)i);
    }
    else {
      menuBrowser->UnCheckEntryByData((void*)i);
      menuHisto->UnCheckEntryByData((void*)i);
    }
  }
  
  fileInfoLabel->SetText(oss.str().c_str());
}

void BrowserMainFrame::OnHelpAbout()
{
  new TGMsgBox(gClient->GetRoot(), this, "About", 
    "Apdbrowser\n"
    "  builded on " __DATE__ " " __TIME__ "\n"
    "  with ROOT " ROOT_RELEASE "\n"
    "\n"
    "Authors:\n"
    "  2000-2005: Andrei Dorokhov, Andrei Kouznetsov\n"
    "  2016,2024: Anton Karneyeu (INR, Moscow)\n",
    kMBIconAsterisk, kMBOk, 0, kVerticalFrame, (kTextLeft | kTextTop));
}

void BrowserMainFrame::OnNewBrowser(int id)
{
  // new Browser window
  const MenyEntry e = menudata[id];
  Browser* b = new Browser(0, 950, 350, e.map, e.type, e.ext);
  b->Popup();
}

void BrowserMainFrame::OnNewHisto(int id)
{
  // new Histo window
  const MenyEntry e = menudata[id];
  Histo* h = new Histo(0, 1250, 600, e.map, e.type, e.ext);
  h->Popup();
}

void BrowserMainFrame::CloseWindow()
{
   TGMainFrame::CloseWindow();
   gApplication->Terminate(0);
}

Bool_t BrowserMainFrame::ProcessMessage(Longptr_t msg, Longptr_t parm1, Longptr_t parm2)
{
   // https://root.cern.ch/doc/master/WidgetMessageTypes_8h_source.html
   /*
   cout << "BrowserMainFrame::ProcessMessage()"
        << " msg = " << GET_MSG(msg)
        << " submsg = " << GET_SUBMSG(msg)
        << " parm1 = " << parm1
        << " parm2 = " << parm2
        << endl;
   */
   
   switch (GET_MSG(msg)) {
      case kC_TEXTENTRY:
         switch (GET_SUBMSG(msg)) {
            case kTE_TEXTCHANGED:
               if (parm1 == 170) { loadAPDListFile = fBrowserText->GetText(); }
               break;
         }
         break;

      case kC_COMMAND:
         switch (GET_SUBMSG(msg)) {
            case kCM_CHECKBUTTON:
               if (parm1 == 150) { needSelection = fCheckButton->IsOn(); }
               if (parm1 == 160) { loadAPDListFromFile = fCheckButtonFile->IsOn(); }
              break;
      
            case kCM_MENU:
               switch (parm1) {
                  case M_FILE_OPEN:  OpenFile(); break;
                  case M_FILE_EXIT:  CloseWindow(); break; // this also terminates theApp
                  case M_HELP_ABOUT: OnHelpAbout(); break;
                  case M_BROWSER:    OnNewBrowser(parm2); break;
                  case M_HISTO:      OnNewHisto(parm2); break;
               }
            break;
         }
        break;
   }
   
   return kTRUE;
}

//---- Main program ------------------------------------------------------------

int main(int argc, char* argv[])
{
  if (argc != 2) {
    cout << "Usage: ./Apdbrowser.exe db_root_file" << endl;
  }
  
  TApplication* app = new TApplication("Apdbrowser", &argc, argv);
  new BrowserMainFrame(0, 300, 200);
  
  app->Run(kTRUE);
  return 0;
}

#ifdef WIN32
#include <windows.h>

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow) 
{ 
  return main(__argc, __argv); 
}
#endif
