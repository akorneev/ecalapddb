#include "BuildDB.h"

#include <TGListBox.h>
#include <TH1.h>
#include <TH2.h>
#include <TFile.h>
#include <TMap.h>
#include <TGraph.h>

void Apdlist_Fill(TGListBox* Lot, TGListBox* Wafer, TGListBox* APD, TObjArray* mmap);

class Varlist {
private:
   TGListBox   *fList;
//   TMap        *fMap;
   
   
//   TMapIter    *imap ;
   TObject     *omap;
   TClass      *clmap; 
   TList       *clmaplist;
   TListIter   *clmaplistiter;
   Int_t        i;
   TObject     *o; 
   
public:
   Varlist(TString, TGListBox *lb);
   Varlist(TGListBox *lb,TString);
   void FillA();
   void FillS();
   virtual ~Varlist();
};


   
class PlotSelected {   
public:
   Int_t fSize;
   TString fXcoord;
   TString fYcoord;
   TObject **fObjarr;
   Float_t fxa[2];
   Float_t fya[2];

   PlotSelected(TGListBox *fListBoxLot,TGListBox *fListBoxWafer,TGListBox *fListBoxAPD,TGListBox *fListX, 
      	        TGListBox *fListY,  TObjArray *fMap,  TObjArray *fMapFile);
   virtual ~PlotSelected();
   void FillGraphs( TString fType, TString fExtendedType, 
      	            TObjArray *fGraphs,Int_t fSkip ,Int_t);
   void SetTitle(TGraph *,TString);
};


class HistSelected {   
public:
   Int_t fSize;
   TArrayI *arr;
   TString fXcoord;
   TString fYcoord;
   TString fZcoord;
   TString fXmap;
   TString fYmap;
   TString fZmap;
   TArrayF **fXarr;
   TArrayF **fYarr;
   TArrayF **fZarr;
//   TObject **fObjarr;

   TString fXunits;
   TString fYunits;
   TString fZunits;


   Float_t *x;
   Float_t xmax,xmin;
   Float_t *y;
   Float_t ymax,ymin;
   Float_t *z;
   Bool_t *index;
   Int_t *indexc;

   TString fTitle,fTitleX,fTitleY;

   Float_t *fxa,*fya;

   Float_t fxmin,fxmax;
   Float_t fymin,fymax;
   TGWindow *fCanvas;

   HistSelected(TGListBox *fListBoxLot,TGListBox *fListBoxWafer,TGListBox *fListBoxAPD,
      	        TGListBox *fListX, TGListBox *fListY,TGListBox *fListZ, TObjArray *fMap);
   virtual ~HistSelected();
   void FillHist( TString fType, TString fExtendedType, 
      	            TObjArray *fHisto,TGListBox *fListHist, TFile *fDBFile,
		    Bool_t* , char **);

   Float_t GetVar(TObject *ox,const char *var);
   void LoadX(TFile *);
   void LoadXY(TFile *);
   void LoadX_Y(TFile *);
   void LoadXYZ(TFile *);
   void SetTitle(TH2F *h,TString tit,TString titx,TString tity);
   void SetTitle(TH1F *h,TString tit,TString titx);
   void SetTitle(TGraph *g,TString tit,TString titx, TString tity);
   void SetCanvas(TGWindow *c) {fCanvas=c;};

};

