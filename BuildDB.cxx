////////////////////////////////////////////////////////////////////////
// 24.10.2000 by Andrei Dorokhov
// The program is developing and supporting by Andrei Kouznetsov
//
////////////////////////////////////////////////////////////////////////

#define NOMAKEST
#include "BuildDB.h"

#include "TGraph.h"
#include "TF1.h"
#include "TSystemDirectory.h"

using std::ios;
using std::ifstream;

static Hamamatsu **sham;
extern Bool_t doUpdate;
//ClassImp(BuildDB)
Double_t BuildDB::par0a[999999];
Double_t BuildDB::par1a[999999]; 
Double_t BuildDB::Vba[999999];
Double_t BuildDB::Temph[999999];
//Double_t BuildDB::errpar1a[999999];
//Double_t BuildDB::errpar0a[999999];


BuildDB::BuildDB(TString s)
{
   //cout<<"Directory:"<<s<<endl;
   
   fDBPath = s;
   fFiles = new TObjArray;
   
   // get contents of the directory fDBPath
   TSystemDirectory dire(fDBPath, fDBPath);
   const TList* list = dire.GetListOfFiles();
   if (!list) return;
   
   TIter next(list);
   TSystemFile* file;
   while ((file = (TSystemFile*) next())) {
      // skip directories
      if (file->IsDirectory()) continue;
      
      const TString fullpath = fDBPath + "/" + file->GetName();
      //cout<<"fullpath"<<fullpath<<endl;
      
      TObjString* tos = new TObjString(fullpath);
      fFiles->Add(tos);
   }
   
   delete list;
}

void BuildDB::FillHamamatsu(TObjArray *mHam) {   
  Long_t fnpn;
  TString hmark = "S8148";
  //cout<<"fFilesFillHamamatsu"<<fFiles<<endl;
  if(fFiles == NULL) return;
  TObjArrayIter *dbfi = new TObjArrayIter(fFiles);  
  TObject *fno;
  ifstream in;
  TString file,line;

  while((fno = dbfi->Next()) != 0) {
    file = ((TObjString*)fno)->GetString();
    cout<<"FillHamamatsuData, file:"<<file<<endl;
    if(!(file.EndsWith(".csv"))) continue;           
    in.open(file, ios::in);
    while(in.good()) {
      line.ReadLine(in); 
      //cout<<"line:::"<<line<<endl;
      Hamamatsu *fm = new Hamamatsu();      
      sham = &fm;     
         Token *t = new Token(line,","); 
         if(t->HasNext()) {
	   if(!hmark.CompareTo(t->Get())) {    
               if(t->HasNext()) fm->fDate = t->Get();
               if(t->HasNext()) fm->SetSerialNo(t->Get());
               if(t->HasNext()) fm->SetPosition(t->Get());	       
               if(t->HasNext()) fm->fTemperature = t->GetD();
	       fm->fnp1 = 13;
	       fm->fnp2 = 12;
	       (fm->fLot>61 || fm->fLot<25)?fnpn=13:fnpn=12;  
		   
		   // numberA = fm->fSNo;
	       //cout<<"Number"<<numberA<<endl;	       
               if(t->HasNext()) SetVar(fm,"fI0Reference",t->GetD()*1.0e9);
               if(t->HasNext()) SetVar(fm,"fIDReference",t->GetD()*1.0e9);
               if(t->HasNext()) SetVar(fm,"fVB",t->GetD());
               if(t->HasNext()) SetVar(fm,"fI0",t->GetD()*1.0e9);
               if(t->HasNext()) SetVar(fm,"fVR",t->GetD());
               if(t->HasNext()) SetVar(fm,"fID",t->GetD()*1.0e9);
               if(t->HasNext()) SetVar(fm,"fNoise",pow(10.0,-(t->GetD())/20.0)*1.0e5);
               if(t->HasNext()) SetVar(fm,"fCt",t->GetD());              

               SetArr(fm,"faVoltage",fm->fnp1);
               SetArr(fm,"faDarkCurrent",fm->fnp1);
               //SetArr(fm,"faTotalCurrent",fm->fnp1);//Mon Sep 30
               SetArr(fm,"faTotalCurrent",fnpn);
	       SetArr(fm,"faNoise",fm->fnp2);
               SetArr(fm,"faCapacitance",fm->fnp2);	       	        

               for(Int_t i=0; i<fm->fnp1; i++)    
                  if(t->HasNext()) SetArr(fm,"faVoltage",i, t->GetD());
               for(Int_t i=0; i<fm->fnp1; i++)    
                  if(t->HasNext()) SetArr(fm,"faDarkCurrent",i, t->GetD()*1.0e9);
               //for(Int_t i=0; i<fm->fnp2; i++)
	       for(Int_t i=0; i<fnpn; i++)
                  if(t->HasNext()) SetArr(fm,"faTotalCurrent",i, t->GetD()*1.0e9);
               for(Int_t i=0; i<fm->fnp2; i++)    
		 if(t->HasNext()) SetArr(fm,"faNoise",i, pow(10.0,-(t->GetD())/20.0)*1.0e5); //vich. noise
               for(Int_t i=0; i<fm->fnp2; i++)    
		 if(t->HasNext()) SetArr(fm,"faCapacitance",i, t->GetD());   //dve strochki                
               if(t->Tokens()>1 && t->NoError() ) {
//cout<<"begin calculations"<<endl;
// Calculations
		 Float_t idoverg, vr;               //is Ham. novii dannii
                  if(GetArr(fm,"faVoltage") !=NULL && GetArr(fm,"faDarkCurrent")!=NULL &&
                        GetArr(fm,"faTotalCurrent") != NULL) {   
      	             IdOverGainHam(fm->fSNo,idoverg,vr,45.0,
                        GetArr(fm,"faVoltage")->GetArray(),GetArr(fm,"faDarkCurrent")->GetArray(),
                        GetArr(fm,"faTotalCurrent")->GetArray(),GetArr(fm,"faTotalCurrent")->GetSize());
                     //= SetVar(fm,"fIdOverGain45",idoverg);
                     //= SetVar(fm,"fVr45",vr);

      	             IdOverGainHam(fm->fSNo,idoverg,vr,50.0,
                        GetArr(fm,"faVoltage")->GetArray(),GetArr(fm,"faDarkCurrent")->GetArray(),
                        GetArr(fm,"faTotalCurrent")->GetArray(),GetArr(fm,"faTotalCurrent")->GetSize());
                     SetVar(fm,"fIdOverGain50",idoverg);
                        SetVar(fm,"fVr50",vr);

      	             IdOverGainHam(fm->fSNo,idoverg,vr,100.0,
                        GetArr(fm,"faVoltage")->GetArray(),GetArr(fm,"faDarkCurrent")->GetArray(),
                        GetArr(fm,"faTotalCurrent")->GetArray(),GetArr(fm,"faTotalCurrent")->GetSize());
                     SetVar(fm,"fIdOverGain100",idoverg);
                        SetVar(fm,"fVr100",vr);

		     IdOverGainHam(fm->fSNo,idoverg,vr,200.0,
				   GetArr(fm,"faVoltage")->GetArray(),GetArr(fm,"faDarkCurrent")->GetArray(),
				   GetArr(fm,"faTotalCurrent")->GetArray(),GetArr(fm,"faTotalCurrent")->GetSize());
		     SetVar(fm,"fIdOverGain200",idoverg);
		        SetVar(fm,"fVr200",vr);
                  }
      	          CalcQE(fm);
      	          CalcGain(fm);
		  //		  cout<<"end calculations"<<endl;		 
		  const char *ss =  fm->fSerialNo.Data();
      	          fm->SetIndex(ss);
        	  mHam->AddAt(fm,fm->GetIndex());            //dobavit' object ham v conteyner v poziciyu sootv. nomeru APD.
               }	   
	       else {
	          cout<<"Format Error in:"<<fm->fSerialNo<<endl;
                  if(fm!=NULL) delete fm;
	       } 
            }
         }
         if(t!=NULL)  delete t;
//         if(fm!=NULL) delete fm;
      }
      in.close();
   }
   if(dbfi) delete dbfi;
//   mHam->Write("mHam",TObject::kSingleKey);
#ifdef MAKEST
char s[20];
Int_t ln = 30 ;
Int_t wn = 1;

 for(Int_t i=5000;i<20000;i++) {
   if(i%500 == 0) {ln++;wn=1;} 
  
   if(i%100 == 0) wn++;
   
   sprintf(s,"%02d%02d%06d",ln,wn,i);
   Hamamatsu *h = new Hamamatsu();
   SetArr(h,"faVoltage",13);
   SetArr(h,"faDarkCurrent",13);
   SetArr(h,"faTotalCurrent",12);
   SetArr(h,"faNoise",12);
   SetArr(h,"faCapacitance",12);	       	        
   SetArr(h,"faGain",12);	       	        
   SetArr(h,"faIdarkOverGain",12);	       	        
   SetArr(h,"fadMdV",12);	       	        
   SetArr(h,"faNoiseRatio",12);	       	        
   h->SetSerialNo(s);
   h->SetIndex(s);
   mHam->AddAt(h,i);
 }
#endif

}

void BuildDB::FillGain(TObjArray *mapn,TObjArray *mapa, TObjArray *mapi) 
{
   Double_t gainfit,voltage, idark=0;
   Int_t n;
   if(fFiles == NULL) return;
   TObjArrayIter *dbfi = new TObjArrayIter(fFiles);  
   TObject *fno;
   ifstream in;
   TString file,line;

   while((fno = dbfi->Next()) != 0) {
      file = ((TObjString*)fno)->GetString();
      cout<<"FillGainData, file:"<<file<<endl;
      if(!file.EndsWith(".txt")) continue;      
      TObjArray *map = NULL;
      Token *tok = new Token(file,"/");                      //razbiv. na tok. otdel /
      while(tok->HasNext());
      TString *filename = new TString(tok->Get());
#ifdef RESTRICTEDVERSION 
      if(filename->BeginsWith("GQn")) map = mapn;          
      else if(filename->BeginsWith("GQa")) map = mapa;          
      else if(filename->BeginsWith("GQi")) map = mapi;          
#endif
      if(filename->BeginsWith("Gn")) map = mapn;          
      else if(filename->BeginsWith("Ga")) map = mapa;          
      else if(filename->BeginsWith("Gi")) map = mapi;                

      delete filename;
      delete tok;
      if(map==NULL) continue;      
      in.open(file, ios::in);
      Gain *fm = new Gain(); 

      Int_t nline=0;
      Bool_t noError=kTRUE;
      Int_t vd=0;
      while(in.good()) {
         line.ReadLine(in);
	 nline++;
      	 Token *t = new Token(line,";");
	 if(nline==3) { 
      	    if(t->HasNext()) SetVar(fm,"fVr",t->GetD());   
      	    if(t->HasNext()) SetVar(fm,"fIdark",t->GetD());   
      	    if(t->HasNext()) SetVar(fm,"fdMdV",t->GetD());   
      	    if(t->HasNext()) SetVar(fm,"fMeanTemp",t->GetD());   
      	    if(t->HasNext()) SetVar(fm,"fVb",t->GetD());   
      	    if(t->HasNext()) //=SetVar(fm,"fQEcoefficient",t->GetD());   
      	    if(t->HasNext()) SetVar(fm,"fAPDPosition",t->GetD()); 
            if(t->HasNext()) fm->fSerialNo = t->Get();
      	    //if(t->HasNext()) SetVar(fm,"fSerialNo",t->Get());   
      	 }
	 if(nline>4) {
      	    if(t->HasNext())  {
	       vd=nline-4;
	       Int_t vi=vd-1;
	       SetArr(fm,"faVoltage",vd);
	       SetArr(fm,"faItotal",vd);
	       SetArr(fm,"faIdark",vd);
	       SetArr(fm,"faIphoto",vd);
	       SetArr(fm,"faGain",vd);
	       SetArr(fm,"fadMdV",vd);
	       SetArr(fm,"faIdarkOverGain",vd);
	       SetArr(fm,"faGaindMdV",vd);
	       SetArr(fm,"faIpinlight",vd);
	       SetArr(fm,"faIpindark",vd);
	       SetArr(fm,"faGainfit",vd);
	       SetArr(fm,"faIdarkOverGainfit",vd);
	       fm->SetIndex(fm->fSerialNo);
	       n=fm->GetIndex();
	       voltage=t->GetD();
	       SetArr(fm,"faVoltage",vi,voltage);
	       if(t->HasNext()) SetArr(fm,"faItotal",vi,t->GetD());
	       if(t->HasNext())  idark=t->GetD();
	       SetArr(fm,"faIdark",vi,idark);
	       if(t->HasNext()) SetArr(fm,"faIphoto",vi,t->GetD());
	       if(t->HasNext()) SetArr(fm,"faGain",vi,t->GetD());
	       if(t->HasNext()) SetArr(fm,"fadMdV",vi,t->GetD());
	       if(t->HasNext()) SetArr(fm,"faIdarkOverGain",vi,t->GetD());
	       if(t->HasNext()) SetArr(fm,"faGaindMdV",vi,t->GetD());
	       if(t->HasNext()) SetArr(fm,"faIpinlight",vi,t->GetD());
	       if(t->HasNext()) SetArr(fm,"faIpindark",vi,t->GetD());
               gainfit=par0a[n]/(par1a[n]*(exp(par0a[n]*(Vba[n]-voltage))-1.));
	       if(gainfit<0 || gainfit>10000) gainfit=0.1;
	       SetArr(fm,"faGainfit",vi,gainfit);
	       SetArr(fm,"faIdarkOverGainfit",vi,idark/gainfit);
      	    }	    
	 }
      	 if(noError) noError = t->NoError();
         if(t)  delete t;
      }
      in.close();
      if(nline>1 && noError) {
         Float_t idoverg, vr;
         if(GetArr(fm,"faVoltage")!=NULL &&GetArr(fm,"faIdark")!=NULL &&GetArr(fm,"faIphoto")!=NULL
            &&GetArr(fm,"faGain")!=NULL ) {
      	 IdOverGain(idoverg,vr,10.0,
            GetArr(fm,"faVoltage")->GetArray() ,GetArr(fm,"faIdark")->GetArray(), 
            GetArr(fm,"faIphoto")->GetArray(),
		    GetArr(fm,"faVoltage")->GetSize(), GetArr(fm,"faGain")->GetArray());
         //SetVar(fm,"fIdOverGain10",idoverg); 
         //SetVar(fm,"fVr10",vr); 

      	 IdOverGain(idoverg,vr,20.0,
            GetArr(fm,"faVoltage")->GetArray() ,GetArr(fm,"faIdark")->GetArray(), 
            GetArr(fm,"faIphoto")->GetArray(),
		    GetArr(fm,"faVoltage")->GetSize(), GetArr(fm,"faGain")->GetArray());
         //SetVar(fm,"fIdOverGain20",idoverg); 
         //SetVar(fm,"fVr20",vr); 

      	 IdOverGain(idoverg,vr,50.0,
            GetArr(fm,"faVoltage")->GetArray() ,GetArr(fm,"faIdark")->GetArray(), 
            GetArr(fm,"faIphoto")->GetArray(),
		    GetArr(fm,"faVoltage")->GetSize(), GetArr(fm,"faGain")->GetArray());
         SetVar(fm,"fIdOverGain50",idoverg); 
         SetVar(fm,"fVr50",vr); 

      	 IdOverGain(idoverg,vr,100.0,
            GetArr(fm,"faVoltage")->GetArray() ,GetArr(fm,"faIdark")->GetArray(), 
            GetArr(fm,"faIphoto")->GetArray(),
		    GetArr(fm,"faVoltage")->GetSize(), GetArr(fm,"faGain")->GetArray());
         SetVar(fm,"fIdOverGain100",idoverg); 
         SetVar(fm,"fVr100",vr);

      	 IdOverGain(idoverg,vr,200.0,
            GetArr(fm,"faVoltage")->GetArray() ,GetArr(fm,"faIdark")->GetArray(), 
            GetArr(fm,"faIphoto")->GetArray(),
		    GetArr(fm,"faVoltage")->GetSize(), GetArr(fm,"faGain")->GetArray());
         SetVar(fm,"fIdOverGain200",idoverg); 
	 SetVar(fm,"fVr200",vr);
         }
          
//         TObjString *key = new TObjString(fm->fSerialNo);
//cout<<":"<<fm->fSerialNo<<":"<<endl;
      	 fm->SetIndex(fm->fSerialNo);
      	 map->AddAt(fm,fm->GetIndex());
      }	                      
      else {
	 cout<<"Format Error in:"<<fm->fSerialNo<<endl;
         if(fm!=NULL) delete fm;
      } 
      
   }
   if(dbfi) delete dbfi;

#ifdef MAKEST
char s[20];
Int_t ln = 30 ;
Int_t wn = 1;
Int_t vd=50;
for(Int_t i=1000;i<20000;i++) {
   if(i%500 == 0) {ln++;wn=1;} 
   if(i%100 == 0) wn++;   
   sprintf(s,"%02d%02d%06d",ln,wn,i);
   Gain *fm = new Gain();
   SetArr(fm,"faVoltage",vd);
   SetArr(fm,"faItotal",vd);
   SetArr(fm,"faIdark",vd);
   SetArr(fm,"faIphoto",vd);
   SetArr(fm,"faGain",vd);
   SetArr(fm,"fadMdV",vd);
   SetArr(fm,"faIdarkOverGain",vd);
   SetArr(fm,"faGaindMdV",vd);
   SetArr(fm,"faIpinlight",vd);
   SetArr(fm,"faIpindark",vd);

   fm->SetSerialNo(s);
   fm->SetIndex(s);
   mapn->AddAt(fm,i);
}

ln = 30 ;
wn = 1;
vd=50;
for(Int_t i=1000;i<20000;i++) {
   if(i%500 == 0) {ln++;wn=1;} 
   if(i%100 == 0) wn++;   
   sprintf(s,"%02d%02d%06d",ln,wn,i);
   Gain *fm = new Gain();
   SetArr(fm,"faVoltage",vd);
   SetArr(fm,"faItotal",vd);
   SetArr(fm,"faIdark",vd);
   SetArr(fm,"faIphoto",vd);
   SetArr(fm,"faGain",vd);
   SetArr(fm,"fadMdV",vd);
   SetArr(fm,"faIdarkOverGain",vd);
   SetArr(fm,"faGaindMdV",vd);
   SetArr(fm,"faIpinlight",vd);
   SetArr(fm,"faIpindark",vd);

   fm->SetSerialNo(s);
   fm->SetIndex(s);
   mapi->AddAt(fm,i);
}

ln = 30 ;
wn = 1;
vd=50;
for(Int_t i=1000;i<20000;i++) {
   if(i%500 == 0) {ln++;wn=1;} 
   if(i%100 == 0) wn++;   
   sprintf(s,"%02d%02d%06d",ln,wn,i);
   Gain *fm = new Gain();
   SetArr(fm,"faVoltage",vd);
   SetArr(fm,"faItotal",vd);
   SetArr(fm,"faIdark",vd);
   SetArr(fm,"faIphoto",vd);
   SetArr(fm,"faGain",vd);
   SetArr(fm,"fadMdV",vd);
   SetArr(fm,"faIdarkOverGain",vd);
   SetArr(fm,"faGaindMdV",vd);
   SetArr(fm,"faIpinlight",vd);
   SetArr(fm,"faIpindark",vd);

   fm->SetSerialNo(s);
   fm->SetIndex(s);
   mapa->AddAt(fm,i);
}

#endif


}

void BuildDB::FillQE(TObjArray *mapn,TObjArray *mapa, TObjArray *mapi) 
{
   if(fFiles == NULL) return;
   TObjArrayIter *dbfi = new TObjArrayIter(fFiles);  
   TObject *fno;
   ifstream in;
   TString file,line;
   while((fno = dbfi->Next()) != 0) {
      file = ((TObjString*)fno)->GetString();
      cout<<"FillQEData, file:"<<file<<endl;
      if(!file.EndsWith(".txt")) continue;
      TObjArray *map = NULL;
      Token *tok = new Token(file,"/");
      while(tok->HasNext());
      TString *filename = new TString(tok->Get());
 //     cout<<(*filename)<<endl;
      if(filename->BeginsWith("QEn")) map = mapn;          
      else if(filename->BeginsWith("QEa")) map = mapa;          
      else if(filename->BeginsWith("QEi")) map = mapi;          
      delete filename;
      delete tok;
      if(map==NULL) continue;

      in.open(file, ios::in);
      QE *fm = new QE(); 
      Int_t nline=0;
      Bool_t noError=kTRUE;
      while(in.good()) {
         line.ReadLine(in);
	 nline++;
      	 Token *t = new Token(line,";");
	 if(nline==1) {
	    t->HasNext();
	    t->HasNext();
	    t->HasNext();
      	    if(t->HasNext()) fm->fSerialNo = t->Get();   
	 } 
	 if(nline==2) { 
	    t->HasNext();
	    //= if(t->HasNext()) SetVar(fm,"fBiasAPD",t->GetD());   
	    t->HasNext();
      	    //= if(t->HasNext()) SetVar(fm,"fAPDdarkcurrent",t->GetD());   
	    t->HasNext();
      	    if(t->HasNext()) SetVar(fm,"fAPDQE",t->GetD());   
      	 }	 
	 if(nline==3) { 
	    t->HasNext();
      	    if(t->HasNext()) {} //= SetVar(fm,"fBiasPIN",t->GetD());   
	    t->HasNext();
      	    if(t->HasNext()) {} //= SetVar(fm,"fPINdarkcurrent",t->GetD());   
      	 }
	 if(nline>5) {
      	    if(t->HasNext())  {
	       Int_t vd=nline-5;
	       Int_t vi=vd-1;
	       SetArr(fm,"faWavelength",vd);
	       SetArr(fm,"faAPDillCurr",vd);
	       SetArr(fm,"faPINillCurr",vd);
      	   SetArr(fm,"faQEPIN",vd);
	       SetArr(fm,"faQEAPD",vd);
	       SetArr(fm,"faWavelength",vi,t->GetD());
	       if(t->HasNext()) SetArr(fm,"faAPDillCurr",vi,t->GetD());
	       if(t->HasNext()) SetArr(fm,"faPINillCurr",vi,t->GetD());
	       if(t->HasNext()) SetArr(fm,"faQEPIN",vi,t->GetD());
	       if(t->HasNext()) SetArr(fm,"faQEAPD",vi,t->GetD());
      	    }	    
	 }
      	 if(noError) noError=t->NoError();
         if(t)  delete t;
      }
      in.close();
      if(nline>1 && noError) {
//         TObjString *key = new TObjString(fm->fSerialNo);

      	 fm->SetIndex(fm->fSerialNo);
      	 map->AddAt(fm,fm->GetIndex());
      	 
      }	                      
      else {
	 cout<<"Format Error in:"<<fm->fSerialNo<<endl;
         if(fm!=NULL) delete fm;
      } 
   }
   if(dbfi) delete dbfi;

}

void BuildDB::FillNoise(TObjArray *mapn,TObjArray *mapa, TObjArray *mapi) 
{
   if(fFiles == NULL) return;
   TObjArrayIter *dbfi = new TObjArrayIter(fFiles);  
   TObject *fno;
   ifstream in;
   TString file,line;
   while((fno = dbfi->Next()) != 0) {
      file = ((TObjString*)fno)->GetString();
      cout<<"FillNoiseData, file:"<<file<<endl;
      if(!file.EndsWith(".txt")) continue;
      TObjArray *map = NULL;
      Token *tok = new Token(file,"/");
      while(tok->HasNext());
      TString *filename = new TString(tok->Get());
 //     cout<<(*filename)<<endl;
      if(filename->BeginsWith("FTn")) map = mapn;          
      else if(filename->BeginsWith("FTa")) map = mapa;          
      else if(filename->BeginsWith("FTi")) map = mapi;          
      delete filename;
      delete tok;
      if(map==NULL) continue;

      in.open(file, ios::in);
      Noise *fm = new Noise(); 
      Int_t nline=0;
      Bool_t noError=kTRUE;
      while(in.good()) {
         line.ReadLine(in);
	 nline++;
      	 Token *t = new Token(line,";");
	 if(nline==1) {
	    t->HasNext();t->HasNext();t->HasNext();
      	    if(t->HasNext()) fm->fSerialNo = t->Get();   
	 } 
	 if(nline==3) { 
	    t->HasNext();
      	    if(t->HasNext()) SetVar(fm,"fIdOverGain50",t->GetD());   
	    t->HasNext();
      	    if(t->HasNext()) SetVar(fm,"fExNoise50",t->GetD());   
	    t->HasNext();
      	    if(t->HasNext()) SetVar(fm,"fENCOverGain50",t->GetD());   
      	 }	 
	 if(nline==4) { 
	    t->HasNext();
      	    if(t->HasNext()) SetVar(fm,"fIdOverGain100",t->GetD());   
	    t->HasNext();
      	    if(t->HasNext()) SetVar(fm,"fExNoise100",t->GetD());   
	    t->HasNext();
      	    if(t->HasNext()) SetVar(fm,"fENCOverGain100",t->GetD());   
      	 }	 
	 if(nline>6) {
      	    if(t->HasNext())  {
	       Int_t vd=nline-6;
	       Int_t vi=vd-1;
	       
	       SetArr(fm,"faVoltage_ill",vd);
	       SetArr(fm,"faMeasVoltage_ill",vd);
	       SetArr(fm,"faTotal_current",vd);
	       SetArr(fm,"faMean_ampl_total",vd);
	       SetArr(fm,"faSigma_fit_total",vd);
	       SetArr(fm,"faRms_calc_total",vd);
	       SetArr(fm,"faVoltage_dark",vd);
	       SetArr(fm,"faMeasVoltage_dark",vd);
	       SetArr(fm,"faDark_current",vd);
	       SetArr(fm,"faMean_ampl_dark",vd);
	       SetArr(fm,"faSigma_fit_dark",vd);
	       SetArr(fm,"faRms_calc_dark",vd);
	       SetArr(fm,"faIph",vd);
	       SetArr(fm,"faGain",vd);
	       SetArr(fm,"faF",vd);
	       SetArr(fm,"faFrms",vd);
	       SetArr(fm,"faIdOverGain",vd);
	       SetArr(fm,"faENC",vd);
	       SetArr(fm,"faENCOverGain",vd);
	       SetArr(fm,"faENCrms",vd);
	       SetArr(fm,"faENCrmsOverGain",vd);
	       
	       SetArr(fm,"faVoltage_ill",vi,t->GetD());
	       if(t->HasNext()) SetArr(fm,"faMeasVoltage_ill",vi,t->GetD());
	       if(t->HasNext()) SetArr(fm,"faTotal_current",vi,t->GetD());
	       if(t->HasNext()) SetArr(fm,"faMean_ampl_total",vi,t->GetD());
	       if(t->HasNext()) SetArr(fm,"faSigma_fit_total",vi,t->GetD());
	       if(t->HasNext()) SetArr(fm,"faRms_calc_total",vi,t->GetD());
	       if(t->HasNext()) SetArr(fm,"faVoltage_dark",vi,t->GetD());
	       if(t->HasNext()) SetArr(fm,"faMeasVoltage_dark",vi,t->GetD());
	       if(t->HasNext()) SetArr(fm,"faDark_current",vi,t->GetD());
	       if(t->HasNext()) SetArr(fm,"faMean_ampl_dark",vi,t->GetD());
	       if(t->HasNext()) SetArr(fm,"faSigma_fit_dark",vi,t->GetD());
	       if(t->HasNext()) SetArr(fm,"faRms_calc_dark",vi,t->GetD());
	       if(t->HasNext()) SetArr(fm,"faIph",vi,t->GetD());
	       if(t->HasNext()) SetArr(fm,"faGain",vi,t->GetD());
	       if(t->HasNext()) SetArr(fm,"faF",vi,t->GetD());
	       if(t->HasNext()) SetArr(fm,"faFrms",vi,t->GetD());
	       if(t->HasNext()) SetArr(fm,"faIdOverGain",vi,t->GetD());
	       if(t->HasNext()) SetArr(fm,"faENC",vi,t->GetD());
	       if(t->HasNext()) SetArr(fm,"faENCOverGain",vi,t->GetD());
	       if(t->HasNext()) SetArr(fm,"faENCrms",vi,t->GetD());
	       if(t->HasNext()) SetArr(fm,"faENCrmsOverGain",vi,t->GetD());

      	    }	    
	 }
      	 if(noError) noError=t->NoError();
         if(t)  delete t;
      }
      in.close();
      if(nline>1 && noError) {
//         TObjString *key = new TObjString(fm->fSerialNo);
      	 fm->SetIndex(fm->fSerialNo);
      	 map->AddAt(fm,fm->GetIndex());
//      	 map->AddAt(fm,sn2i(fm->fSerialNo));

      }	                      
      else {
	 cout<<"Format Error in:"<<fm->fSerialNo<<endl;
         if(fm!=NULL) delete fm;
      } 
   }
   if(dbfi) delete dbfi;

}

//
void BuildDB::FillGainQE(TObjArray *mapn,TObjArray *mapa, TObjArray *mapi) 
{
   Double_t gainfit,voltage,idark=0;
   Int_t n;
   if(fFiles == NULL) return;
   TObjArrayIter *dbfi = new TObjArrayIter(fFiles);  
   TObject *fno;
   ifstream in;
   TString file,line;
   while((fno = dbfi->Next()) != 0) {
      file = ((TObjString*)fno)->GetString();
      cout<<"FillGainQEData, file:"<<file<<endl;
      if(!file.EndsWith(".txt")) continue;

      TObjArray *map = NULL;
      Token *tok = new Token(file,"/");
      while(tok->HasNext());
      TString *filename = new TString(tok->Get());
 //     cout<<(*filename)<<endl;
      if(filename->BeginsWith("GQn")) map = mapn;          
      else if(filename->BeginsWith("GQa")) map = mapa;          
      else if(filename->BeginsWith("GQi")) map = mapi;          
      delete filename;
      delete tok;

      if(map==NULL) continue;
      in.open(file, ios::in);
      GainQE *fm = new GainQE(); 

      Int_t nline=0;
      Bool_t noError=kTRUE;
      while(in.good()) {
         line.ReadLine(in);
	 nline++;
      	 Token *t = new Token(line,";");
	 if(nline==3) { 
      	    if(t->HasNext()) SetVar(fm,"fVr",t->GetD());   
      	    if(t->HasNext()) SetVar(fm,"fIdark",t->GetD());   
      	    if(t->HasNext()) SetVar(fm,"fdMdV",t->GetD());   
      	    if(t->HasNext()) SetVar(fm,"fMeanTemp",t->GetD());   
      	    if(t->HasNext()) SetVar(fm,"fVb",t->GetD());   
      	    if(t->HasNext()) //SetVar(fm,"fQEcoefficient",t->GetD());   
      	    if(t->HasNext()) SetVar(fm,"fAPDPosition",t->GetD());   
            if(t->HasNext()) fm->fSerialNo =  t->Get();
      	    //if(t->HasNext()) SetVar(fm,"fSerialNo",t->Get());   
	    
      	 }	 
	 if(nline>4) {
      	    if(t->HasNext())  {
	       Int_t vd=nline-4;
	       Int_t vi=vd-1;
	       SetArr(fm,"faVoltage",vd);
	       SetArr(fm,"faItotal",vd);
	       SetArr(fm,"faIdark",vd);
	       SetArr(fm,"faIphoto",vd);
	       SetArr(fm,"faGain",vd);
	       SetArr(fm,"fadMdV",vd);
	       SetArr(fm,"faIdarkOverGain",vd);
	       SetArr(fm,"faGaindMdV",vd);
	       SetArr(fm,"faIpinlight",vd);
	       SetArr(fm,"faIpindark",vd);
	       SetArr(fm,"faGainfit",vd);
	       SetArr(fm,"faIdarkOverGainfit",vd);

	       fm->SetIndex(fm->fSerialNo);
	       n=fm->GetIndex();
	       
	       voltage=t->GetD();
	       SetArr(fm,"faVoltage",vi,voltage);
	       if(t->HasNext()) SetArr(fm,"faItotal",vi,t->GetD());
	       if(t->HasNext())  idark=t->GetD();
	       SetArr(fm,"faIdark",vi,idark);
	       if(t->HasNext()) SetArr(fm,"faIphoto",vi,t->GetD());
	       if(t->HasNext()) SetArr(fm,"faGain",vi,t->GetD());
	       if(t->HasNext()) SetArr(fm,"fadMdV",vi,t->GetD());
	       if(t->HasNext()) SetArr(fm,"faIdarkOverGain",vi,t->GetD());
	       if(t->HasNext()) SetArr(fm,"faGaindMdV",vi,t->GetD());
	       if(t->HasNext()) SetArr(fm,"faIpinlight",vi,t->GetD());
	       if(t->HasNext()) SetArr(fm,"faIpindark",vi,t->GetD());
               gainfit=par0a[n]/(par1a[n]*(exp(par0a[n]*(Vba[n]-voltage))-1.));
	       if(gainfit<0 || gainfit >10000) gainfit=0.1;
	       SetArr(fm,"faGainfit",vi,gainfit);
	       SetArr(fm,"faIdarkOverGainfit",vi,idark/gainfit);

      	    }	    
	 }
	 if(noError) noError=t->NoError();
         if(t)  delete t;
      }
      in.close();
      if(nline>1 && noError) {
//         TObjString *key = new TObjString(fm->fSerialNo);
      	 fm->SetIndex(fm->fSerialNo);
      	 map->AddAt(fm,fm->GetIndex());
//      	 map->AddAt(fm,sn2i(fm->fSerialNo));
      }	                      
      else {
	 cout<<"Format Error in:"<<fm->fSerialNo<<endl;
      	 cout<<"|"<<line<<"|"<<endl;
         if(fm!=NULL) delete fm;
      } 
   }
   if(dbfi) delete dbfi;

}
//

void BuildDB::FillCapacitance(TObjArray *mapn,TObjArray *mapa, TObjArray *mapi) 
{
   if(fFiles == NULL) return;
   TObjArrayIter *dbfi = new TObjArrayIter(fFiles);  
   TObject *fno;
   ifstream in;
   TString file,line;
   while((fno = dbfi->Next()) != 0) {
      file = ((TObjString*)fno)->GetString();
      cout<<"FillCapacitanceData, file:"<<file<<endl;
      if(!file.EndsWith(".txt")) continue;

      TObjArray *map = NULL;
      Token *tok = new Token(file,"/");
      while(tok->HasNext());
      TString *filename = new TString(tok->Get());
 //     cout<<(*filename)<<endl;
      if(filename->BeginsWith("CRn")) map = mapn;          
      else if(filename->BeginsWith("CRa")) map = mapa;          
      else if(filename->BeginsWith("CRi")) map = mapi;          
      delete filename;
      delete tok;

      if(map==NULL) continue;
      in.open(file, ios::in);
      Capacitance *fm = new Capacitance(); 
      Int_t nline=0;
      Bool_t noError=kTRUE;
      while(in.good()) {
         line.ReadLine(in);
         nline++;
      	 Token *t = new Token(line,";");
	 if(nline==1) { 
	   if(t->HasNext()) 
	     if(t->HasNext()) fm->fSerialNo =  t->Get();
//      	    if(t->HasNext()) SetVar(fm,"fSerialNo", t->Get());   
      	 }	 
	 if(nline>3) {
      	    if(t->HasNext())  {
	       Int_t vd=nline-3;
	       Int_t vi=vd-1;
	       SetArr(fm,"faVoltage",vd);
	       SetArr(fm,"faCsMeasured",vd);
	       SetArr(fm,"faCsCorrected",vd);
	       SetArr(fm,"faRs",vd);
	       SetArr(fm,"fa1OverCs",vd);
	       SetArr(fm,"fa1OverCs2",vd);
	       SetArr(fm,"faDopProf",vd);
	       SetArr(fm,"faVoltage",vi, t->GetD());
	       if(t->HasNext()) SetArr(fm,"faCsMeasured",vi,t->GetD());
	       if(t->HasNext()) SetArr(fm,"faCsCorrected",vi,t->GetD());
	       if(t->HasNext()) SetArr(fm,"faRs",vi,t->GetD());
	       if(t->HasNext()) SetArr(fm,"fa1OverCs",vi,t->GetD());
	       if(t->HasNext()) SetArr(fm,"fa1OverCs2",vi,t->GetD());
	       if(t->HasNext()) SetArr(fm,"faDopProf",vi,t->GetD());
      	    }	    
	 }
	 if(noError) noError=t->NoError();
         if(t)  delete t;
      }
      in.close();
      if(nline>1 && noError) {
//         TObjString *key = new TObjString(fm->fSerialNo);
      	 fm->SetIndex(fm->fSerialNo);
      	 map->AddAt(fm,fm->GetIndex());
//      	 map->AddAt(fm,sn2i(fm->fSerialNo));
      }	                      
      else {
	 cout<<"Format Error in:"<<fm->fSerialNo<<endl;
         if(fm!=NULL) delete fm;
      } 
   }
   if(dbfi) delete dbfi;

}
//
void BuildDB::FillAnnealing(TObjArray *mapi) 
{
  if(fFiles == NULL) return;
  
  TObjArrayIter *dbfi = new TObjArrayIter(fFiles);  
  TObject *fno;

  ifstream in;

  TString file,line;
  while((fno = dbfi->Next()) != 0) {

    file = ((TObjString*)fno)->GetString();
    cout<<"FillAnnealingData, file:"<<file<<endl;
    if(!file.EndsWith(".txt")) continue;
    
    TObjArray *map = NULL;
    Token *tok = new Token(file,"/");
    while(tok->HasNext());
    TString *filename = new TString(tok->Get());
    ////     cout<<(*filename)<<endl;
    //if(filename->BeginsWith("CRn")) map = mapn;        (tol'ko odin tip
    //else if(filename->BeginsWith("CRa")) map = mapa;          izmereniy)
    //else if(filename->BeginsWith("CRi")) map = mapi;          
    map=mapi;                                               //!!
    delete filename;
    delete tok;
    
    if(map==NULL) continue;
    in.open(file, ios::in);
    Annealing *fm = new Annealing(); 
    Int_t nline=0;               
    Bool_t noError=kTRUE;
    while(in.good()) {
      line.ReadLine(in);
      nline++;
      Token *t = new Token(line,";");

      if(nline==2) {                   //format
	if(t->HasNext())
	  if(t->HasNext()) fm->fSerialNo =  t->Get();

//      	    if(t->HasNext()) SetVar(fm,"fSerialNo", t->Get());   
      }	 
      if(nline>3) {
	if(t->HasNext())  {
	  Int_t vd=nline-3;
	  Int_t vi=vd-1;
	  SetArr(fm,"faCurrent",vd);
	  SetArr(fm,"faDays",vd);
	  SetArr(fm,"faHour",vd);
	  SetArr(fm,"faMinute",vd);
	  SetArr(fm,"faDay",vd);
	  SetArr(fm,"faMonth",vd);
	  SetArr(fm,"faYear",vd);
	  SetArr(fm,"faHV",vd);
	  SetArr(fm,"faTemperature",vd);
	  SetArr(fm,"faCurrent",vi,t->GetD());
	    if(t->HasNext()) {
	  Float_t s2d,tget;
	  tget=t->GetD();
	  s2d=tget/86400.-35526;
	      SetArr(fm,"faDays",vi,s2d);
	    }
	  if(t->HasNext()) SetArr(fm,"faHour",vi,t->GetD());
	  if(t->HasNext()) SetArr(fm,"faMinute",vi,t->GetD());
	  if(t->HasNext()) SetArr(fm,"faDay",vi,t->GetD());
 	  if(t->HasNext()) SetArr(fm,"faMonth",vi,t->GetD());
	  if(t->HasNext()) SetArr(fm,"faYear",vi,t->GetD());
	  if(t->HasNext()) SetArr(fm,"faHV",vi,t->GetD());
	  if(t->HasNext()) SetArr(fm,"faTemperature",vi,t->GetD());

	}	    
      }
      if(noError) noError=t->NoError();
      if(t)  delete t;
    }
    in.close();
    if(nline>1 && noError) {
      //         TObjString *key = new TObjString(fm->fSerialNo);
      fm->SetIndex(fm->fSerialNo);
      map->AddAt(fm,fm->GetIndex());
      //      	 map->AddAt(fm,sn2i(fm->fSerialNo));
    }	                      
    else {
      cout<<"Format Error in:"<<fm->fSerialNo<<endl;
      if(fm!=NULL) delete fm;
    } 
  }
  if(dbfi) delete dbfi; 
}


void BuildDB::FillBRKDPSI(TObjArray *mapn,TObjArray *mapa,TObjArray *mapi) 
{
  Double_t bias, gainfit, VBBC=0, idark=0;
  Int_t n=0;
  if(fFiles == NULL) return;
  
  TObjArrayIter *dbfi = new TObjArrayIter(fFiles);  
  TObject *fno;

  ifstream in;

  TString file,line;
  while((fno = dbfi->Next()) != 0) {

    file = ((TObjString*)fno)->GetString();
    cout<<"FillBRKDPSIData, file:"<<file<<endl;
    if(!file.EndsWith(".txt")) continue;
    
    TObjArray *map = NULL;
    Token *tok = new Token(file,"/");
    while(tok->HasNext());
    TString *filename = new TString(tok->Get());
    ////     cout<<(*filename)<<endl;
    if(filename->BeginsWith("BPn")) map = mapn;     
    else if(filename->BeginsWith("BPa")) map = mapa; 
    else if(filename->BeginsWith("BPi")) map = mapi;          
    //    map=mapi;                                               //!!
    delete filename;
    delete tok;
    
    if(map==NULL) continue;
    in.open(file, ios::in);
    BRKDPSI *fm = new BRKDPSI(); 
    Int_t nline=0;               
    Bool_t noError=kTRUE;
    Int_t fit=0;
    while(in.good()) {
      line.ReadLine(in);
      nline++;
      Token *t = new Token(line,";");

      if(nline==3) {                   //format
	if(t->HasNext())
	  if(t->HasNext()) SetVar(fm,"fID",t->GetD());
	    if(t->HasNext())
	      if(t->HasNext()) SetVar(fm,"fTemperature",t->GetD());
		if(t->HasNext())  VBBC=t->GetD();
		  SetVar(fm,"fVB",VBBC);
		  if(t->HasNext())
		    if(t->HasNext())     
		      if(t->HasNext()) fm->fSerialNo = t->Get();
	          fm->SetIndex(fm->fSerialNo);
		  n=fm->GetIndex();
		  if(fm->fTemperature!=0) VBBC=VBBC-0.76*(fm->fTemperature-Temph[n]);
		  SetVar(fm,"fVBcorr",VBBC);
  
//      	    if(t->HasNext()) SetVar(fm,"fSerialNo", t->Get());   
      }	
      //    fm->SetIndex(fm->fSerialNo);
      //n=fm->GetIndex();
      if(nline>4) {
	if(t->HasNext())  {
	  Int_t vd=nline-4; //!!
	  Int_t vi=vd-1;
	  SetArr(fm,"faBias",vd);
	  SetArr(fm,"faItotal",vd);
	  SetArr(fm,"faIdark",vd);
	  SetArr(fm,"faIphoto",vd);
	  //SetArr(fm,"faGainfit",vd);
	  SetArr(fm,"faGaindMdV",vd);
	  //SetArr(fm,"faIdarkOverGainfit",vd);
	  SetArr(fm,"fadMdV",vd);
	  bias=t->GetD();
	  SetArr(fm,"faBias",vi,bias);
	  //SetArr(fm,"faBias",vi,t->GetD());
	  if(t->HasNext()) SetArr(fm,"faItotal",vi,t->GetD());
	  if(t->HasNext()) idark=t->GetD();
	    SetArr(fm,"faIdark",vi,idark);
	  if(t->HasNext()) SetArr(fm,"faIphoto",vi,t->GetD());
	  if(fm->fTemperature==0) gainfit=par0a[n]/(par1a[n]*(exp(par0a[n]*(Vba[n]-bias))-1.));
	  else gainfit=par0a[n]/(par1a[n]*(exp(par0a[n]*(Vba[n]+0.76*(fm->fTemperature-Temph[n])-bias))-1.));
	  //if(gainfit<0 || gainfit>1000) gainfit=0.1;
 	  
	  if(t->HasNext()) { 
	    if(gainfit>0 && gainfit<1000) { SetArr(fm,"faGainfit",vd-fit); SetArr(fm,"faGainfit",vi-fit,gainfit);} 
	    else {fit++;}
	  }
	  
	  if(t->HasNext()) SetArr(fm,"faGaindMdV",vi,t->GetD());

	  if(t->HasNext()) { 
	    if(gainfit>0 && gainfit<1000) {SetArr(fm,"faIdarkOverGainfit",vd-fit);SetArr(fm,"faIdarkOverGainfit",vi-fit,idark/gainfit);} 
	  }
	  if(t->HasNext()) SetArr(fm,"fadMdV",vi,t->GetD());
	}	    
      }
      if(noError) noError=t->NoError();
      if(t)  delete t;
    }
    in.close();
    QuentinparPSI(fm);
    IDforshumPSI(fm);

    if(nline>1 && noError) {
      //         TObjString *key = new TObjString(fm->fSerialNo);
      //fm->SetIndex(fm->fSerialNo);
      map->AddAt(fm,fm->GetIndex());
      //      	 map->AddAt(fm,sn2i(fm->fSerialNo));
    }	                      
    else {
      cout<<"Format Error in:"<<fm->fSerialNo<<endl;
      if(fm!=NULL) delete fm;
    } 
  }
  if(dbfi) delete dbfi; 
}

void BuildDB::FillBRKDCERN(TObjArray *mapn,TObjArray *mapa,TObjArray *mapi) 
{
  Double_t bias, gainfit, VBBC=0, idark=0;
  Int_t n=0;
  if(fFiles == NULL) return;
  
  TObjArrayIter *dbfi = new TObjArrayIter(fFiles);  
  TObject *fno;

  ifstream in;

  TString file,line;
  while((fno = dbfi->Next()) != 0) {

    file = ((TObjString*)fno)->GetString();
    cout<<"FillBRKDCERNData, file:"<<file<<endl;
    if(!file.EndsWith(".txt")) continue;
    
    TObjArray *map = NULL;
    Token *tok = new Token(file,"/");
    while(tok->HasNext());
    TString *filename = new TString(tok->Get());
    ////     cout<<(*filename)<<endl;
    if(filename->BeginsWith("BCn")) map = mapn;     
    else if(filename->BeginsWith("BCa")) map = mapa; 
    else if(filename->BeginsWith("BCi")) map = mapi;          
    //    map=mapi;                                               //!!
    delete filename;
    delete tok;
    
    if(map==NULL) continue;
    in.open(file, ios::in);
    BRKDCERN *fm = new BRKDCERN(); 
    Int_t nline=0;               
    Bool_t noError=kTRUE;
    Int_t fit=0;
    while(in.good()) {
      line.ReadLine(in);
      nline++;
      Token *t = new Token(line,";");

      if(nline==3) {                   //format
	if(t->HasNext())
	  if(t->HasNext()) SetVar(fm,"fID",t->GetD());
	    if(t->HasNext())
	      if(t->HasNext()) SetVar(fm,"fTemperature",t->GetD());
	        if(t->HasNext()) VBBC=t->GetD();
		SetVar(fm,"fVB",VBBC);
		if(t->HasNext())
		    if(t->HasNext())     
		      if(t->HasNext()) fm->fSerialNo = t->Get();
		        fm->SetIndex(fm->fSerialNo);
			n=fm->GetIndex();
			VBBC=VBBC-0.76*(fm->fTemperature-Temph[n]);
			SetVar(fm,"fVBcorr",VBBC);	

//      	    if(t->HasNext()) SetVar(fm,"fSerialNo", t->Get());   
      }	
      //    fm->SetIndex(fm->fSerialNo);
      // n=fm->GetIndex();
      if(nline>4) {
	if(t->HasNext())  {
	  Int_t vd=nline-4; //!!
	  Int_t vi=vd-1;
	  SetArr(fm,"faBias",vd);
	  SetArr(fm,"faItotal",vd);
	  SetArr(fm,"faIdark",vd);
	  SetArr(fm,"faIphoto",vd);
	  //SetArr(fm,"faGainfit",vd-fit);
	  SetArr(fm,"faGaindMdV",vd);
	  SetArr(fm,"faIdarkOverGainfit",vd-fit);
	  SetArr(fm,"fadMdV",vd);
	  bias=t->GetD();
	  SetArr(fm,"faBias",vi,bias);
	  //SetArr(fm,"faBias",vi,t->GetD());
	  if(t->HasNext()) SetArr(fm,"faItotal",vi,t->GetD());
	  if(t->HasNext()) idark=t->GetD();
	    SetArr(fm,"faIdark",vi,idark);
	  if(t->HasNext()) SetArr(fm,"faIphoto",vi,t->GetD());
	  if(fm->fTemperature==0) gainfit=par0a[n]/(par1a[n]*(exp(par0a[n]*(Vba[n]-bias))-1.));
	  else gainfit=par0a[n]/(par1a[n]*(exp(par0a[n]*(Vba[n]+0.76*(fm->fTemperature-Temph[n])-bias))-1.));	  
	  //gainfit=par0a[n]/(par1a[n]*(exp(par0a[n]*(Vba[n]-bias))-1.));
	  //gainfit=par0a[n]/(par1a[n]*(exp(par0a[n]*(Vba[n]+0.76*(fm->fTemperature-Temph[n])-bias))-1.));
	  //if(gainfit<0 || gainfit>1000) gainfit=0.1;
	  if(t->HasNext()) { 
	    if(gainfit>0 && gainfit<1000) { SetArr(fm,"faGainfit",vd-fit); SetArr(fm,"faGainfit",vi-fit,gainfit);} 
	    else {fit++;}
	  }
	  //if(t->HasNext()) { if(gainfit>0 && gainfit<1000) SetArr(fm,"faGainfit",vi,gainfit); }
	  //if(t->HasNext()) SetArr(fm,"faGainfit",vi,gainfit);
	  if(t->HasNext()) SetArr(fm,"faGaindMdV",vi,t->GetD());
	  if(t->HasNext()) { 
	    if(gainfit>0 && gainfit<1000) {SetArr(fm,"faIdarkOverGainfit",vd-fit);SetArr(fm,"faIdarkOverGainfit",vi-fit,idark/gainfit);} 
	  }	  
	  //if(t->HasNext()) { if(gainfit>0 && gainfit<1000) SetArr(fm,"faIdarkOverGainfit",vi,idark/gainfit); }
	  //if(t->HasNext()) SetArr(fm,"faIdarkOverGainfit",vi,idark/gainfit);
	  if(t->HasNext()) SetArr(fm,"fadMdV",vi,t->GetD());

	}
      }
      if(noError) noError=t->NoError();
      if(t)  delete t;
    }
    in.close();
    QuentinparCERN(fm);
    IDforshumCERN(fm);

    if(nline>1 && noError) {
      //         TObjString *key = new TObjString(fm->fSerialNo);
      fm->SetIndex(fm->fSerialNo); //*******bilo zakoment.
      map->AddAt(fm,fm->GetIndex());
      //      	 map->AddAt(fm,sn2i(fm->fSerialNo));
    }	                      
    else {
      cout<<"Format Error in:"<<fm->fSerialNo<<endl;
      if(fm!=NULL) delete fm;
    } 
  
  }
  if(dbfi) delete dbfi; 
}
/*
void BuildDB::FillBRKDCERN(TObjArray *mapn,TObjArray *mapa,TObjArray *mapi) 
{
  if(fFiles == NULL) return;
  
  TObjArrayIter *dbfi = new TObjArrayIter(fFiles);  
  TObject *fno;

  ifstream in;

  TString file,line;
  while((fno = dbfi->Next()) != 0) {

    file = ((TObjString*)fno)->GetString();
    cout<<"FillBRKDCERNData, file:"<<file<<endl;
    if(!file.EndsWith(".txt")) continue;
    
    TObjArray *map = NULL;
    Token *tok = new Token(file,"/");
    while(tok->HasNext());
    TString *filename = new TString(tok->Get());
    ////     cout<<(*filename)<<endl;
    if(filename->BeginsWith("BCn")) map = mapn;     
    else if(filename->BeginsWith("BCa")) map = mapa; 
    else if(filename->BeginsWith("BCi")) map = mapi;          
    //    map=mapi;                                               //!!
    delete filename;
    delete tok;
    
    if(map==NULL) continue;
    in.open(file, ios::in);
    BRKDCERN *fm = new BRKDCERN(); 
    Int_t nline=0;               
    Bool_t noError=kTRUE;
    while(in.good()) {
      line.ReadLine(in);
      nline++;
      Token *t = new Token(line,";");

      if(nline==3) {                   //format
	if(t->HasNext())
	  if(t->HasNext()) SetVar(fm,"fID",t->GetD());
	    if(t->HasNext())
	      if(t->HasNext())
		if(t->HasNext()) SetVar(fm,"fVB",t->GetD());
		  if(t->HasNext())
		    if(t->HasNext())     
		      if(t->HasNext()) fm->fSerialNo = t->Get();

//      	    if(t->HasNext()) SetVar(fm,"fSerialNo", t->Get());   
      }	 
      if(nline>4) {
	if(t->HasNext())  {
	  Int_t vd=nline-4; //!!
	  Int_t vi=vd-1;
	  SetArr(fm,"faBias",vd);
	  SetArr(fm,"faItotal",vd);
	  SetArr(fm,"faIdark",vd);
	  SetArr(fm,"faIphoto",vd);
	  SetArr(fm,"faGain",vd);
	  SetArr(fm,"faGaindMdV",vd);
	  SetArr(fm,"faIdarkOverGain",vd);
	  SetArr(fm,"fadMdV",vd);
	  SetArr(fm,"faBias",vi,t->GetD());
	  if(t->HasNext()) SetArr(fm,"faItotal",vi,t->GetD());
	  if(t->HasNext()) SetArr(fm,"faIdark",vi,t->GetD());
	  if(t->HasNext()) SetArr(fm,"faIphoto",vi,t->GetD());
 	  if(t->HasNext()) SetArr(fm,"faGain",vi,t->GetD());
	  if(t->HasNext()) SetArr(fm,"faGaindMdV",vi,t->GetD());
	  if(t->HasNext()) SetArr(fm,"faIdarkOverGain",vi,t->GetD());
	  if(t->HasNext()) SetArr(fm,"fadMdV",vi,t->GetD());

	}	    
      }
      if(noError) noError=t->NoError();
      if(t)  delete t;
    }
    in.close();
    if(nline>1 && noError) {
      //         TObjString *key = new TObjString(fm->fSerialNo);
      fm->SetIndex(fm->fSerialNo);
      map->AddAt(fm,fm->GetIndex());
      //      	 map->AddAt(fm,sn2i(fm->fSerialNo));
    }	                      
    else {
      cout<<"Format Error in:"<<fm->fSerialNo<<endl;
      if(fm!=NULL) delete fm;
    } 
  }
  if(dbfi) delete dbfi; 
}
*/


void BuildDB::FillGAINPSI(TObjArray *mapn,TObjArray *mapa,TObjArray *mapi) 
{
  if(fFiles == NULL) return;
  
  TObjArrayIter *dbfi = new TObjArrayIter(fFiles);  
  TObject *fno;

  ifstream in;

  TString file,line;
  while((fno = dbfi->Next()) != 0) {

    file = ((TObjString*)fno)->GetString();
    cout<<"FillGAINPSIData, file:"<<file<<endl;
    if(!file.EndsWith(".txt")) continue;
    
    TObjArray *map = NULL;
    Token *tok = new Token(file,"/");
    while(tok->HasNext());
    TString *filename = new TString(tok->Get());
    ////     cout<<(*filename)<<endl;
    if(filename->BeginsWith("GPn")) map = mapn;     
    else if(filename->BeginsWith("GPa")) map = mapa; 
    else if(filename->BeginsWith("GPi")) map = mapi;          
    //    map=mapi;                                               //!!
    delete filename;
    delete tok;
    
    if(map==NULL) continue;
    in.open(file, ios::in);
    GAINPSI *fm = new GAINPSI(); 
    Int_t nline=0;               
    Bool_t noError=kTRUE;
    while(in.good()) {
      line.ReadLine(in);
      nline++;
      Token *t = new Token(line,";");

      if(nline==3) {                   //format
	if(t->HasNext())
	  if(t->HasNext())
	    if(t->HasNext())
	      if(t->HasNext())
		if(t->HasNext())
		  if(t->HasNext())
		    if(t->HasNext())     
		      if(t->HasNext()) fm->fSerialNo = t->Get();

//      	    if(t->HasNext()) SetVar(fm,"fSerialNo", t->Get());   
      }	 
      if(nline>4) {
	if(t->HasNext())  {
	  Int_t vd=nline-4; //!!
	  Int_t vi=vd-1;
	  SetArr(fm,"faBias",vd);
	  SetArr(fm,"faItotal",vd);
	  SetArr(fm,"faIdark",vd);
	  SetArr(fm,"faIphoto",vd);
	  SetArr(fm,"faGain",vd);
	  SetArr(fm,"faGaindMdV",vd);
	  SetArr(fm,"faIdarkOverGain",vd);
	  SetArr(fm,"fadMdV",vd);
	  SetArr(fm,"faBias",vi,t->GetD());
	  if(t->HasNext()) SetArr(fm,"faItotal",vi,t->GetD());
	  if(t->HasNext()) SetArr(fm,"faIdark",vi,t->GetD());
	  if(t->HasNext()) SetArr(fm,"faIphoto",vi,t->GetD());
 	  if(t->HasNext()) SetArr(fm,"faGain",vi,t->GetD());
	  if(t->HasNext()) SetArr(fm,"faGaindMdV",vi,t->GetD());
	  if(t->HasNext()) SetArr(fm,"faIdarkOverGain",vi,t->GetD());
	  if(t->HasNext()) SetArr(fm,"fadMdV",vi,t->GetD());

	}	    
      }
      if(noError) noError=t->NoError();
      if(t)  delete t;
    }
    in.close();
    if(nline>1 && noError) {
      //         TObjString *key = new TObjString(fm->fSerialNo);
      fm->SetIndex(fm->fSerialNo);
      map->AddAt(fm,fm->GetIndex());
      //      	 map->AddAt(fm,sn2i(fm->fSerialNo));
    }	                      
    else {
      cout<<"Format Error in:"<<fm->fSerialNo<<endl;
      if(fm!=NULL) delete fm;
    } 
  }
  if(dbfi) delete dbfi; 
}

void BuildDB::FillGAINCERN(TObjArray *mapn,TObjArray *mapa,TObjArray *mapi) 
{
   Double_t gainfit,voltage, idark=0;
   Int_t n;
   if(fFiles == NULL) return;
   TObjArrayIter *dbfi = new TObjArrayIter(fFiles);  
   TObject *fno;
   ifstream in;
   TString file,line;

   while((fno = dbfi->Next()) != 0) {
      file = ((TObjString*)fno)->GetString();
      cout<<"FillGAINCERNData, file:"<<file<<endl;
      if(!file.EndsWith(".txt")) continue;      
      TObjArray *map = NULL;
      Token *tok = new Token(file,"/");                      //razbiv. na tok. otdel /
      while(tok->HasNext());
      TString *filename = new TString(tok->Get());
#ifdef RESTRICTEDVERSION 
      if(filename->BeginsWith("GCn")) map = mapn;          
      else if(filename->BeginsWith("GCa")) map = mapa;          
      else if(filename->BeginsWith("GCi")) map = mapi;          
#endif
      if(filename->BeginsWith("GCn")) map = mapn;          
      else if(filename->BeginsWith("GCa")) map = mapa;          
      else if(filename->BeginsWith("GCi")) map = mapi;                

      delete filename;
      delete tok;
      if(map==NULL) continue;      
      in.open(file, ios::in);
      GAINCERN *fm = new GAINCERN(); 

      Int_t nline=0;
      Bool_t noError=kTRUE;
      Int_t vd=0;
      while(in.good()) {
         line.ReadLine(in);
	 nline++;
      	 Token *t = new Token(line,";");
	 if(nline==3) { 
      	    if(t->HasNext()) SetVar(fm,"fVr",t->GetD());   
      	    if(t->HasNext()) SetVar(fm,"fIdark",t->GetD());   
      	    if(t->HasNext()) SetVar(fm,"fdMdV",t->GetD());   
      	    if(t->HasNext()) SetVar(fm,"fMeanTemp",t->GetD());   
      	    if(t->HasNext()) SetVar(fm,"fVb",t->GetD());   
      	    if(t->HasNext()) //SetVar(fm,"fQEcoefficient",t->GetD());   
      	    if(t->HasNext()) SetVar(fm,"fAPDPosition",t->GetD()); 
            if(t->HasNext()) fm->fSerialNo =  t->Get();
      	    //if(t->HasNext()) SetVar(fm,"fSerialNo",t->Get());   
      	 }
	 if(nline>4) {
      	    if(t->HasNext())  {
	       vd=nline-4;
	       Int_t vi=vd-1;
	       SetArr(fm,"faVoltage",vd);
	       SetArr(fm,"faItotal",vd);
	       SetArr(fm,"faIdark",vd);
	       SetArr(fm,"faIphoto",vd);
	       SetArr(fm,"faGain",vd);
	       SetArr(fm,"fadMdV",vd);
	       SetArr(fm,"faIdarkOverGain",vd);
	       SetArr(fm,"faGaindMdV",vd);
	       SetArr(fm,"faIpinlight",vd);
	       SetArr(fm,"faIpindark",vd);
	       SetArr(fm,"faGainfit",vd);
	       SetArr(fm,"faIdarkOverGainfit",vd);

	       fm->SetIndex(fm->fSerialNo);
	       n=fm->GetIndex();
	       voltage=t->GetD();
	       SetArr(fm,"faVoltage",vi,voltage);
	       if(t->HasNext()) SetArr(fm,"faItotal",vi,t->GetD());
	       if(t->HasNext())  idark=t->GetD();
	       SetArr(fm,"faIdark",vi,idark);
	       if(t->HasNext()) SetArr(fm,"faIphoto",vi,t->GetD());
	       if(t->HasNext()) SetArr(fm,"faGain",vi,t->GetD());
	       if(t->HasNext()) SetArr(fm,"fadMdV",vi,t->GetD());
	       if(t->HasNext()) SetArr(fm,"faIdarkOverGain",vi,t->GetD());
	       if(t->HasNext()) SetArr(fm,"faGaindMdV",vi,t->GetD());
	       if(t->HasNext()) SetArr(fm,"faIpinlight",vi,t->GetD());
	       if(t->HasNext()) SetArr(fm,"faIpindark",vi,t->GetD());
               gainfit=par0a[n]/(par1a[n]*(exp(par0a[n]*(Vba[n]-voltage))-1.));
	       if(gainfit<0 || gainfit >10000) gainfit=10000;
	       SetArr(fm,"faGainfit",vi,gainfit);
	       SetArr(fm,"faIdarkOverGainfit",vi,idark/gainfit);
      	    }	    
	 }
      	 if(noError) noError = t->NoError();
         if(t)  delete t;
      }
      in.close();
      if(nline>1 && noError) {
         Float_t idoverg, vr;
         if(GetArr(fm,"faVoltage")!=NULL &&GetArr(fm,"faIdark")!=NULL &&GetArr(fm,"faIphoto")!=NULL
            &&GetArr(fm,"faGain")!=NULL ) {
      	 IdOverGain(idoverg,vr,10.0,
            GetArr(fm,"faVoltage")->GetArray() ,GetArr(fm,"faIdark")->GetArray(), 
            GetArr(fm,"faIphoto")->GetArray(),
		    GetArr(fm,"faVoltage")->GetSize(), GetArr(fm,"faGain")->GetArray());
         SetVar(fm,"fIdOverGain10",idoverg); 
         SetVar(fm,"fVr10",vr); 

      	 IdOverGain(idoverg,vr,20.0,
            GetArr(fm,"faVoltage")->GetArray() ,GetArr(fm,"faIdark")->GetArray(), 
            GetArr(fm,"faIphoto")->GetArray(),
		    GetArr(fm,"faVoltage")->GetSize(), GetArr(fm,"faGain")->GetArray());
         SetVar(fm,"fIdOverGain20",idoverg); 
         SetVar(fm,"fVr20",vr); 

      	 IdOverGain(idoverg,vr,50.0,
            GetArr(fm,"faVoltage")->GetArray() ,GetArr(fm,"faIdark")->GetArray(), 
            GetArr(fm,"faIphoto")->GetArray(),
		    GetArr(fm,"faVoltage")->GetSize(), GetArr(fm,"faGain")->GetArray());
         SetVar(fm,"fIdOverGain50",idoverg); 
         SetVar(fm,"fVr50",vr); 

      	 IdOverGain(idoverg,vr,100.0,
            GetArr(fm,"faVoltage")->GetArray() ,GetArr(fm,"faIdark")->GetArray(), 
            GetArr(fm,"faIphoto")->GetArray(),
		    GetArr(fm,"faVoltage")->GetSize(), GetArr(fm,"faGain")->GetArray());
         SetVar(fm,"fIdOverGain100",idoverg); 
         SetVar(fm,"fVr100",vr);

      	 IdOverGain(idoverg,vr,200.0,
            GetArr(fm,"faVoltage")->GetArray() ,GetArr(fm,"faIdark")->GetArray(), 
            GetArr(fm,"faIphoto")->GetArray(),
		    GetArr(fm,"faVoltage")->GetSize(), GetArr(fm,"faGain")->GetArray());
         SetVar(fm,"fIdOverGain200",idoverg); 
	 SetVar(fm,"fVr200",vr);
         }
          
//         TObjString *key = new TObjString(fm->fSerialNo);
//cout<<":"<<fm->fSerialNo<<":"<<endl;
      	 fm->SetIndex(fm->fSerialNo);
      	 map->AddAt(fm,fm->GetIndex());
      }	                      
      else {
	 cout<<"Format Error in:"<<fm->fSerialNo<<endl;
         if(fm!=NULL) delete fm;
      } 
      
   }
   if(dbfi) delete dbfi;

#ifdef MAKEST
char s[20];
Int_t ln = 30 ;
Int_t wn = 1;
Int_t vd=50;
for(Int_t i=1000;i<20000;i++) {
   if(i%500 == 0) {ln++;wn=1;} 
   if(i%100 == 0) wn++;   
   sprintf(s,"%02d%02d%06d",ln,wn,i);
   Gaincern *fm = new Gaincern();
   SetArr(fm,"faVoltage",vd);
   SetArr(fm,"faItotal",vd);
   SetArr(fm,"faIdark",vd);
   SetArr(fm,"faIphoto",vd);
   SetArr(fm,"faGain",vd);
   SetArr(fm,"fadMdV",vd);
   SetArr(fm,"faIdarkOverGain",vd);
   SetArr(fm,"faGaindMdV",vd);
   SetArr(fm,"faIpinlight",vd);
   SetArr(fm,"faIpindark",vd);

   fm->SetSerialNo(s);
   fm->SetIndex(s);
   mapn->AddAt(fm,i);
}

ln = 30 ;
wn = 1;
vd=50;
for(Int_t i=1000;i<20000;i++) {
   if(i%500 == 0) {ln++;wn=1;} 
   if(i%100 == 0) wn++;   
   sprintf(s,"%02d%02d%06d",ln,wn,i);
   Gain *fm = new Gain();
   SetArr(fm,"faVoltage",vd);
   SetArr(fm,"faItotal",vd);
   SetArr(fm,"faIdark",vd);
   SetArr(fm,"faIphoto",vd);
   SetArr(fm,"faGain",vd);
   SetArr(fm,"fadMdV",vd);
   SetArr(fm,"faIdarkOverGain",vd);
   SetArr(fm,"faGaindMdV",vd);
   SetArr(fm,"faIpinlight",vd);
   SetArr(fm,"faIpindark",vd);

   fm->SetSerialNo(s);
   fm->SetIndex(s);
   mapi->AddAt(fm,i);
}

ln = 30 ;
wn = 1;
vd=50;
for(Int_t i=1000;i<20000;i++) {
   if(i%500 == 0) {ln++;wn=1;} 
   if(i%100 == 0) wn++;   
   sprintf(s,"%02d%02d%06d",ln,wn,i);
   Gaincern *fm = new Gaincern();
   SetArr(fm,"faVoltage",vd);
   SetArr(fm,"faItotal",vd);
   SetArr(fm,"faIdark",vd);
   SetArr(fm,"faIphoto",vd);
   SetArr(fm,"faGain",vd);
   SetArr(fm,"fadMdV",vd);
   SetArr(fm,"faIdarkOverGain",vd);
   SetArr(fm,"faGaindMdV",vd);
   SetArr(fm,"faIpinlight",vd);
   SetArr(fm,"faIpindark",vd);

   fm->SetSerialNo(s);
   fm->SetIndex(s);
   mapa->AddAt(fm,i);
}

#endif

}


void BuildDB::FillMnoise(TObjArray *mapi) {   
  //TString hmark = "S8148";
   //cout<<"fFilesFillHamamatsu"<<fFiles<<endl;
   if(fFiles == NULL) return;
   TObjArrayIter *dbfi = new TObjArrayIter(fFiles);  
   TObject *fno;
   ifstream in;
   TString file,line;
   while((fno = dbfi->Next()) != 0) {
      file = ((TObjString*)fno)->GetString();
      cout<<"FillMnoiseData, file:"<<file<<endl;
      if(!file.EndsWith(".txt")) {cout<<"error in the file name"<<endl; continue;}
      TObjArray *map = NULL;
      map = mapi;
      in.open(file, ios::in);
      while(in.good()) {
         line.ReadLine(in); 
	 if(line.Sizeof()<40) continue;
	 //cout<<"line:::"<<line<<endl;
         Mnoise *fm = new Mnoise();      
      	 //smno = &fm;     
         Token *t = new Token(line,","); 
         //if(t->HasNext()) {
	   //if(!hmark.CompareTo(t->Get())) {    
	   //  if(t->HasNext()) fm->fDate = t->Get();
	 if(t->HasNext()) fm->SetSerialNo(t->Get());
	 ///if(t->HasNext()) fm->fSerialNo = t->Get();
	 /// fm->SetIndex(fm->fSerialNo);
	 if(t->HasNext()) fm->fch = atoi(t->Get());
	 //	 cout<<"fSerialNo "<<fm->fSerialNo<<endl;
	 //cout<<fm->fch<<endl;
	       if(t->HasNext()) 
		 if(t->HasNext())
		   if(t->HasNext())
		     if(t->HasNext())
		       if(t->HasNext())
			 if(t->HasNext()) fm->fTemperature = t->GetD();
	       //    cout<<"T "<<fm->fTemperature<<endl;
	       if(t->HasNext())
		 if(t->HasNext())
		   if(t->HasNext())
		     if(t->HasNext())
		       if(t->HasNext()) fm->fNoise1 = t->GetD();
	       if(t->HasNext())
		 if(t->HasNext())
		   if(t->HasNext())
		     if(t->HasNext())
		       if(t->HasNext())
			 if(t->HasNext()) fm->fNoise50 = t->GetD();
	       // cout<<"N50 "<<fm->fNoise50<<endl;
	       if(t->HasNext())
		 if(t->HasNext())
		   if(t->HasNext())
		     if(t->HasNext())
		       if(t->HasNext())
			 if(t->HasNext()) fm->fNoise150 = t->GetD();
	       // cout<<"N150 "<<fm->fNoise150<<endl;
	       if(t->HasNext())
		 if(t->HasNext())
		   if(t->HasNext())
		     if(t->HasNext())
		       if(t->HasNext())
			 if(t->HasNext()) fm->fNoise300 = t->GetD();
	       //cout<<"N300 "<<fm->fNoise300<<endl;
	       // fm->fnp1 = 13;
	       //  fm->fnp2 = 12;
               // if(t->HasNext()) SetVar(fm,"fNoise",pow(10.0,-(t->GetD())/20.0)*1.0e5);       
	       // if(t->HasNext()) SetArr(fm,"faNoise",i, pow(10.0,-(t->GetD())/20.0)*1.0e5); //vich. noise
	       if(t->Tokens()>1 && t->NoError() ) {
		 const char *ss =  fm->fSerialNo.Data();
		 fm->SetIndex(ss);
		 map->AddAt(fm,fm->GetIndex());            //dobavit' object Mnoise v conteyner v poziciyu sootv. nomeru APD.
	       }	   
	       else {
		 cout<<"Format Error in:"<<fm->fSerialNo<<endl;
		 if(fm!=NULL) delete fm;
	       } 
	       //}
	       //}
	       if(t!=NULL)  delete t;
	       //         if(fm!=NULL) delete fm;
      }
      in.close();
   }
   if(dbfi) delete dbfi;
   //   mHam->Write("mHam",TObject::kSingleKey);
#ifdef MAKEST
   /*
     char s[20];
Int_t ln = 30 ;
Int_t wn = 1;

 for(Int_t i=5000;i<20000;i++) {
   if(i%500 == 0) {ln++;wn=1;} 
  
   if(i%100 == 0) wn++;
   
   sprintf(s,"%02d%02d%06d",ln,wn,i);
   Hamamatsu *h = new Hamamatsu();
   SetArr(h,"faVoltage",13);
   SetArr(h,"faDarkCurrent",13);
   SetArr(h,"faTotalCurrent",12);
   SetArr(h,"faNoise",12);
   SetArr(h,"faCapacitance",12);	       	        
   SetArr(h,"faGain",12);	       	        
   SetArr(h,"faIdarkOverGain",12);	       	        
   SetArr(h,"fadMdV",12);	       	        
   SetArr(h,"faNoiseRatio",12);	       	        
   h->SetSerialNo(s);
   h->SetIndex(s);
   mHam->AddAt(h,i);
 }
   */
#endif

}




////////////////////////////////////////////////
// Some Calculations from data
////////////////////////////////////////////////
//#include<TSpline.h>
void BuildDB::IdOverGain(Float_t &idoverg, Float_t &vatg,
      	             	 Float_t g, Float_t *v, Float_t *idark, Float_t *iph,Int_t n,
			 Float_t *mygain) {
   if(v ==NULL || idark==NULL || iph==NULL || n==0 || mygain==NULL) { cout<<"cannot calculate IdOverGain"<<endl;return;}   

   if(n<1) {cout<<"Error in IdOverGain n<1"<<n<<endl; exit(1);}
//   Double_t *v_d = new Double_t[n];
//   Double_t *idark_d = new  Double_t[n];
//   Double_t *iph_d = new Double_t[n];

   for(Int_t i=0;i<n;i++) {
//      v_d[i]=v[i];idark_d[i]=idark[i];iph_d[i]=iph[i];
   }   

   Spline siph("iph",v,iph,n);
   Float_t  bias = ( siph.Eval(25.0)+siph.Eval(30.0)+
      	             siph.Eval(35.0)+siph.Eval(40.0))/4.0;
   Float_t *gain = new Float_t[n];
   for(Int_t i=0;i<n;i++)gain[i] = iph[i]/bias;   

   Int_t lindex=-1, hindex=-1;
   for(Int_t i=0;i<n;i++) {
      if(gain[i]<g) lindex=i;
   }
   for(Int_t i=n-1;i>=0;i--) {
      if(gain[i]>g) hindex=i;
   }


//try to skip wrong last 2 point
   if(hindex<=lindex && n>2) {
      lindex=-1; hindex=-1;
      for(Int_t i=0;i<n-2;i++) {
	 if(gain[i]<g) lindex=i;
      }
      for(Int_t i=n-3;i>=0;i--) {
	 if(gain[i]>g) hindex=i;
      }      
   }



   if(lindex==-1 || hindex==-1 || hindex<=lindex) {
     //com      cout<<"Gain "<<g<<"can not be reached..."<<endl;
     //com  for(Int_t i=0;i<n;i++) cout<<"Gain::"<<v[i]<<"  "<<gain[i]<<" "<<iph[i] <<endl;
   }
   else {  
      Spline *sidark = new Spline("idark",&v[lindex],&idark[lindex],hindex-lindex+1);
      Spline *svolt = new Spline("volt",&gain[lindex],&v[lindex],hindex-lindex+1);
      vatg = svolt->Eval(g);
      idoverg = (sidark->Eval(vatg))/g;
//      cout<<"Gain|"<<vatg<<"  "<<idoverg<<"  "<<g<<endl;
      delete sidark;
      delete svolt;
   }
   delete [] gain;
//   delete v_d;
//   delete iph_d;
//   delete idark_d;

}

void BuildDB::IdOverGainHam(Int_t /* serno */,Float_t &idoverg, Float_t &vatg,
      	             	 Float_t g, Float_t *v, Float_t *idark, Float_t *iph,const Int_t n) {
if(v ==NULL || idark==NULL || iph==NULL || n==0) { cout<<"cannot calculate IdOverGainHam"<<endl;return;}
//   Double_t *v_d = new  Double_t[n];
//   Double_t *idark_d = new  Double_t[n];
   Float_t *iph_d = new  Float_t[n];
   for(Int_t i=0;i<n;i++) {
//      v_d[i]=v[i];idark_d[i]=idark[i];
      iph_d[i]=iph[i]-idark[i];
   }   
   Spline siph("iph",v,iph_d,n);
   Float_t  bias = siph.Eval(20.0);
   Float_t *gain = new Float_t[n];
   for(Int_t i=0;i<n;i++) {
      gain[i] = iph_d[i]/bias;  
   } 

   Int_t lindex=-1, hindex=-1;
   for(Int_t i=0;i<n;i++) {
      if(gain[i]<g) lindex=i;
   }
   for(Int_t i=n-1;i>=0;i--) {
      if(gain[i]>g) hindex=i;
   }

//try to skip probably wrong last 2 point
   if(hindex<=lindex && n>2) {
      lindex=-1; hindex=-1;
      for(Int_t i=0;i<n-2;i++) {
	 if(gain[i]<g) lindex=i;
      }
      for(Int_t i=n-3;i>=0;i--) {
	 if(gain[i]>g) hindex=i;
      }      
   }


   if(lindex==-1 || hindex==-1 || hindex<=lindex) {
     //com      cout<<"SNo:"<<serno<<endl;
     //com  cout<<"GainHam "<<g<<" can not be calculated..."<<endl;
//      for(Int_t i=0;i<n;i++) cout<<"Gain::"<<v[i]<<"  "<<gain[i]<<endl;
     //com for(Int_t i=0;i<n;i++) cout<<"Gain::"<<v[i]<<"  "<<gain[i]<<" "<<iph[i] <<" "
     //             	      	 <<idark[i]<<endl;
      
   }
   else {
      Spline sidark("idark",&v[lindex],&idark[lindex],hindex-lindex+1);
      Spline svolt("volt",&gain[lindex],&v[lindex],hindex-lindex+1);
      vatg = svolt.Eval(g);
//Tempereture correction
//      vatg-=0.7*(((*sham)->fTemperature)-25.0);      
//      Spline svolt("volt",&tgain[0],&v_d[lindex],hindex-lindex+1);
//      vatg = svolt.Eval(1.0/sqrt(g));
      idoverg = sidark.Eval(vatg) / g;
//      cout<<"Gain|"<<vatg<<"  "<<idoverg<<"  "<<g<<endl;
   }
   delete [] gain;
//   delete [] v_d;
   delete [] iph_d;
//   delete [] idark_d;

}

void BuildDB::CalcQE(Hamamatsu *fm){
   Float_t i0ref,idref,i0;
   i0ref = GetVar(fm,"fI0Reference");
   idref = GetVar(fm,"fIDReference");
   i0 = GetVar(fm,"fI0");   
   if(i0ref!=0.0 && idref!=0.0 && (i0ref-idref)>0.0)
      SetVar(fm,"fQE", 78.5*i0/(i0ref-idref));
}
      	          

void BuildDB::CalcGain(Hamamatsu *fm){
  Long_t fnpn; (fm->fLot>61 || fm->fLot<25)?fnpn=13:fnpn=12;
   TArrayF  *pfaTotalCurrent = GetArr(fm,"faTotalCurrent");
   TArrayF  *pfaDarkCurrent  = GetArr(fm,"faDarkCurrent");
   TArrayF  *pfaGain = GetArr(fm,"faGain");
   TArrayF  *pfaIdarkOverGain = GetArr(fm,"faIdarkOverGain");
   TArrayF  *pfaVoltage = GetArr(fm,"faVoltage");
   TArrayF  *pfadMdV = GetArr(fm,"fadMdV");
   TArrayF  *pfaNoise = GetArr(fm,"faNoise");
   TArrayF  *pfaCapacitance = GetArr(fm,"faCapacitance");
   TArrayF  *pfaNoiseRatio = GetArr(fm,"faNoiseRatio");

   TArrayF  &faTotalCurrent = *pfaTotalCurrent;
   TArrayF  &faDarkCurrent  = *pfaDarkCurrent;
   TArrayF  &faGain = *pfaGain;
   TArrayF  &faIdarkOverGain = *pfaIdarkOverGain;
   TArrayF  &faVoltage = *pfaVoltage; //12.10
   TArrayF  &fadMdV = *pfadMdV;
   TArrayF  &faNoise = *pfaNoise;
//   TArrayF  &faCapacitance = *pfaCapacitance;
   TArrayF  &faNoiseRatio = *pfaNoiseRatio;
   if(pfaTotalCurrent==NULL || pfaDarkCurrent==NULL ||  pfaGain==NULL ||  pfaIdarkOverGain==NULL || 
      pfaVoltage==NULL ||  pfadMdV==NULL ||  pfaNoise==NULL ||  pfaNoiseRatio==NULL ) return;

   Float_t unit = faTotalCurrent[0]-faDarkCurrent[0];
   //SetArr(fm,"faGain",fm->fnp1);//Mon Sep 30
   SetArr(fm,"faGain",fnpn);
   faGain[0] = 1.0;
   if(unit!=0.0) {
     //  for(Int_t i=1;i<fm->fnp1;i++) //Mon Sep 30 2->1
     for(Int_t i=1;i<fnpn;i++) faGain[i] = ((faTotalCurrent[i]-faDarkCurrent[i])/unit);
   }

   //SetArr(fm,"faIdarkOverGain",fm->fnp2); //Mon Sep 30
   SetArr(fm,"faIdarkOverGain",fnpn);   //Mon Sep 30
   //for(Int_t i=0;i<fm->fnp1;i++) {//Mon Sep 30
   for(Int_t i=0;i<fnpn;i++) {
     if(faGain[i]!=0.0) {
       faIdarkOverGain[i] = (faDarkCurrent[i]/faGain[i]);
     }
   }  
   for (Int_t i=2;i<fm->fnp2-1;i++){
     /*
     if(faIdarkOverGain[i-1]-faIdarkOverGain[i-2]>0.05*faIdarkOverGain[i-2] 
	 && faIdarkOverGain[i]-faIdarkOverGain[i-2]>0.05*faIdarkOverGain[i-2]) 
	{if (faGain[i-2]>150) {SetVar(fm,"fGain_incrIDoverM",faGain[i-2]); break; }
	else {SetVar(fm,"fGain_incrIDoverM",0);break;}}
      else {SetVar(fm,"fGain_incrIDoverM",0); }
     */
     //cout<<"before fGain_incrIDoverM"<<endl;
     if(faGain[i-1]>40 && faGain[i-1]<400 && faIdarkOverGain[i]-faIdarkOverGain[i-1]>0.1*faIdarkOverGain[i-1]) 
       {if(faIdarkOverGain[i+1]-faIdarkOverGain[i]>0 || i==fm->fnp2-1)  SetVar(fm,"fGain_incrIDoverM",faGain[i-1]);}
     else {SetVar(fm,"fGain_incrIDoverM",0);}
   }
   // segodnya
//   Double_t *v_d = new  Double_t[fm->fnp2];
//   Double_t *gain_d = new  Double_t[fm->fnp2];
//   for(Int_t i=0;i<fm->fnp2;i++) {
//      v_d[i]=((fm->faVoltage).GetArray())[i];
//      gain_d[i]=((fm->faGain).GetArray())[i];
//   }   
   Spline *spl = new Spline("spl",pfaVoltage,pfaGain,fm->fnp2);


//
   Float_t* xder = new Float_t[fm->fnp2];
   Float_t* yder = new Float_t[fm->fnp2];
   spl->Derivative(xder,yder);


   xder[fm->fnp2-1] = xder[fm->fnp2-2]; 
   yder[fm->fnp2-1] = yder[fm->fnp2-2]; 

   for(Int_t i=1;i<fm->fnp2;i++) {
      if(xder[i]<xder[i-1]) {
	//com cout<<"Warning:Mean Gain points are not in increasing order at "<<fm->fSNo<<endl;
      	//com cout<<"::"<<xder[i-1] <<"::"<<xder[i]<<endl;
      	 xder[i] = xder[i-1];
      }
   }
   Spline *splder = new Spline("splder",xder,yder,fm->fnp2);

   SetArr(fm,"fadMdV",fm->fnp2);
   for(Int_t i=0;i<fm->fnp2;i++) {
      if(faGain[i]!=0.0) {
//         fm->fadMdV[i] = 100.0*(splder->Eval(v_d[i]))*sqrt(fm->faGain[i])*2.0; //100.0*arrb[i]/fm->faGain[i];
         fadMdV[i] = 100.0*(splder->Eval(faGain[i]));  ///(fm->faGain[i]); //100.0*arrb[i]/fm->faGain[i];
      }
      if(i==0 || i==(fm->fnp2-1) || i==1) fadMdV[i]=0.0; 
   }   
   if(GetVar(fm,"fVr50")>0.0) {
//      fm->fdMdV = 100.0*splder->Eval(fm->fVr50)*sqrt(50.0)*2.0;
      SetVar(fm,"fdMdV", 100.0*splder->Eval(50.0));///(50.0);
//cout<<"::"<<splder->Eval(-1.0/fm->fVr50)<<"::"<<1.0/fm->fVr50<<endl;
//      	fm->fdMdV = 100.0*(splder->Eval(-1.0/(fm->fVr50)))*log(50.0)/(fm->fVr50*fm->fVr50);

   }
   delete splder; 
   delete [] xder;
   delete [] yder;
   par1=0;
   par0=0;
   /*   Int_t maxp=6;

   Double_t sxy=0, sx2=0, sy=0, sx=0 ;
   for(Int_t i=2;i<maxp;i++) {
     sxy+=((fadMdV[i])/100.0)*(faGain[i]);
     sx+=faGain[i]; sy+=(fadMdV[i])/100.0;
     sx2+=(faGain[i])*(faGain[i]);
   }
   //cout<<"fadMdV[2]="<<fadMdV[2]<<endl;
   //cout<<"faGain[2]="<<faGain[2]<<endl;
   par1=(sxy-sx*sy/(maxp-2))/(sx2-sx*sx/(maxp-2));
   par0=(sy-par1*sx)/(maxp-2);
   */
   Int_t size=7; // faGain->GetSize();
   TGraph *grf = new TGraph(size,&(faGain[0]),&(fadMdV[0])); //posmotret' zdes' utechku pamyati.!!!!!!!!!!

   Int_t inach, ikon;
   if(fadMdV[2]>3.7 || fadMdV[2]<2.2 || fadMdV[3]>3.7 || fadMdV[3]<2.2) {inach=4; ikon=7;}
   else{inach=2 ; ikon=5;}
   Double_t xi=faGain[inach];
   Double_t xk=faGain[ikon];
   TF1 *f1 = new TF1("f1","pol1",xi-1,xk+1);
   grf->Fit(f1,"RQN");
   par1 = (f1->GetParameter(1))/100.;
   par0 = (f1->GetParameter(0))/100.;
   errpar0 = (f1->GetParError(0))/100.;
   errpar1 = (f1->GetParError(1))/100.;
   //   cout<<"errpar1="<<errpar1<<"   errpar0="<<errpar0<<endl;
   delete f1;
   delete grf;
   // cout<<"par1="<<par1<<"  par0="<<par0<<endl;
   SetVar(fm,"fpar0",par0);
   SetVar(fm,"fpar1",par1);
   VBt=0;
   
   for(Int_t i=inach;i<ikon+1;i++) {
     //if(i>1 && i<6)
     VBp[i-inach]=(log(1+par0/(par1*faGain[i])))/par0+faVoltage[i];
     // cout<<par0/(par1*faGain[i])<<endl;
     VBt+=VBp[i-inach];
   }
   VBt=VBt/4;
   dVBt=0;
   for(Int_t i=0;i<4;i++) dVBt+=(VBp[i]-VBt)*(VBp[i]-VBt);
   dVBt=sqrt(dVBt/3.);
   dVBtpar0=fabs(((1-(par1*faGain[inach+1]+par0)*(log(1+par0/(par1*faGain[inach+1])))/par0)/((par0+par1*faGain[inach+1])*par0))*errpar0);
   dVBtpar1=fabs((1./((par0+par1*faGain[inach+1])*par1))*errpar1);
   SetVar(fm,"fVBt",VBt);
   SetVar(fm,"fdVBt",dVBt);
   SetVar(fm,"fdVBtpar0",dVBtpar0);
   SetVar(fm,"fdVBtpar1",dVBtpar1);
   //cout<<"Vb="<<Vb<<endl;
   const char *ss =  fm->fSerialNo.Data();
   fm->SetIndex(ss);
   Int_t nom=fm->GetIndex();
   //   cout<<fm->GetIndex()<<endl;
   BuildDB::par0a[nom]=par0;
   BuildDB::par1a[nom]=par1;
   BuildDB::Vba[nom]=VBt;
   BuildDB::Temph[nom]=fm->fTemperature;
           //((TGraph *)o)->GetPoint(i, xi, yi);
   //((TGraph *)o)->GetPoint(k, xk, yk);
   
   //cout<<"Vb["<<nom<<"]="<<Vba[nom]<<endl;
   //cout<<"par1="<<par1<<"  par0="<<par0<<" Vb="<<Vb<<" faVoltage[2]="<<faVoltage[2]<<" faGain[2]="<<faGain[2]<<endl;
   //TF1 *f1 = new TF1("f1","[0]/([1]*(exp([0]*([2]-x)/100.)-1.))",0,vb-2);

   /* TF1 *f1 = new TF1("f1","pol1",xi-1,xk+1);
       ((TGraph *)o)->Fit(f1,"RN");
       p1 = f1->GetParameter(1);
       p0 = f1->GetParameter(0); */

//   delete gain_d; 
   if(GetVar(fm,"fVr50")>0.0) {
/*
      Int_t i;
      for(i=0;i<fm->fnp2;i++) if(fm->faVoltage[i]>=fm->fVr50) break; 
      i--; 
      if(i>=0) { 
//         spl->GetCoeff(i,x,y,b,c,d);
         Float_t dx = fm->fVr50 - fm->faVoltage[i];
      	 fm->fdMdV = 100.0*(spl->EvalD(fm->fVr50))/50.0; //100.0*((b+dx*(c+dx*d)) + dx*(c+2.0*d*dx))/50.0;
      }
*/

//      Double_t *idark_d = new Double_t[fm->fnp2];
//      Double_t *noise_d = new Double_t[fm->fnp2];
//      Double_t *ct_d = new Double_t[fm->fnp2];
//      for(Int_t j=0;j<fm->fnp2;j++) {
//      	 idark_d[j] = ((fm->faDarkCurrent).GetArray())[j];
//      	 noise_d[j] = ((fm->faNoise).GetArray())[j];
//      	 ct_d[j]=((fm->faCapacitance).GetArray())[j];
//      }

      Spline *idarks = new Spline("idarks",pfaVoltage,pfaDarkCurrent,fm->fnp2);
      Spline *cts = new Spline("cts",pfaVoltage,pfaCapacitance,fm->fnp2);
      Spline *noises = new Spline("noises",pfaVoltage,pfaNoise,fm->fnp2);

//      fm->fNoise50 = noises->Eval(((Double_t)fm->fVr50));
      Double_t tmpd=GetVar(fm,"fVr50");
      Double_t tmpd2=GetVar(fm,"fVr100");
      Double_t tmpd3=GetVar(fm,"fVr200");
      SetVar(fm,"fID50", idarks->Eval(tmpd));
      SetVar(fm,"fID100",idarks->Eval(tmpd2));
      SetVar(fm,"fID200",idarks->Eval(tmpd3));
      SetVar(fm,"fNoise50", noises->Eval(tmpd));
      SetVar(fm,"fNoise100",noises->Eval(tmpd2));
      SetVar(fm,"fNoise200",noises->Eval(tmpd3));
      SetVar(fm,"fCt50",  cts->Eval(((Double_t)fm->fVr50)));
      //==SetVar(fm,"fCt50",  cts->Eval(tmpd));
      if(GetVar(fm,"fIdOverGain50")>0.0) {    
        //SetVar(fm,"fNoiseRatio50", 
        //(GetVar(fm,"fNoise50")/sqrt((GetVar(fm,"fIdOverGain50")*50.0)*50.0*(1.75+0.007*50.0)))*1.0e2);
      }
//      delete idark_d;
//      delete noise_d;
//      delete ct_d;
      delete idarks;
      delete cts;
      delete noises;
   }
   
//   delete v_d;
   delete spl;
   SetArr(fm,"faNoiseRatio",fm->fnp2);

   for(Int_t i=0;i<fm->fnp2;i++) {
      if(faGain[i]!=0.0) {
      	 faNoiseRatio[i]=
	   (faNoise[i]/sqrt(faDarkCurrent[i]*faGain[i]*(1.75+0.007*faGain[i])))*1.0e2; //vichisl. noiseratio
//      	    fm->faNoise[i]/fm->faGain[i]);
      }
      if(i==0) faNoiseRatio[i]=0.0; 
   }
}   	 

void BuildDB::QuentinparCERN(BRKDCERN *fm){
   TArrayF  *pfaGainfit = GetArr(fm,"faGainfit");
   TArrayF  *pfaIdarkOverGainfit = GetArr(fm,"faIdarkOverGainfit");
   TArrayF  &faGainfit = *pfaGainfit;
   TArrayF  &faIdarkOverGainfit = *pfaIdarkOverGainfit;
   Int_t size=pfaGainfit->GetSize();
    for (Int_t i=2;i<size;i++){
      if(faIdarkOverGainfit[i-1]-faIdarkOverGainfit[i-2]>0.05*faIdarkOverGainfit[i-2] 
	 && faIdarkOverGainfit[i]-faIdarkOverGainfit[i-2]>0 && faGainfit[i-1]>faGainfit[i-2]) //0.05*faIdarkOverGainfit[i-2]) 
	{if (faGainfit[i-2]>40 && faGainfit[i-2]<400) {SetVar(fm,"fGain_incrIDoverM",faGainfit[i-2]); break; }
	else {SetVar(fm,"fGain_incrIDoverM",0);break;}}
      else {SetVar(fm,"fGain_incrIDoverM",0); }
    }
}  

void BuildDB::QuentinparPSI(BRKDPSI *fm){
   TArrayF  *pfaGainfit = GetArr(fm,"faGainfit");
   TArrayF  *pfaIdarkOverGainfit = GetArr(fm,"faIdarkOverGainfit");
   TArrayF  &faGainfit = *pfaGainfit;
   TArrayF  &faIdarkOverGainfit = *pfaIdarkOverGainfit;
   Int_t size=pfaGainfit->GetSize();
    for (Int_t i=2;i<size;i++){
      if(faIdarkOverGainfit[i-1]-faIdarkOverGainfit[i-2]>0.05*faIdarkOverGainfit[i-2] 
	 && faIdarkOverGainfit[i]-faIdarkOverGainfit[i-2]>0 && faGainfit[i-1]>faGainfit[i-2]) //0.05*faIdarkOverGainfit[i-2]) 
	{if (faGainfit[i-2]>100 && faGainfit[i-2]<400) {SetVar(fm,"fGain_incrIDoverM",faGainfit[i-2]); break; }
	else {SetVar(fm,"fGain_incrIDoverM",0);break;}}
      else {SetVar(fm,"fGain_incrIDoverM",0); }
    }
}

void BuildDB::IDforshumPSI(BRKDPSI *fm){
  Float_t T;
  Int_t n,p1,p2,k;
  Double_t Vb,V,ID50,ID150,ID300;
  TArrayF  *pfaBias = GetArr(fm,"faBias");
  TArrayF  *pfaIdark = GetArr(fm,"faIdark");   
  TArrayF  &faBias = *pfaBias;
  TArrayF  &faIdark = *pfaIdark;
  T = GetVar(fm,"fTemperature");
  n=fm->GetIndex();
  //cout<<Vba[n]<<endl;
  Vb=Vba[n]+0.76*(T-Temph[n]);
  //VBh=h->fVB;
      V=Vb-log(1+par0a[n]/(50*par1a[n]))/par0a[n];
      p2=pfaBias->GetSize()-1; p1=0;     
      while(p1<(p2-1)) {
	k = ((p2+p1)>>1);
	if(faBias[k]>V) p2=k;
	else p1=k;
      }
      if(faBias[p1+1]>V)  {ID50=(faIdark[p1+1]-faIdark[p1])*(V-faBias[p1])/(faBias[p1+1]-faBias[p1])+faIdark[p1];}
      else {ID50=1;}
      SetVar(fm,"fID50",ID50);
      //cout<<ID50<<endl;
    V=Vb-log(1+par0a[n]/(150*par1a[n]))/par0a[n];
      p2=pfaBias->GetSize()-1; p1=0;     
      while(p1<(p2-1)) {
	k = ((p2+p1)>>1);
	if(faBias[k]>V) p2=k;
	else p1=k;
      }
      if(faBias[p1+1]>V) {ID150=(faIdark[p1+1]-faIdark[p1])*(V-faBias[p1])/(faBias[p1+1]-faBias[p1])+faIdark[p1];}
      else {ID150=1;}
      SetVar(fm,"fID150",ID150);

    V=Vb-log(1+par0a[n]/(300*par1a[n]))/par0a[n];
      p2=pfaBias->GetSize()-1; p1=0;     
      while(p1<(p2-1)) {
	k = ((p2+p1)>>1);
	if(faBias[k]>V) p2=k;
	else p1=k;
      }
      if(faBias[p1+1]>V) {ID300=(faIdark[p1+1]-faIdark[p1])*(V-faBias[p1])/(faBias[p1+1]-faBias[p1])+faIdark[p1];}
      else {ID300=1;}
      SetVar(fm,"fID300",ID300);
}
void BuildDB::IDforshumCERN(BRKDCERN *fm){
  Float_t T;
  Int_t n,p1,p2,k;
  Double_t Vb,V,ID50,ID150,ID300;
  TArrayF  *pfaBias = GetArr(fm,"faBias");
  TArrayF  *pfaIdark = GetArr(fm,"faIdark");   
  TArrayF  &faBias = *pfaBias;
  TArrayF  &faIdark = *pfaIdark;
  T = GetVar(fm,"fTemperature");
  n=fm->GetIndex();
  //cout<<Vba[n]<<endl;
  Vb=Vba[n]+0.76*(T-Temph[n]);
  //VBh=h->fVB;
      V=Vb-log(1+par0a[n]/(50*par1a[n]))/par0a[n];
      p2=pfaBias->GetSize()-1; p1=0;     
      while(p1<(p2-1)) {
	k = ((p2+p1)>>1);
	if(faBias[k]>V) p2=k;
	else p1=k;
      }
      if(faBias[p1+1]>V)  {ID50=(faIdark[p1+1]-faIdark[p1])*(V-faBias[p1])/(faBias[p1+1]-faBias[p1])+faIdark[p1];}
      else {ID50=1;}
      SetVar(fm,"fID50",ID50);
      //cout<<ID50<<endl;
      V=Vb-log(1+par0a[n]/(150*par1a[n]))/par0a[n];
      p2=pfaBias->GetSize()-1; p1=0;     
      while(p1<(p2-1)) {
	k = ((p2+p1)>>1);
	if(faBias[k]>V) p2=k;
	else p1=k;
      }
      if(faBias[p1+1]>V) {ID150=(faIdark[p1+1]-faIdark[p1])*(V-faBias[p1])/(faBias[p1+1]-faBias[p1])+faIdark[p1];}
      else {ID150=1;}
      SetVar(fm,"fID150",ID150);

      V=Vb-log(1+par0a[n]/(300*par1a[n]))/par0a[n];
      p2=pfaBias->GetSize()-1; p1=0;     
      while(p1<(p2-1)) {
	k = ((p2+p1)>>1);
	if(faBias[k]>V) p2=k;
	else p1=k;
      }
      if(faBias[p1+1]>V) {ID300=(faIdark[p1+1]-faIdark[p1])*(V-faBias[p1])/(faBias[p1+1]-faBias[p1])+faIdark[p1];}
      else {ID300=1;}
      SetVar(fm,"fID300",ID300);
}


#include "Apdlist.h"
#include <TMethodCall.h>


void BuildDB::SetVar(TObject *ox, const char *var, Float_t val) {
   if(ox==NULL) return;
   TDataMember *dm = (ox->IsA())->GetDataMember(var);
   if(dm==NULL) return;
   TMethodCall *setter = dm->SetterMethod(ox->IsA());
   if(setter==NULL) return;
   char s[80];
   sprintf(s,"%f",val);
   setter->Execute(ox,s);
}

void BuildDB::SetVar(TObject *ox, const char *var, Char_t* val) {
  //com cout<<"SetVar:"<<val<<":"<<endl;
   if(ox==NULL) return;
   TDataMember *dm = (ox->IsA())->GetDataMember(var);
   if(dm==NULL) return;
   TMethodCall *setter = dm->SetterMethod(ox->IsA());
   if(setter==NULL) return;
   char s[80];
   sprintf(s,"%s",val);
   setter->Execute(ox,s);
}


void BuildDB::SetVar(TObject *ox, const char *var, Double_t val) {
   if(ox==NULL) return;
   TDataMember *dm = (ox->IsA())->GetDataMember(var);
   if(dm==NULL) return;
   TMethodCall *setter = dm->SetterMethod(ox->IsA());
   if(setter==NULL) return;
   char s[80];
   sprintf(s,"%f",(Float_t)val);
   setter->Execute(ox,s);
}

void BuildDB::SetVar(TObject *ox, const char *var, Int_t val) {
   if(ox==NULL) return;
   TDataMember *dm = (ox->IsA())->GetDataMember(var);
   if(dm==NULL) return;
   TMethodCall *setter = dm->SetterMethod(ox->IsA());
   if(setter==NULL) return;
   char s[80];
   sprintf(s,"%i",val);
   setter->Execute(ox,s);
}


void BuildDB::SetArr(TObject *ox, const char *var, Int_t val) {
   if(ox==NULL) return;
   TDataMember *dm = (ox->IsA())->GetDataMember(var);
   if(dm==NULL) return;
   TMethodCall *getter = dm->GetterMethod(ox->IsA());
   if(getter==NULL) return;
   TString type = dm->GetFullTypeName();
   if(type.BeginsWith("TArrayF")) {
      Longptr_t retd;
      getter->Execute(ox,"",retd);
      ((TArrayF*) (retd))->Set(val);
   }
}   

void BuildDB::SetArr(TObject *ox, const char *var,Int_t i, Float_t val) {
   if(ox==NULL) return;
   TDataMember *dm = (ox->IsA())->GetDataMember(var);
   if(dm==NULL) return;
   TMethodCall *getter = dm->GetterMethod(ox->IsA());
   if(getter==NULL) return;
   TString type = dm->GetFullTypeName();
   if(type.BeginsWith("TArrayF")) {
      Longptr_t retd;
      getter->Execute(ox,"",retd);
      ((TArrayF*) (retd))->AddAt(val,i);
   }
}   

TArrayF * BuildDB::GetArr(TObject *ox, const char *var) {
   if(ox==NULL) return NULL;
   TDataMember *dm = (ox->IsA())->GetDataMember(var);
   if(dm==NULL) return NULL;
   TMethodCall *getter = dm->GetterMethod(ox->IsA());
   if(getter==NULL) return NULL;
   TString type = dm->GetFullTypeName();
   if(type.BeginsWith("TArrayF")) {
      Longptr_t retd;
      getter->Execute(ox,"",retd);
      return ((TArrayF*) (retd));
   }
   return NULL;
}   

Float_t BuildDB::GetVar(TObject *ox,const char *var) {
   if(ox==NULL) {
//      cout<<"GetVar::GetVar from NULL object"<<endl;
      return 0;
   }
   TDataMember *dm = (ox->IsA())->GetDataMember(var);
   TMethodCall *getter = dm->GetterMethod(ox->IsA());
   TString type = dm->GetFullTypeName();
   if(type.BeginsWith("Float_t")) {
      Double_t retd=0;
      getter->Execute(ox,"",retd);
      return (Float_t)(retd);
   }
   if(type.BeginsWith("Int_t")) {
      Longptr_t reti=0;
      getter->Execute(ox,"",reti);
      return ((Float_t)(reti));
   }
   return 0.;
}






