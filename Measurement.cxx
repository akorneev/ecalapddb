////////////////////////////////////////////////////////////////////////
// 22.10.2000 by Andrei Dorokhov
// The program is developing and supporting by Andrei Kouznetsov
//
////////////////////////////////////////////////////////////////////////

#include <TEnv.h>
#include <TGListView.h>


#include <TFile.h>
#include <TMap.h>
#include <TObjString.h>
#include <TBrowser.h>
#include <TFolder.h>
#include <TOrdCollection.h>
#include <TGraph.h>
#include <TDataMember.h>
#include <TMethodCall.h>
#include <TClass.h>




#include "TRandom.h"
#include "TDirectory.h"

#include "Measurement.h"

ClassImp(Header)
ClassImp(Hamamatsu)
ClassImp(Gain)
ClassImp(GainQE)
ClassImp(QE)
ClassImp(Noise)
ClassImp(Capacitance)
ClassImp(Annealing)
ClassImp(BRKDPSI)
ClassImp(BRKDCERN)
ClassImp(GAINPSI)
ClassImp(GAINCERN)
ClassImp(Mnoise)
// extern Int_t fPlotCanvasID;
// extern TObjArray *fCanvasArray;
// 
// // 
// void PlotCanvas::SaveMe() // *MENU* 
// {cout<<"Save me invoked"<<endl;} // *MENU*
// 
// 
// 
// PlotCanvas::PlotCanvas(const char *name,const char *title, Int_t wtopx, Int_t wtopy, Int_t ww, Int_t wh) : 
//    TCanvas(name,title,wtopx,wtopy,ww,wh) { 
//    fCanvasName = new TString(title);
//    fPlotCanvasID++;
//    fCurrentCanvasID=fPlotCanvasID;
//    ToggleEventStatus();
// 
// }
// 
// PlotCanvas::~PlotCanvas() {
//    fCanvasArray->Remove(this);   
//    if(fGraphs!=NULL) {
//       fGraphs->SetOwner();
//       delete fGraphs;
//    }
//    if(fCanvasName) delete fCanvasName;
//    fPlotCanvasID--;
// }
// 
// 
// 



Hamamatsu::Hamamatsu()
{
}


void Header::DeleteArr(TObject *o) {
   TList *lm = (o->IsA())->GetListOfDataMembers();
   TIter next(lm);
   while(TObject *obj = next()) {
      TDataMember *dm = (TDataMember*)obj;
      if(dm!=NULL) { 
         TMethodCall *getter = dm->GetterMethod();
         if(getter!=NULL) {
            TString type = dm->GetFullTypeName();
            if(type.BeginsWith("TArrayF")) {
               Longptr_t retd=0;
			   // TODO: strange condition ????
               if(retd!=0) {
                  getter->Execute(o,"",retd);
                  ((TArrayF*) (retd))->Set(0);
               }
            }
         }
      }   
   }   
}


void Hamamatsu::SetSerialNo(TString sn) 
{
   fSerialNo = sn; 
   const char *s = ((const char*)sn);
   fSN = atof(s);
   fLot=-1;fWafer=-1;fSNo=-1;
   if(strlen(s)==10) {
      char s1[3]={0};
      char s2[3]={0};
      char s3[7]={0};
      strncpy(s1,s,2);
      strncpy(s2,&s[2],2);
      strncpy(s3,&s[4],6);

      fLot = atoi(s1);fWafer = atoi(s2);fSNo = atoi(s3);
   }      
}   

void Hamamatsu::SetPosition(TString sp) { 
   fPosition = sp; 
   Int_t o=0,oy=0;
   if(strlen((const char*)sp)==3) {  
      char s = ((const char*)sp)[0];
      (s=='A')?o=1:(s=='B')?o=2:(s=='C')?o=3:(s=='D')?o=4:(s=='E')?o=5:(s=='F')?o=6:
         (s=='G')?o=7:(s=='H')?o=8:(s=='I')?o=9:(s=='J')?o=10:(s=='K')?o=11:(s=='L')?o=12:
         (s=='M')?o=13:(s=='N')?o=14:(s=='O')?o=15:(s=='P')?o=16:(s=='Q')?o=17:(s=='R')?o=18:     
         (s=='S')?o=19:(s=='T')?o=20:(s=='U')?o=21:(s=='V')?o=22:(s=='W')?o=23:(s=='X')?o=24:
         (s=='Y')?o=25:(s=='Z')?o=26:o=0;

      char ss[3]={0};
      strcpy(ss,&(((const char*)sp)[1]));
      oy=atoi(ss);
   }
   fXPos = o;
   fYPos = oy;      
}   



Hamamatsu::~Hamamatsu(){
   DeleteArr(this);
}


Gain::~Gain() {
   DeleteArr(this);
}

QE::~QE() {
   DeleteArr(this);
}

GainQE::~GainQE() {
   DeleteArr(this);
}


Noise::~Noise() {
   DeleteArr(this);
}


Capacitance::~Capacitance() {
   DeleteArr(this);
}

Annealing::~Annealing() {
  DeleteArr(this);
}

BRKDPSI::~BRKDPSI() {
  DeleteArr(this);
}
BRKDCERN::~BRKDCERN() {
  DeleteArr(this);
}
GAINPSI::~GAINPSI() {
  DeleteArr(this);
}
GAINCERN::~GAINCERN() {
  DeleteArr(this);
}
void Mnoise::SetSerialNo(TString sn) 
{
   fSerialNo = sn; 
   const char *s = ((const char*)sn);
   fSN = atof(s);
   fLot=-1;fWafer=-1;fSNo=-1;
   if(strlen(s)==10) {
      char s1[3]={0};
      char s2[3]={0};
      char s3[7]={0};
      strncpy(s1,s,2);
      strncpy(s2,&s[2],2);
      strncpy(s3,&s[4],6);

      fLot = atoi(s1);fWafer = atoi(s2);fSNo = atoi(s3);
   }      
}   

Mnoise::~Mnoise() {
   DeleteArr(this);
}

//Units
TString Header::GetUnitsWC(TString n,TString type) { 
   TString ret=" ";
   if(!type.CompareTo("mHam")) {
      if(!n.CompareTo("fTemperature")) ret = "C";   
      else if(n.BeginsWith("fI") || n.BeginsWith("faI") 
      	       ||n.BeginsWith("faDark") || n.BeginsWith("faTotalCur") ) ret = "nA"; 
      else if(n.BeginsWith("fV") || n.BeginsWith("faV")) ret = "V";
      else if(n.BeginsWith("fdMdV")|| n.BeginsWith("fadMdV") ) ret = "%";
      else if(n.BeginsWith("fQE")) ret = "%";
      else if(n.BeginsWith("fNoise")||n.BeginsWith("faNoise")) ret = "a.u.";
      else if(n.BeginsWith("fC")) ret = "pF";
      //else if(n.BeginsWith("fpar1")) ret = "a.u.";
      //else if(n.BeginsWith("fpar2")) ret = "a.u.";
   }
   else if(type.BeginsWith("mGQ")) {
      if(n.BeginsWith("fV")) ret = "V";   
      else if(n.BeginsWith("fI") || n.BeginsWith("faI") 
      	       ||n.BeginsWith("faDark") || n.BeginsWith("faPhoto") ) ret = "nA"; 
      else if(n.BeginsWith("fV") || n.BeginsWith("faV")) ret = "V";
      else if(n.BeginsWith("fdMdV")|| n.BeginsWith("fadMdV") ) ret = "%";
      else if(n.BeginsWith("fQE")) ret = "%";
      else if(n.BeginsWith("fNoise")||n.BeginsWith("faNoise")) ret = "a.u.";
   }
   else if(type.BeginsWith("mG")) {
      if(!n.CompareTo("fTemperature")) ret = "C";   
      else if(n.BeginsWith("fI") || n.BeginsWith("faI") 
      	       ||n.BeginsWith("faDark") || n.BeginsWith("faPhoto") ) ret = "nA"; 
      else if(n.BeginsWith("fV") || n.BeginsWith("faV")) ret = "V";
      else if(n.BeginsWith("fdMdV")|| n.BeginsWith("fadMdV") ) ret = "%";
      else if(n.BeginsWith("fQE")) ret = "%";
      else if(n.BeginsWith("fNoise")||n.BeginsWith("faNoise")) ret = "a.u.";
      else if(n.BeginsWith("fC")) ret = "pF";
   }
   else if(type.BeginsWith("mQ")) {
      if(n.BeginsWith("fBias")) ret = "V";   
      else if(n.BeginsWith("fAPDdarkcurrent")) ret = "uA";
      else if(n.BeginsWith("fAPDQE")) ret = "%";
      else if(n.BeginsWith("fPINdarkcurrent")) ret = "uA";
      else if(n.BeginsWith("faWavelength")) ret = "nm";
      else if(n.BeginsWith("fAPDill")) ret = "nA";
      else if(n.BeginsWith("fPINill")) ret = "nA";
      else if(n.BeginsWith("faQE")) ret = "%";
   }
   else if(type.BeginsWith("mF")) {
      if(n.BeginsWith("fV") || n.BeginsWith("faV") ) ret = "V";   
      else if(n.BeginsWith("fI") || n.BeginsWith("faI") 
      	       ||n.BeginsWith("faDark") || n.BeginsWith("faPhoto") ) ret = "nA"; 
      else if(n.BeginsWith("fV") || n.BeginsWith("faV")) ret = "V";
      else if(n.BeginsWith("fdMdV")|| n.BeginsWith("fadMdV") ) ret = "%";
      else if(n.BeginsWith("fQE")) ret = "%";
      else if(n.BeginsWith("fNoise")||n.BeginsWith("faNoise")) ret = "a.u.";
      
   }
   else if(type.BeginsWith("mCR")) {                                 //opred osev.edintsi
      if(n.BeginsWith("fV") || n.BeginsWith("faV") ) ret = "V";   
      else if(n.BeginsWith("fC") || n.BeginsWith("faC") ) ret = "pF";   
      else if(n.BeginsWith("fR") || n.BeginsWith("faR") ) ret = "Ohm";   
   }
   return ret;
}

TString Header::GetUnits(TString n,TString type) { 
   TString ret = GetUnitsWC(n,type);
   if(!ret.CompareTo(" ")) {
      return ret;
   }
   else {
      return TString(", ")+ret;
   }         
}


//void Gain::CopyGainFromGainQE(GainQE *q) {
//   fFullSerialNo = q->fFullSerialNo;
//}









