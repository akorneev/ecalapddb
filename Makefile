RESTRICTED	= UNRESTRICTEDVERSION

CXX           = g++
CXXFLAGS      = -O -Wall -Wextra -fPIC
CXXFLAGS     += $(shell root-config --cflags)
CXXFLAGS     += -D$(RESTRICTED)
LD            = g++
LDFLAGS       = -O
SOFLAGS       = -shared
LIBS          = $(shell root-config --libs)
GLIBS         = $(shell root-config --glibs)

#------------------------------------------------------------------------------

BUILDDBO = BuildDB.o

APDLISTO = Apdlist.o

MEASUREMENTO        = Measurement.o MeasurementDict.o
MEASUREMENTSO       = libMeasurement.so

TESTO = Test.o
TEST  = Test.exe

APDBROWSERO = Apdbrowser.o
APDBROWSER  = Apdbrowser.exe

#------------------------------------------------------------------------------

.SUFFIXES: .cxx .o .so

all: MainMeasurement.exe $(MEASUREMENTSO) $(TEST) Query.exe $(APDBROWSER) many.exe txtdata.exe dump.exe txtgaindata.exe

MeasurementDict.cxx: Measurement.h MeasurementLinkDef.h
	rootcint -f $@ -c -D$(RESTRICTED) $^

$(MEASUREMENTSO): $(MEASUREMENTO)
		$(LD) -o $@ $^ $(SOFLAGS) $(LDFLAGS)

$(TEST): $(TESTO) $(MEASUREMENTO) $(BUILDDBO)
		$(LD) -o $@ $^ $(LDFLAGS) $(GLIBS)

$(APDBROWSER): $(APDBROWSERO) $(MEASUREMENTO) $(BUILDDBO) $(APDLISTO)
		$(LD) -o $@ $^ $(LDFLAGS) $(GLIBS)

%.exe: %.o $(MEASUREMENTO) $(BUILDDBO)
		$(LD) -o $@ $^ $(LDFLAGS) $(LIBS)

%.o: %.cxx
	$(CXX) -c $< $(CXXFLAGS)

clean:
		rm -f *.exe *.o *.so *.gch *Dict.* *Dict_rdict.pcm

Measurement.o: Measurement.h Token.h
MainMeasurement.o: Measurement.h BuildDB.h Token.h
BuildDB.o: BuildDB.h Measurement.h Token.h
Test.o: BuildDB.h Measurement.h Token.h
Query.o: BuildDB.h Measurement.h Token.h
Apdbrowser.o: Measurement.h BuildDB.h Token.h Apdlist.h
many.o: Measurement.h Token.h BuildDB.h Token.h Apdlist.h
txtdata.o: Measurement.h Token.h BuildDB.h Token.h Apdlist.h
txtgaindata.o: Measurement.h Token.h BuildDB.h Token.h Apdlist.h
