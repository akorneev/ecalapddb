#ifndef ROOT_BuildDB
#define ROOT_BuildDB

//////////////////////////////////////////////////////////////////////
//    24.10.2000 by Andrei Dorokhov                                 //
//    The program is developing and supporting by Andrei Kouznetsov //
//              BuildDB                                             //
//                                                                  //
//////////////////////////////////////////////////////////////////////

#include "Measurement.h"

#include "TFile.h"
#include "TObject.h"
#include "TObjArray.h"
#include "TObjString.h"
#include "TMap.h"
#include "TClass.h"
#include "TDataMember.h"

#include <iostream>
#include <fstream>
#include <cstring>

//static Int_t numberA=0;
//Double_t par0, par1, Vb;
//Double_t BuildDB::par0a[999999];
//Double_t BuildDB::par1a[999999]; 
//Double_t BuildDB::Vba[999999];
class BuildDB  {
public:
   TString fDBPath;
   TObjArray *fFiles;
   
   static Double_t par0a[999999], par1a[999999], Vba[999999], Temph[999999]; //Tempp[999999], ; 
   Float_t par0, par1, errpar1, errpar0, VBt, dVBt, dVBtpar1, dVBtpar0 ;
   Float_t VBp[4];
public:
   BuildDB(TString s);
   virtual ~BuildDB(){};
   TObjArray *GetFiles(){return fFiles;}

   void FillHamamatsu(TObjArray *map);
   void FillGain(TObjArray *mapn,TObjArray *mapa, TObjArray *mapi);
   void FillQE(TObjArray *mapn,TObjArray *mapa, TObjArray *mapi);
   void FillGainQE(TObjArray *mapn,TObjArray *mapa, TObjArray *mapi);
   void FillNoise(TObjArray *mapn,TObjArray *mapa, TObjArray *mapi);
   void FillCapacitance(TObjArray *mapn,TObjArray *mapa, TObjArray *mapi);
   void FillAnnealing(TObjArray *mapi);
   void FillBRKDPSI(TObjArray *mapn,TObjArray *mapi,TObjArray *mapa);
   void FillBRKDCERN(TObjArray *mapn,TObjArray *mapi,TObjArray *mapa);
   void FillGAINPSI(TObjArray *mapn,TObjArray *mapi,TObjArray *mapa);
   void FillGAINCERN(TObjArray *mapn,TObjArray *mapi,TObjArray *mapa);
   void FillMnoise(TObjArray *mapi);
//!!void FillCurrent(TObjArray *mapn);
void IdOverGain(Float_t &idoverg, Float_t &vatg,
      	             	 Float_t g, Float_t *v, Float_t *idark, Float_t
			 *iph,Int_t n,Float_t *mygain);

void IdOverGainHam(Int_t serno,Float_t &idoverg, Float_t &vatg,
      	             	 Float_t g, Float_t *v, Float_t *idark, Float_t
			 *iph,Int_t n);

void CalcQE(Hamamatsu *);
void CalcGain(Hamamatsu *);
void QuentinparPSI(BRKDPSI *); 
void QuentinparCERN(BRKDCERN *);
void IDforshumPSI(BRKDPSI *);
void IDforshumCERN(BRKDCERN *);
Float_t GetVar(TObject *ox,const char *var);

void SetVar(TObject *ox, const char *var, Float_t val) ;
void SetVar(TObject *ox, const char *var, Char_t *val) ;

void SetVar(TObject *ox, const char *var, Double_t val);

void SetVar(TObject *ox, const char *var, Int_t val);

void SetArr(TObject *ox, const char *var, Int_t val) ;
void SetArr(TObject *ox, const char *var,Int_t i, Float_t val) ;
TArrayF * GetArr(TObject *ox, const char *var) ;

//   ClassDef(BuildDB,1)  
};


class Spline {
public:
   Float_t *x,*y;
   Int_t n;        
   Double_t *a,*b;
   const char *name;

//   Spline(char *name,Double_t *x,Double_t *y, Int_t n) {
//   
//   }

   Spline(const char *name,Float_t *x,Float_t *y, Int_t n) {
      this->name=name;
      this->n=n;
      this->x=x;
      this->y=y;
//if(numberA==2509) cout<<"2509"<<endl;
      a = new Double_t[n-1];
      b = new Double_t[n-1];
      for(Int_t i=0;i<n-1;i++) {
      	 a[i] = (y[i+1]-y[i])/(x[i+1]-x[i]);
	 b[i] = y[i]-a[i]*x[i];
	    
//            if(numberA==2509) cout<<"constr"<<y[i]<<endl;
//            if(numberA==2509) cout<<"constr"<<y[i+1]<<endl;
//            if(numberA==2509) cout<<"constr"<<x[i]<<endl;
//            if(numberA==2509) cout<<"constr"<<x[i+1]<<endl;
      } 
//            if(numberA==1991) cout<<"constr"<<n<<endl;           
   }

   Spline(const char *name,TArrayF *x,TArrayF *y, Int_t n) {
      this->name=name;
      this->n=n;
      this->x=x->GetArray();
      this->y=y->GetArray();
      a = new Double_t[n-1];
      b = new Double_t[n-1];
      for(Int_t i=0;i<n-1;i++) {
      	 a[i] = (this->y[i+1]-this->y[i])/(this->x[i+1]-this->x[i]);
	     b[i] = this->y[i]-a[i]*this->x[i];
      } 
   }


   ~Spline(){delete [] a;delete [] b;}

   Double_t Eval(Double_t xe){
//   cout<<"xe"<<xe<<endl;
//            if(numberA==2509) cout<<"Eval"<<n<<endl;
      Int_t p1=0,p2=n-1,k;      
      while(p1<(p2-1)) {
      	 k = ((p2+p1)>>1); //cout<<"k="<<k<<endl;
      	 if(x[k]>xe) p2=k;
	 else p1=k;
//      if(numberA==2509) {
//	 cout<<"x[k]"<<x[k]<<endl;
//	 cout<<"k"<<k<<endl;
//      }
      }
//      if(numberA==2509) {
//      	 cout<<p1<<endl<<a[p1]<<endl<<b[p1]<<endl<<xe<<endl;
//	 cout<<a[p1]*xe+b[p1]<<endl;
//      }      
      return a[p1]*xe+b[p1];
   }   
   
   Double_t EvalD(Double_t xe){
      Int_t p1=0,p2=n-1,k;      

      while(p1<(p2-1)) {
      	 k = ((p2+p1)>>1);
      	 if(x[k]>xe) p2=k;
	 else p1=k;
      }
      if(xe < (x[p1]+x[p2])*0.5) {
      	 if(p1==0) return a[p1];
	 else return (a[p1-1]+a[p1])*0.5;
      }
      else {
      	 if(p2==n-1) return a[p1];
	 else return (a[p1+1]+a[p1])*0.5;     
      } 


   }   


   void Derivative(Float_t *xder, Float_t *yder) {
      for(Int_t i=0;i<n-1;i++) {
      	 xder[i] = (y[i+1]+y[i])*0.5;
      	 
//	 yder[i] = -(1.0/sqrt(y[i+1])-1.0/sqrt(y[i]))/(x[i+1]-x[i]);


	 yder[i] = ((y[i+1])-(y[i]))/(x[i+1]-x[i]);
	 yder[i] = 2.0*yder[i]/(y[i+1]+y[i]);
	 

//      	 xder[i] = 0.5*(-1.0/x[i+1]- 1.0/x[i]);
//	 yder[i] = (log(log(y[i+1])) - log(log(y[i])))/(-1.0/x[i+1]+ 1.0/x[i]) ;

//if(numberA==2509) {
//   cout<<"der"<<endl;
//   cout<<"der x y "<<i<<endl<< x[i]<<endl<<y[i]<<endl;
//   cout<<"der x y "<<i+1<<endl<< x[i+1]<<endl<<y[i+1]<<endl;

//   cout<<(y[i])<<endl;
//   cout<<(y[i+1])<<endl;

//   cout<<(y[i+1]+y[i])*0.5<<endl;
//   cout<<((y[i+1])-(y[i]))/(x[i+1]-x[i])<<endl;
//}
      }
//      xder[0] = xder[1]-(xder[2]-xder[1]);
//      xder[n-1] = xder[n-2]+(xder[n-2]-xder[n-3]);
      
   
   }  

};


/*
class Spline {
public:
   Double_t *y2;
   Double_t yp1,ypn;
   Double_t *xa,*ya;
   Int_t n;
   char *name;
   Spline(char *name,Double_t *x,Double_t *y, Int_t n) {
      this->name=name;
      this->n = n;
      xa=x;ya=y;
      y2=new Double_t[n];
      yp1=(y[1]-y[0])/(x[1]-x[0]);
      ypn=(y[n-1]-y[n-2])/(x[n-1]-x[n-2]);
      Int_t i,k;
      Double_t p,qn,sig,un,*u;
      u=new Double_t[n];
      if(yp1>0.99e30) y2[0]=u[0]=0.0;
      else {
      	 y2[0]=-0.5;
	 u[0]=(3.0/(x[1]-x[0]))*((y[1]-y[0])/(x[1]-y[0])-yp1);
      }
      for(i=1;i<n-1;i++) {
      	 sig=(x[i]-x[i-1])/(x[i+1]-x[i-1]);
	 p=sig*y2[i-1]+2.0;
	 y2[i]=(sig-1.0)/p;
	 u[i]=(y[i+1]-y[i])/(x[i+1]-x[i]) - (y[i]-y[i-1])/(x[i]-x[i-1]);
	 u[i]=(6.0*u[i]/(x[i+1]-x[i-1])-sig*u[i-1])/p;
      }
      if(ypn>0.99e30) qn=un=0.0;
      else {
      	 qn=0.5;
	 un=(3.0/(x[n-1]-x[n-2]))*(ypn-(y[n-1]-y[n-2])/(x[n-1]-x[n-2]));
      }
      y2[n-1]=(un-qn*u[n-2])/(qn*y2[n-2]+1.0);
      for(k=n-2;k>=0;k--) y2[k]=y2[k]*y2[k+1]+u[k];
      if(u!=NULL)delete u;
   }
   
   ~Spline() {
      if(y2!=NULL) delete y2;
   }
   
   Double_t Eval(Double_t x) {
      Int_t klo=1,khi=n,k;
      Double_t h,b,a;
      while(khi-klo > 1) {
      	 k=(khi+klo) >> 1;
	 if(xa[k-1] > x) khi=k;
	 else klo=k;
      }    
      khi=khi-1;klo=klo-1;        
      h=xa[khi]-xa[klo];
      a=(xa[khi]-x)/h;
      b=(x-xa[klo])/h;
      return a*ya[klo]+b*ya[khi]+((a*a*a-a)*y2[klo]+(b*b*b-b)*y2[khi])*(h*h)/6.0;      

   }
   
   Double_t EvalD(Double_t x) {
      Int_t klo=1,khi=n,k;
      Double_t h,b,a;
      while(khi-klo > 1) {
      	 k=(khi+klo) >> 1;
	 if(xa[k-1] > x) khi=k;
	 else klo=k;
      }    
      khi=khi-1;klo=klo-1;        
      h=xa[khi]-xa[klo];
      a=(xa[khi]-x)/h;
      b=(x-xa[klo])/h;
      return (ya[khi]-ya[klo])/(xa[khi]-xa[klo]) -
      	 (3.0*a*a-1.0)*(xa[khi]-xa[klo])*y2[klo]/6.0 +
      	 (3.0*b*b-1.0)*(xa[khi]-xa[klo])*y2[khi]/6.0;
	 
   }

};
*/



class MyFile : public TFile {
public :
TObject *mHam,*mGn,*mGa, *mGi, *mGQn,
        *mGQa,*mGQi,*mQn ,*mQa,*mQi ,
        *mFn ,*mFa ,*mFi ,*mCRn,*mCRa,*mCRi,*mAni,
        *mBPn, *mBPa, *mBPi, *mBCn, *mBCa, *mBCi, 
        *mGPn, *mGPa, *mGPi, *mGCn, *mGCi, *mGCa, *mMno;   

MyFile(const char *fname1, Option_t *option) : TFile(fname1,option) {
  //  cout<<"file in TFile"<<fname1<<endl;
      mHam = NULL;
      mGn = NULL;
      mGa = NULL;
      mGi = NULL;
      mGQn = NULL;
      mGQa = NULL;
      mGQi = NULL;
      mQn = NULL;
      mQa = NULL;
      mQi = NULL;
      mFn = NULL;
      mFa = NULL;
      mFi = NULL;
      mCRn = NULL;
      mCRa = NULL;
      mCRi = NULL;
      mAni = NULL;
      mBPn = NULL;
      mBPi = NULL;
      mBPa = NULL;
      mBCn = NULL;
      mBCi = NULL;
      mBCa = NULL;
      mGPn = NULL;
      mGPi = NULL;
      mGPa = NULL;
      mGCn = NULL;
      mGCi = NULL;
      mGCa = NULL;
      mMno = NULL;
   }
   

TObject * Get(const char *s) {
   if(!strcmp(s,"mHam")) {
      if(mHam==NULL) return mHam = TFile::Get(s);
      else return mHam;
   }
   else if(!strcmp(s,"mGn")) {
      if(mGn==NULL) return mGn = TFile::Get(s);
      else return mGn;
   }
   else if(!strcmp(s,"mGa")) {
      if(mGa==NULL) return mGa = TFile::Get(s);
      else return mGa;
   }
   else if(!strcmp(s,"mGi")) {
      if( mGi==NULL) return mGi = TFile::Get(s);
      else return mGi;
   }
   else if(!strcmp(s,"mGQn")) {
      if(!strcmp(s,"mGQn") && mGQn==NULL) return  mGQn= TFile::Get(s);
      else return mGQn;
   }
   else if(!strcmp(s,"mGQi")) {
      if(mGQi==NULL) return  mGQi= TFile::Get(s);
      else return mGQi;
   }
   else if(!strcmp(s,"mGQa") ) {
      if(mGQa==NULL) return mGQa = TFile::Get(s);
      else return mGQa;
   }
   else if(!strcmp(s,"mQn")) {
      if(mQn==NULL) return  mQn= TFile::Get(s);
      else return mQn;
   }
   else if(!strcmp(s,"mQa")) {
      if(mQa==NULL) return  mQa= TFile::Get(s);
      else return mQa;
   }
   else if(!strcmp(s,"mQi")) {
      if(mQi==NULL) return  mQi= TFile::Get(s);
      else return mQi;
   }
   else if(!strcmp(s,"mFn")) {
      if(mFn==NULL) return  mFn= TFile::Get(s);
      else return mFn;
   }
   else if(!strcmp(s,"mFa")) {
      if(mFa==NULL) return  mFa= TFile::Get(s);
      else return mFa;
   }
   else if(!strcmp(s,"mFi")) {
      if(mFi==NULL) return  mFi= TFile::Get(s);
      else return mFi;
   }
   else if(!strcmp(s,"mCRn")) {
      if(mCRn==NULL) return mCRn = TFile::Get(s);
      else return mCRn;
   }
   else if(!strcmp(s,"mCRa")) {
      if(mCRa==NULL) return  mCRa= TFile::Get(s);
      else return mCRa;
   }
   else if(!strcmp(s,"mCRi")) {
      if(mCRi==NULL) return  mCRi= TFile::Get(s);
      else return mCRi;
   }
   else if(!strcmp(s,"mAni")) {
     if(mAni==NULL) return mAni= TFile::Get(s);
     else return mAni;
   }
   else if(!strcmp(s,"mBPn")) {
     if(mBPn==NULL) return mBPn= TFile::Get(s);
     else return mBPn;
   }
   else if(!strcmp(s,"mBPi")) {
     if(mBPi==NULL) return mBPi= TFile::Get(s);
     else return mBPi;
   }
   else if(!strcmp(s,"mBPa")) {
     if(mBPa==NULL) return mBPa= TFile::Get(s);
     else return mBPa;
   }

   else if(!strcmp(s,"mBCn")) {
     if(mBCn==NULL) return mBCn= TFile::Get(s);
     else return mBCn;
   }
   else if(!strcmp(s,"mBCi")) {
     if(mBCi==NULL) return mBCi= TFile::Get(s);
     else return mBCi;
   }
   else if(!strcmp(s,"mBCa")) {
     if(mBCa==NULL) return mBCa= TFile::Get(s);
     else return mBCa;
   }
   else if(!strcmp(s,"mGPn")) {
     if(mGPn==NULL) return mGPn= TFile::Get(s);
     else return mGPn;
   }
   else if(!strcmp(s,"mGPi")) {
     if(mGPi==NULL) return mGPi= TFile::Get(s);
     else return mGPi;
   }
   else if(!strcmp(s,"mGPa")) {
     if(mGPa==NULL) return mGPa= TFile::Get(s);
     else return mGPa;
   }
   else if(!strcmp(s,"mGCn")) {
     if(mGCn==NULL) return mGCn= TFile::Get(s);
     else return mGCn;
   }
   else if(!strcmp(s,"mGCi")) {
     if(mGCi==NULL) return mGCi= TFile::Get(s);
     else return mGCi;
   }
   else if(!strcmp(s,"mGCa")) {
     if(mGCa==NULL) return mGCa= TFile::Get(s);
     else return mGCa;
   }
   else if(!strcmp(s,"mMno")) {
     if(mMno==NULL) return mMno= TFile::Get(s);
     else return mMno;
   }


   else return TFile::Get(s);
}
};
#endif




























