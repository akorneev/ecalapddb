////////////////////////////////////////////////////////////////////////
// 8.11.2000 by Andrei Dorokhov
//
//
//
////////////////////////////////////////////////////////////////////////

#include <stdlib.h>
#include <sys/types.h>
#include <dirent.h>

#include "TROOT.h"
#include "TSystem.h"
#include "TApplication.h"
#include "TFile.h"
#include "TRandom.h"
#include "TTree.h"
#include "TBranch.h"
#include "TClonesArray.h"
#include "TStopwatch.h"
#include "TCanvas.h"
#include "TGraph.h"

//#include "Measurement.h"
#include "BuildDB.h"
#include "Apdlist.h"
#include <TMethodCall.h>

void printmem(int i) {
cout<<"Stop:"<<i<<endl;
getchar();
}


//______________________________________________________________________________
int main(int argc, char **argv)
{
   TROOT APDMeasurement("measurement","APD measurements");
   TApplication theApp("App", &argc, argv);
//   theApp.Run();
//   TStorage MyHeap;
//   MyHeap.EnableStatistics();
//   cout<<"Before"<<endl;
//   MyHeap.PrintStatistics();
//   gROOT->SetBatch();
   TGGroupFrame *fContainerListAPD = new TGGroupFrame(gClient->GetRoot(),new TGString("Apd"));
   TObjArray *fListBoxAPD = new TObjArray();
   char str[100];
   TGLayoutHints *fL2 = new TGLayoutHints(kLHintsTop | kLHintsLeft | kLHintsExpandX,0,0,5,0);
   TGListBox *lb=0;
   Int_t lbe = 0;
   for(Int_t i=0;i<100;i++) {
      fListBoxAPD->Add(lb = new TGListBox(fContainerListAPD,i));
      fContainerListAPD->AddFrame(lb,fL2);
      
      for(Int_t j=0;j<100;j++){ 
         sprintf(str,"1234567890123456789012345678901234567890%20d",lbe);
         lb->AddEntry(new TGString(str),j); 
         lbe++;
      }

   }
   
   printmem(1);
/*
   for(Int_t i=0;i<100;i++) {
      TGListBox *llb = ((TGListBox*)fListBoxAPD->At(i));
      
      llb->RemoveEntries(0,99);
      delete llb;      
   }   
*/
   TList *list = fContainerListAPD->GetList();
   list->SetOwner();list->Clear();

   fListBoxAPD->SetOwner();//delete fListBoxAPD;



   delete fListBoxAPD;
   printmem(2);



   return 0;
}



