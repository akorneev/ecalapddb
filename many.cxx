#include "TFile.h"
#include "TKey.h"
#include "TObjArray.h"

#include <iostream>
#include <algorithm>
using namespace std;

int main(int argc, char* argv[])
{
  if (argc < 2) {
    cout << "Usage: ./many.exe file1 file2 ..." << endl;
    return 1;
  }
  
  // open all input files and prepare list of all uniq keys
  cout << "Input files:" << endl;
  TList flist;
  flist.SetOwner();
  vector<string> keys;
  
  for (int i = 1; i < argc; ++i) {
    cout << argv[i] << endl;
    
    // open file
    TFile* f = new TFile(argv[i]);
    flist.Add(f);
    
    if (f->IsZombie()) {
      cout << "ERROR: fail to open input file " << argv[i] << endl;
      return 1;
    }
    
    // populate list of keys
    TIter next(f->GetListOfKeys());
    TKey* key;
    while ((key = (TKey*)next())) {
      const char* name = key->GetName();
      
      if (find(keys.begin(), keys.end(), name) == keys.end())
        keys.push_back(name);
    }
  }
  
  cout << "Keys list:";
  for (size_t i = 0; i < keys.size(); ++i) cout << " " << keys[i];
  cout << endl;
  
  // open output file
  const char* outfile = "measurementtot.root";
  cout << "Output file:" << outfile << endl;
  TFile fnew(outfile, "RECREATE");
  
  if (fnew.IsZombie()) {
    cout << "ERROR: fail to open output file " << outfile << endl;
    return 1;
  }
  
  // loop on all objects (keys) of input files
  for (size_t i = 0; i < keys.size(); ++i) {
    const char* name = keys[i].c_str();
    cout << "Reading key " << name << endl;
    
    TObjArray* objnew = 0;
    
    // loop on all files
    TIter next(&flist);
    TFile* f;
    while ((f = (TFile*)next.Next())) {
      f->cd();
      TObject* obj = f->Get(name);
      
      // integrity check
      if (!obj) {
        cout << "ERROR: file " << f->GetName() << ": missing key" << endl;
        return 1;
      }
      
      if (!obj->InheritsFrom("TObjArray")) {
        cout << "ERROR: file " << f->GetName() << ": the key is not based on TObjArray" << endl;
        return 1;
      }
      
      TObjArray* objf = (TObjArray*)obj;
      cout << "size = " << objf->GetEntries() << endl;
      
      if (!objnew) {
        // the key seen first time in the input file list
        objnew = objf;
        continue;
      }
      
      // append data
      TObjArrayIter j(objf);
      TObject* o;
      while ((o = j.Next())) {
        //cout << objf->IndexOf(o) << endl;
        objnew->AddAtAndExpand(o, objf->IndexOf(o));
      }
      
      delete obj;
    }
    
    cout << "final size = " << objnew->GetEntries() << endl;
    
    fnew.cd();
    objnew->Write(name, TObject::kSingleKey | TObject::kOverwrite);
    delete objnew;
  }
  
  fnew.cd();
  fnew.ls();
  fnew.Write();
}
