#ifndef MyToken
#define MyToken

#include "TString.h"

#include <iostream>
using std::cout;
using std::endl;

class Token {
private:
   Bool_t isError;
   ULong_t count;
   ULong_t begin;
   ULong_t end;
   const char *str;
   const char *delim;
   char *newstr;
   char *Next() {
      if(begin<end) {

         const char* ts = strstr(&str[begin],delim);
         unsigned long len;
         if(ts != NULL) {
            len = ((uint64_t)(ts))  - ((uint64_t)(&str[begin]));
         }
         else {
      	    len = end-begin;
         }

         strncpy(newstr,&str[begin],len);	 
         strcpy(&newstr[len],"");
         begin = begin + len+strlen(delim);
	 
         return newstr;
      }
      else {
         return NULL;
      }
   }   
public:
   Token (const TString& s, const char* del) {
      isError=kFALSE;
      count =0;
      delim = del;
      str = (const char*)s;
      begin = 0;
      end = 0;
      if(str!=NULL){ 
	end = strlen(str);
	if(str[end-1]==0x0d) end--;
	if(str[end-1]==0x0a) end--;
	if(str[end-1]==0x0d) end--;
      }
      newstr = new char[end+1];
   
   }
   Token (const char *s, const char *del) {  
      isError=kFALSE;
      count =0;
      delim = del;
      str = s;
      begin = 0;
      end = 0;
      if(str!=NULL) {
	end = strlen(str);
	if(str[end-1]==0x0d) end--;
	if(str[end-1]==0x0a) end--;
	if(str[end-1]==0x0d) end--;
      }
      newstr = new char[end+1];
   }


   ~Token() {
      delete [] newstr;
   }
   Bool_t HasNext(){
      if(Next()!=NULL) {
	Trim();                           //otkus probeli  
         count++;
         return kTRUE;     
      } 
      else return kFALSE;
   }
   ULong_t Tokens() {return count;}
   char *Get() {
      return newstr;            
   }
   Double_t GetD() {
      if(!isNum(newstr)) {
	 cout<<"Not a number:"<< newstr<<endl;
      	 isError=kTRUE;
      }
      Double_t o = atof(newstr);
      return o;            
   }
   Long_t GetL() {
      if(!isNum(newstr)) {
      	 cout<<"Not a number:"<< newstr<<endl;
      	 isError=kTRUE;
      }
      Long_t o = atol(newstr);
      return o;            
   }


   Bool_t NoError() {
      return !(isError);
   }

   static Bool_t isNum(const char *s) {
      for(unsigned int i=0;i<strlen(s);i++) {
      	 if(s[i]!='0' && s[i]!='1'  && s[i]!='2' && s[i]!='3' && s[i]!='4'
	  && s[i]!='5' && s[i]!='6' && s[i]!='7' && s[i]!='8' && s[i]!='9'
	   && s[i]!='e' && s[i]!='E' && s[i]!='-' && s[i]!='+' && s[i]!='.'
	   && s[i]!=0x0d && s[i]!=' ' && s[i]!=0x0a) {
	    return kFALSE;
	 }   	 
      }    
      return kTRUE;	    
   }



   char *NextToken() { return Next();}
   
   Int_t GetX() {
      Int_t o=0;
      if(strlen(newstr)!=3) return 0;   
      char s = newstr[0];
      (s=='A')?o=1:(s=='B')?o=2:(s=='C')?o=3:(s=='D')?o=4:(s=='E')?o=5:(s=='F')?o=6:
      (s=='G')?o=7:(s=='H')?o=8:(s=='I')?o=9:(s=='J')?o=10:(s=='K')?o=11:(s=='L')?o=12:
      (s=='M')?o=13:(s=='N')?o=14:(s=='O')?o=15:(s=='P')?o=16:(s=='Q')?o=17:(s=='R')?o=18:     
      (s=='S')?o=19:(s=='T')?o=20:(s=='U')?o=21:(s=='V')?o=22:(s=='W')?o=23:(s=='X')?o=24:
      (s=='Y')?o=25:(s=='Z')?o=26:o=0;    
      return o;
   }

   Int_t GetY() {
      Int_t o=0;
      if(GetX()==0) return 0;
      if(strlen(newstr)!=3) return 0;  
      char s[3];
      strcpy(s,&newstr[1]);
      o=atoi(s);
      return o;             
   }



private:
   char *Trim() {
      int len = strlen(newstr);
      if(len==0) return newstr;
      for(int ic=0;ic<len;ic++) if(newstr[ic]==0x09) newstr[ic]=' '; //remove TAB
      int fi=0,ei=len-1;
      if(newstr[0]==' ') {
         fi = 1;
         for(int i=0; i<len-1;i++) {
            fi=i+1;
            if((newstr[i]==' ') && (newstr[i+1]!=' ')) break;
         }
      }
      if(newstr[len-1]==' ') {
         for(int i=len-1;i>0;i--) { 
            ei=i-1;
            if(newstr[i-1]!=' ' && newstr[i]==' ') break;
         }
      }
      if(ei < fi) { 
         strcpy(newstr,"");
         return newstr;
      }
      char *ts = new char[ei-fi+2];
      strncpy(ts,&newstr[fi],ei-fi+1);
      strcpy(&ts[ei-fi+1],"");
      strcpy(newstr,ts);
      delete [] ts;
      return newstr;
   }

};


#endif
