#include "TFile.h"
#include "TString.h"
#include <string.h>
#include <fstream>
#include <iostream>
#include "Measurement.h"
using std::ifstream;
using std::ofstream;
//______________________________________________________________________

int main( int argc, char **argv ){
  //Double_t noise[6000];
  Int_t nomi=0,gmin=40, gmax=400, gminp=100, nhmin,nhmax,fl, n;
  Double_t IDoGp[500], Gp[500], IDoGc[500], Gc[500], IDoGh[20], Gh[20],IDoGhmax;
  TFile f("/home/apddata/apdsoft/measurement.root");
  BRKDCERN *bc=NULL;
  BRKDPSI *bp=NULL;
  Hamamatsu *h=NULL;
  Mnoise *no=NULL;
  GAINCERN *bg=NULL;
  TString line;
  TObjArray *arrg=(TObjArray*)f.Get("mGCa");
  TObjArray *arrc=(TObjArray*)f.Get("mBCa");
  TObjArray *arrp=(TObjArray*)f.Get("mBPi"); 
  TObjArray *arrh=(TObjArray*)f.Get("mHam");
  TObjArray *arrn=(TObjArray*)f.Get("mMno");

  if (argc < 2) {
    cout << "Usage: txtdata.exe input_file" << endl;
    return 1;
  }
  
  ifstream fin(argv[1]);
  ofstream fcernvbg("gaincern_vb.txt");
  ofstream fcernvb("cern_vb.txt");
  ofstream fpsivb("psi_vb.txt");
  ofstream fnoise("noise.txt");
  ofstream fidoverM("IdoverM.txt");
  fcernvbg<<"APDNumber,,TestDone,,,,,,,,,Temperature,Vb,,ID50,,"<<endl; 
  fcernvb<<"APDNumber,,TestDone,,,,,,,,,Temperature,Vb,,ID50,,"<<endl; 
  fpsivb<<"APDNumber,,TestDone,,,,,,,,,Temperature,Vb,,ID50,,"<<endl; 
  fnoise<<"APDNumber,Noise1,Noise50,Noise150,Noise300"<<endl;
  fidoverM<<"APD ID,flag,Reason,MIN position"<<endl;
  while(!fin.eof()){
    bc=NULL;bp=NULL;no=NULL;bg=NULL;
    line.ReadLine(fin);
    if(line.Sizeof()<2) continue;
    line.Resize(10);
    nomi=atoi(line.Data())%1000000;
    //    nomi=atoi(&line.Data()[4]);
    if(nomi<929000 && nomi>928000) continue;
    if(nomi<=arrg->GetLast()) bg= (GAINCERN *) (arrg->At(nomi));
    if(nomi<=arrc->GetLast()) bc= (BRKDCERN *) (arrc->At(nomi));
    if(nomi<=arrp->GetLast()) bp= (BRKDPSI *) (arrp->At(nomi));
    if(nomi<=arrn->GetLast()) no= (Mnoise *) (arrn->At(nomi));
    h= (Hamamatsu *) (arrh->At(nomi));

    n=(h->faIdarkOverGain).GetSize();
    for (Int_t l=0;l<n;l++){
      IDoGh[l]=((h->faIdarkOverGain).GetArray())[l];
      Gh[l]=((h->faGain).GetArray())[l];
    }
    fl=0; nhmin=0; nhmax=0;
    for (Int_t l=1;l<n-2;l++){
      if(Gh[l]>gmin && nhmin==0) nhmin=l+1;
      if(Gh[l+2]>gmax && nhmax==0) nhmax=l+1;
      if(IDoGh[l+1]>1.1*IDoGh[l] && Gh[l]>gmin && Gh[l+1]<gmax && Gh[l+1]>Gh[l]) {
   	if((IDoGh[l+2]>IDoGh[l+1] && Gh[l+2]>Gh[l+1]) || l+2>gmax) {fidoverM<<line.Data()<<",1,Id/M_Ham,"<<Gh[l]<<endl;fl=1;break;}
      }    
      else if(IDoGh[l+2]>1.1*IDoGh[l+1] && Gh[l]>gmin && Gh[l+2]>Gh[l+1] && Gh[l+1]<265 && Gh[l+2]>400) {
	fidoverM<<line.Data()<<",1,Id/M_Ham,"<<Gh[l+1]<<endl;fl=1;break;   
      }
    }
    IDoGhmax=IDoGh[nhmin] ;
    for (Int_t l=1;l<n-2;l++){
      if(Gh[l]>gmin && Gh[l+1]<gmax) {if (IDoGhmax<IDoGh[l+1]) IDoGhmax=IDoGh[l+1];}
    }
    if(nhmax==0) nhmax=n-1;
    if(IDoGhmax>1.1*IDoGh[nhmin] && fl==0) {fidoverM<<line.Data()<<",1,Id/M_Ham,"<<Gh[nhmin]<<endl;}

    if(!bc) {} //{fcernvb<<line.Data()<<"\t"<<"no data"<<endl;}
    else{
      fcernvb<<line.Data()<<",,a,,,,,,,,,"<<bc->fTemperature<<","<<bc->fVB<<",,"<<bc->fID<<",,"<<endl;

      n=(bc->faIdarkOverGainfit).GetSize();
      for (Int_t l=0;l<n;l++){
	IDoGc[l]=((bc->faIdarkOverGainfit).GetArray())[l];
	Gc[l]=((bc->faGainfit).GetArray())[l];
      }
      for (Int_t l=1;l<n-2;l++){
	if(IDoGc[l+1]>1.05*IDoGc[l] && Gc[l]>gmin && Gc[l]<gmax && Gc[l+1]>Gc[l]) 
	  if(IDoGc[l+2]>IDoGc[l+1] && Gc[l+2]>Gc[l+1])
	    {fidoverM<<line.Data()<<",100,Id/M_cern,"<<Gc[l]<<endl;break;}
	//{cout<<"CERN "<<nomi<<endl;break;}
      }
    }

   if(!bg) {} //{fcernvb<<line.Data()<<"\t"<<"no data"<<endl;}
    else{
      fcernvbg<<line.Data()<<",,a,,,,,,,,,"<<bg->fMeanTemp<<","<<bg->fVb<<",,"<<bg->fIdark<<",,"<<endl;
      n=(bg->faIdarkOverGain).GetSize();
      for (Int_t l=0;l<n;l++){
	IDoGc[l]=((bg->faIdarkOverGain).GetArray())[l];
	Gc[l]=((bg->faGain).GetArray())[l];
      }
      for (Int_t l=1;l<n-2;l++){
	if(IDoGc[l+1]>1.05*IDoGc[l] && Gc[l]>gmin && Gc[l]<gmax && Gc[l+1]>Gc[l]) 
	  if(IDoGc[l+2]>IDoGc[l+1] && Gc[l+2]>Gc[l+1])
	    {fidoverM<<line.Data()<<",100,Id/M_cerngain,"<<Gc[l]<<endl;break;}
	//{cout<<"CERN "<<nomi<<endl;break;}
      }
    }



    if(!bp) {} //{fpsivb<<line.Data()<<"\t"<<"no data"<<endl;}
    else{
      fpsivb<<line.Data()<<",,i,,,,,,,,,"<<bp->fTemperature<<","<<bp->fVB<<",,"<<bp->fID<<",,"<<endl;
      n=(bp->faIdarkOverGainfit).GetSize();
      for (Int_t l=0;l<n;l++){
	IDoGp[l]=((bp->faIdarkOverGainfit).GetArray())[l];
	Gp[l]=((bp->faGainfit).GetArray())[l];
      }
      for (Int_t l=1;l<n-2;l++){
	if(IDoGp[l+1]>1.05*IDoGp[l] && Gp[l]>gminp && Gp[l]<gmax && Gp[l+1]>Gp[l]) 
	  if(IDoGp[l+2]>IDoGp[l+1] && Gp[l+2]>Gp[l+1])
	    {fidoverM<<line.Data()<<",10,Id/M_psi,"<<Gp[l]<<endl;break;}
      }
    }

    if(!no) {} //{fnoise<<line.Data()<<"\t"<<"no data"<<endl;}
    else{
      fnoise<<line.Data()<<","<<no->fNoise1<<","<<no->fNoise50<<","<<no->fNoise150<<","<<no->fNoise300<<endl;
    }  
  }
  fin.close();
  fcernvb.close();
  fcernvbg.close();
  fpsivb.close();
  fnoise.close();
  fidoverM.close();
  f.Close();
  f.Delete("*");
  if(arrg) arrg->~TObjArray();
  if(arrc) arrc->~TObjArray();
  if(arrp) arrp->~TObjArray();
  if(arrh) arrh->~TObjArray();
  if(arrn) arrn->~TObjArray();


}

  /*
  //  Int_t nomi=0,gmin=40, gmax=400, gminp=100, nhmin,nhmax,fl, n;
  //Double_t IDoGp[500], Gp[500], IDoGc[500], Gc[500], IDoGh[20], Gh[20],IDoGhmax;
  TFile f("/home/apddata/Double_irr1/measurement.root");
  BRKDCERN *bc=NULL;
  BRKDPSI *bp=NULL;
  Hamamatsu *h=NULL;
  Mnoise *no=NULL;
  TString line;
  TObjArray *arrc=(TObjArray*)f.Get("mBCa");
  TObjArray *arrp=(TObjArray*)f.Get("mBPi"); 
  TObjArray *arrh=(TObjArray*)f.Get("mHam");
  TObjArray *arrn=(TObjArray*)f.Get("mMno");

  ifstream fin(argv[1]);
  ofstream fcernvb("cern_vb_d.txt");
  ofstream fpsivb("psi_vb_d.txt");
  ofstream fnoise("noise_d.txt");
  ofstream fidoverM("IdoverM.txt");
  fcernvb<<"APDNumber,,TestDone,,,,,,,,,Temperature,Vb,,ID50,,"<<endl; 
  fpsivb<<"APDNumber,,TestDone,,,,,,,,,Temperature,Vb,,ID50,,"<<endl; 
  fnoise<<"APDNumber,Noise1,Noise50,Noise150,Noise300"<<endl;
  fidoverM<<"APD ID,flag,Reason,MIN position"<<endl;
  while(!fin.eof()){
    bc=NULL;bp=NULL;no=NULL;
    line.ReadLine(fin);
    if(line.Sizeof()<2) continue;
    line.Resize(10);
    nomi=atoi(&line.Data()[4]);
    if(nomi<929000 && nomi>928000) continue;
    if(nomi<=arrc->GetLast()) bc= (BRKDCERN *) (arrc->At(nomi));
    if(nomi<=arrp->GetLast()) bp= (BRKDPSI *) (arrp->At(nomi));
    if(nomi<=arrn->GetLast()) no= (Mnoise *) (arrn->At(nomi));
    h= (Hamamatsu *) (arrh->At(nomi));

    n=(h->faIdarkOverGain).GetSize();
    for (Int_t l=0;l<n;l++){
      IDoGh[l]=((h->faIdarkOverGain).GetArray())[l];
      Gh[l]=((h->faGain).GetArray())[l];
    }
    fl=0; nhmin=0; nhmax=0;
    for (Int_t l=1;l<n-2;l++){
      if(Gh[l]>gmin && nhmin==0) nhmin=l+1;
      if(Gh[l+2]>gmax && nhmax==0) nhmax=l+1;
      if(IDoGh[l+1]>1.1*IDoGh[l] && Gh[l]>gmin && Gh[l+1]<gmax && Gh[l+1]>Gh[l]) {
   	if((IDoGh[l+2]>IDoGh[l+1] && Gh[l+2]>Gh[l+1]) || l+2>gmax) {fidoverM<<line.Data()<<",1,Id/M_Ham,"<<Gh[l]<<endl;fl=1;break;}
      }    
      else if(IDoGh[l+2]>1.1*IDoGh[l+1] && Gh[l]>gmin && Gh[l+2]>Gh[l+1] && Gh[l+1]<265 && Gh[l+2]>400) {
	fidoverM<<line.Data()<<",1,Id/M_Ham,"<<Gh[l+1]<<endl;fl=1;break;   
      }
    }
    IDoGhmax=IDoGh[nhmin] ;
    for (Int_t l=1;l<n-2;l++){
      if(Gh[l]>gmin && Gh[l+1]<gmax) {(IDoGhmax>IDoGh[l+1])?:IDoGhmax=IDoGh[l+1];}
    }
    if(nhmax==0) nhmax=n-1;
    if(IDoGhmax>1.1*IDoGh[nhmin] && fl==0) {fidoverM<<line.Data()<<",1,Id/M_Ham,"<<Gh[nhmin]<<endl;}

    if(!bc) {} //{fcernvb<<line.Data()<<"\t"<<"no data"<<endl;}
    else{
      fcernvb<<line.Data()<<",,a,,,,,,,,,"<<bc->fTemperature<<","<<bc->fVB<<",,"<<bc->fID<<",,"<<endl;

      n=(bc->faIdarkOverGainfit).GetSize();
      for (Int_t l=0;l<n;l++){
	IDoGc[l]=((bc->faIdarkOverGainfit).GetArray())[l];
	Gc[l]=((bc->faGainfit).GetArray())[l];
      }
      for (Int_t l=1;l<n-2;l++){
	if(IDoGc[l+1]>1.05*IDoGc[l] && Gc[l]>gmin && Gc[l]<gmax && Gc[l+1]>Gc[l]) 
	  if(IDoGc[l+2]>IDoGc[l+1] && Gc[l+2]>Gc[l+1])
	    {fidoverM<<line.Data()<<",100,Id/M_cern,"<<Gc[l]<<endl;break;}
	//{cout<<"CERN "<<nomi<<endl;break;}
      }
    }

    if(!bp) {} //{fpsivb<<line.Data()<<"\t"<<"no data"<<endl;}
    else{
      fpsivb<<line.Data()<<",,i,,,,,,,,,"<<bp->fTemperature<<","<<bp->fVB<<",,"<<bp->fID<<",,"<<endl;
      n=(bp->faIdarkOverGainfit).GetSize();
      for (Int_t l=0;l<n;l++){
	IDoGp[l]=((bp->faIdarkOverGainfit).GetArray())[l];
	Gp[l]=((bp->faGainfit).GetArray())[l];
      }
      for (Int_t l=1;l<n-2;l++){
	if(IDoGp[l+1]>1.05*IDoGp[l] && Gp[l]>gminp && Gp[l]<gmax && Gp[l+1]>Gp[l]) 
	  if(IDoGp[l+2]>IDoGp[l+1] && Gp[l+2]>Gp[l+1])
	    {fidoverM<<line.Data()<<",10,Id/M_psi,"<<Gp[l]<<endl;break;}
      }
    }

    if(!no) {} //{fnoise<<line.Data()<<"\t"<<"no data"<<endl;}
    else{
      fnoise<<line.Data()<<","<<no->fNoise1<<","<<no->fNoise50<<","<<no->fNoise150<<","<<no->fNoise300<<endl;
    }  
  }
  fin.close();
  fcernvb.close();
  fpsivb.close();
  fnoise.close();
  fidoverM.close();
  f.~TFile();
  arrc->~TObjArray();
  arrp->~TObjArray();
  arrh->~TObjArray();
  arrn->~TObjArray();

  */

//bad_IdM.txt  cern_vb.txt  noise.txt  psi_vb.txt fGain_incrIDoverM














